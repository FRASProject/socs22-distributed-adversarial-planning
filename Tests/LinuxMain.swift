import XCTest

import FRASTests

var tests = [XCTestCaseEntry]()
tests += FRASTests.allTests()
XCTMain(tests)