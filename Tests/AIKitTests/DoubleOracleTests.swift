//
//  DoubleOracleTests.swift
//  FRASTests
//
//  Created by Pavel Rytir on 9/3/18.
//

import XCTest
@testable import AIKit


final class DoubleOracleTests: XCTestCase {

    
//    func testLPGInitLAMABestResponse3UAVNoCommCompletePOIAbstraction0() {
//        // When deadlines are too loose, LAMA sometime cannot fnd good plans.
//        // This takes around 3 minutes.
//        let i = 3
//        let verbose = true
//        let gridX = 3 * i + (i % 2 == 0 ? 1 : 0)
//        let gridY = 2 * i + (i % 2 == 0 ? 0 : 1)
//        let gridZ = 1
//        let communicationRange = ceil(sqrt(Double(gridX) / 2 + Double(gridY) / 2 + Double(i) / 2))
//        let gridGameConfig = FRASGridGameGeneratorConfig(x: gridX, y: gridY, z: gridZ, numUAVP1: i, p1Relays: [], numUAVP2: i, p2Relays: [], numResources: i, numberOfControlNodes: 0, spreadResources: false, length: Double(10 * (gridX - 1)), width: Double(10 * (gridY - 1)), height: Double(1 * gridZ), communicationRange: communicationRange, communicationMode: .noCommunication, sensors: [], resourceRequiredSensors: [:], uavSensors: [:])
//        let game = generateSimpleGridGame(from: gridGameConfig)
//        
//        let abstraction = CompletePOIAbstraction(numberOfSubdivisions: 0)
//        
//        let lpgConfig = LPGPlanner.LPGConfig(numberOfIterations: 120, maxTime: 60, verbose: false, plannerLPGExecutablePath: LPGPlanner.LPGConfig.defaultConfig.plannerLPGExecutablePath)
//        
//        let initPlanner = TemporalPlanner(planner: LPGPlanner(plannerConfig: lpgConfig, verbose: verbose), abstraction: abstraction,  mode: .noCommunication)
//        
//        let lamaConfig = LAMAPlanner.LAMAConfig(maxTime: 120, verbose: false, plannerExecutablePath: LAMAPlanner.LAMAConfig.defaultConfig.plannerExecutablePath, anyTimePlanCheckerExecutablePath: "", killScriptExecutablePath: "")
//        
//        let bestResponsePlanner = PlannerBestResponse(planner: LAMAPlanner(plannerConfig: lamaConfig, verbose: verbose), abstraction: abstraction, hardDeadlines: true, unitTurnExpansion: 1.0, mode: .noCommunication, verbose: true)
//        
//        
//        let doubleOracleConfiguration = DoubleOracleConfiguration(numberOfInitialPlans: 1, maximumNumberOfAttemptsToImproveEquilibriumApprox: 2, initialPlansPlannerP1: initPlanner, initialPlansPlannerP2: initPlanner, bestResponsePlannerP1: [bestResponsePlanner], bestResponsePlannerP2: [bestResponsePlanner], epsilon: FRASLibConfig.defaultEpsilon, name: "test343")
//        
//        var doubleOracle = try! DoubleOracleAlgorithm(game: game, configuration: doubleOracleConfiguration, plansCacheDir: nil, verbose: verbose)
//        
//        let equilibriumReached = try! doubleOracle.run(numberOfIterations: 20)
//        
//        XCTAssertTrue(equilibriumReached)
//        let stats = doubleOracle.log.map { $0.statItem }
//        
//        XCTAssert(abs(stats.last!.player1EqVal - 1.5) < 0.25)
//        XCTAssert(abs(stats.last!.player2EqVal - 1.5) < 0.25)
//        
//        print(stats)
//    }
//
//    
//
//    
//
// 
//    
//    
//    func testLPGInitLAMABestResponse3UAVCommControlNodesCompletePOIAbstraction0() {
//        
//        // This takes around 3 minutes.
//        let i = 3
//        let verbose = false
//        let gridX = 3 * i + (i % 2 == 0 ? 1 : 0)
//        let gridY = 2 * i + (i % 2 == 0 ? 0 : 1)
//        let gridZ = 1
//        let communicationRange = ceil(sqrt(Double(gridX) / 2 + Double(gridY) / 2 + Double(i) / 2))
//        let gridGameConfig = FRASGridGameGeneratorConfig(x: gridX, y: gridY, z: gridZ, numUAVP1: i, p1Relays: [], numUAVP2: i, p2Relays: [], numResources: i, numberOfControlNodes: 1, spreadResources: false, length: Double(10 * (gridX - 1)), width: Double(10 * (gridY - 1)), height: Double(1 * gridZ), communicationRange: communicationRange, communicationMode: .communicationWithCentralNodes, sensors: [], resourceRequiredSensors: [:], uavSensors: [:])
//       
//        let game = generateSimpleGridGame(from: gridGameConfig)
//        
//        let abstraction = CompletePOIAbstraction(numberOfSubdivisions: 0)
//        
//        
//        let lpgConfig = LPGPlanner.LPGConfig(numberOfIterations: 30, maxTime: 30, verbose: verbose, plannerLPGExecutablePath: LPGPlanner.LPGConfig.defaultConfig.plannerLPGExecutablePath)
//        
//        let initPlanner = TemporalPlanner(planner: LPGPlanner(plannerConfig: lpgConfig, verbose: verbose), abstraction: abstraction,  mode: .communicationWithCentralNodesFixedRelays)
//        
//        let lamaConfig = LAMAPlanner.LAMAConfig(maxTime: 60, verbose: verbose, plannerExecutablePath: LAMAPlanner.LAMAConfig.defaultConfig.plannerExecutablePath, anyTimePlanCheckerExecutablePath: "", killScriptExecutablePath: "")
//        
//        let bestResponsePlanner = PlannerBestResponse(planner: LAMAPlanner(plannerConfig: lamaConfig, verbose: verbose), abstraction: abstraction, hardDeadlines: true, unitTurnExpansion: 1.0, mode: .communicationWithCentralNodesFixedRelays, verbose: true)
//        
//        
//        let doubleOracleConfiguration = DoubleOracleConfiguration(numberOfInitialPlans: 1, maximumNumberOfAttemptsToImproveEquilibriumApprox: 2, initialPlansPlannerP1: initPlanner, initialPlansPlannerP2: initPlanner, bestResponsePlannerP1: [bestResponsePlanner], bestResponsePlannerP2: [bestResponsePlanner], epsilon: FRASLibConfig.defaultEpsilon, name: "test342545")
//        
//        var doubleOracle = try! DoubleOracleAlgorithm(game: game, configuration: doubleOracleConfiguration, plansCacheDir: nil, verbose: verbose)
//        
//        let equilibriumReached = try! doubleOracle.run(numberOfIterations: 20)
//        
//        XCTAssertTrue(equilibriumReached)
//        let stats = doubleOracle.log.map { $0.statItem }
//        
//        XCTAssert(abs(stats.last!.player1EqVal - 1.5) < 0.25)
//        XCTAssert(abs(stats.last!.player2EqVal - 1.5) < 0.25)
//        
//        print(stats)
//    }
//
//    
//    func testLPGInitLAMABestResponse3UAVCommControlNodesCompleteDelaunayAbstraction0() {
//        
//        let i = 3
//        let verbose = false
//        let gridX = 3 * i + (i % 2 == 0 ? 1 : 0)
//        let gridY = 2 * i + (i % 2 == 0 ? 0 : 1)
//        let gridZ = 1
//        let communicationRange = ceil(sqrt(Double(gridX) / 2 + Double(gridY) / 2 + Double(i) / 2))
//        let gridGameConfig = FRASGridGameGeneratorConfig(x: gridX, y: gridY, z: gridZ, numUAVP1: i, p1Relays: [], numUAVP2: i, p2Relays: [], numResources: i, numberOfControlNodes: 1, spreadResources: false, length: Double(10 * (gridX - 1)), width: Double(10 * (gridY - 1)), height: Double(1 * gridZ), communicationRange: communicationRange, communicationMode: .communicationWithCentralNodes, sensors: [], resourceRequiredSensors: [:], uavSensors: [:])
//        let game = generateSimpleGridGame(from: gridGameConfig)
//        
//        let abstraction = DelaunayAbstraction(numberOfSubdivisions: 0)
//                
//        
//        let lpgConfig = LPGPlanner.LPGConfig(numberOfIterations: 30, maxTime: 30, verbose: verbose, plannerLPGExecutablePath: LPGPlanner.LPGConfig.defaultConfig.plannerLPGExecutablePath)
//        
//        let initPlanner = TemporalPlanner(planner: LPGPlanner(plannerConfig: lpgConfig, verbose: verbose), abstraction: abstraction,  mode: .communicationWithCentralNodesFixedRelays)
//        
//        
//        let lamaConfig = LAMAPlanner.LAMAConfig(maxTime: 60, verbose: verbose, plannerExecutablePath: LAMAPlanner.LAMAConfig.defaultConfig.plannerExecutablePath, anyTimePlanCheckerExecutablePath: "", killScriptExecutablePath: "")
//        
//        let bestResponsePlanner = PlannerBestResponse(planner: LAMAPlanner(plannerConfig: lamaConfig, verbose: verbose), abstraction: abstraction, hardDeadlines: true, unitTurnExpansion: 1.0, mode: .communicationWithCentralNodesFixedRelays, verbose: true)
//        
//        
//                
//        let doubleOracleConfiguration = DoubleOracleConfiguration(numberOfInitialPlans: 1, maximumNumberOfAttemptsToImproveEquilibriumApprox: 2, initialPlansPlannerP1: initPlanner, initialPlansPlannerP2: initPlanner, bestResponsePlannerP1: [bestResponsePlanner], bestResponsePlannerP2: [bestResponsePlanner], epsilon: FRASLibConfig.defaultEpsilon, name: "test4534543")
//        
//        var doubleOracle = try! DoubleOracleAlgorithm(game: game, configuration: doubleOracleConfiguration, plansCacheDir: nil, verbose: verbose)
//        
//        let equilibriumReached = try! doubleOracle.run(numberOfIterations: 20)
//        
//        XCTAssertTrue(equilibriumReached)
//        let stats = doubleOracle.log.map { $0.statItem }
//        
//        print(stats)
//        
//        XCTAssert(abs(stats.last!.player1EqVal - 1.5) < 0.25)
//        XCTAssert(abs(stats.last!.player2EqVal - 1.5) < 0.25)
//        
//        
//    }
//    
//
//    
//    static var allTests = [
//        ("testLPGInitLAMABestResponse3UAVNoCommCompletePOIAbstraction0", testLPGInitLAMABestResponse3UAVNoCommCompletePOIAbstraction0),
//        ]
}
