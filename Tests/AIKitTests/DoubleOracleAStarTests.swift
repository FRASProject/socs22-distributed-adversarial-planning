//
//  DoubleOracleAStarTests.swift
//  FRASTests
//
//  Created by Pavel Rytir on 10/09/2018.
//

import XCTest
@testable import AIKit


final class DoubleOracleAStarTests: XCTestCase {
//    func testAStarNoComm() {
//        // This takes around 3 minutes.
//        let i = 3
//        let verbose = false
//        let gridX = 3 * i + (i % 2 == 0 ? 1 : 0)
//        let gridY = 2 * i + (i % 2 == 0 ? 0 : 1)
//        let gridZ = 1
//        let communicationRange = ceil(sqrt(Double(gridX) / 2 + Double(gridY) / 2 + Double(i) / 2))
//        let gridGameConfig = FRASGridGameGeneratorConfig(x: gridX, y: gridY, z: gridZ, numUAVP1: i, p1Relays: [], numUAVP2: i, p2Relays: [], numResources: i, numberOfControlNodes: 0, spreadResources: false, length: Double(10 * (gridX - 1)), width: Double(10 * (gridY - 1)), height: Double(1 * gridZ), communicationRange: communicationRange, communicationMode: .noCommunication, sensors: [], resourceRequiredSensors: [:], uavSensors: [:])
//        let game = generateSimpleGridGame(from: gridGameConfig)
//        
//       
//        let initPlanner = AStarPlanner(mode:.communicationWithCentralNodes , heuristics: { newHeuristics(environment: $0,player: $1, metric: $2)}, costFunction: costFunctionMaxDistSumPen,verbose: false)
//       
//        let bestResponsePlanner = AStarBestResponse(mode:.communicationWithCentralNodes, heuristics: {newHeuristics(environment: $0,player: $1, metric: $2, allDeadlines: $3)}, betterHeuristicsWeight: [1.0], costFunction: costFunctionMaxDistSumPen, verbose: false)
//        
//        
//           
//        let doubleOracleConfiguration = DoubleOracleConfiguration(numberOfInitialPlans: 1, maximumNumberOfAttemptsToImproveEquilibriumApprox: 2, initialPlansPlannerP1: initPlanner, initialPlansPlannerP2: initPlanner, bestResponsePlannerP1: [bestResponsePlanner], bestResponsePlannerP2: [bestResponsePlanner], epsilon: FRASLibConfig.defaultEpsilon, name: "test45435")
//        
//        var doubleOracle = try! DoubleOracleAlgorithm(game: game, configuration: doubleOracleConfiguration, plansCacheDir: nil, verbose: verbose)
//        
//        let equilibriumReached = try! doubleOracle.run(numberOfIterations: 20)
//        
//        XCTAssertTrue(equilibriumReached)
//        let stats = doubleOracle.log.map { $0.statItem }
//        
//        XCTAssert(abs(stats.last!.player1EqVal - 1.5) < 0.25)
//        XCTAssert(abs(stats.last!.player2EqVal - 1.5) < 0.25)
//        
//    }
//    
//    func testAStarCommWithoutCentralNodes() {
//        // This takes around 3 minutes.
//        let i = 3
//        let verbose = false
//        let gridX = 3 * i + (i % 2 == 0 ? 1 : 0)
//        let gridY = 2 * i + (i % 2 == 0 ? 0 : 1)
//        let gridZ = 1
//        let communicationRange = ceil(sqrt(Double(gridX) / 2 + Double(gridY) / 2 + Double(i) / 2))
//        let gridGameConfig = FRASGridGameGeneratorConfig(x: gridX, y: gridY, z: gridZ, numUAVP1: i, p1Relays: [], numUAVP2: i, p2Relays: [], numResources: i, numberOfControlNodes: 0, spreadResources: false, length: Double(10 * (gridX - 1)), width: Double(10 * (gridY - 1)), height: Double(1 * gridZ), communicationRange: communicationRange, communicationMode: .communication, sensors: [], resourceRequiredSensors: [:], uavSensors: [:])
//        let game = generateSimpleGridGame(from: gridGameConfig)
//        
//   
//        let initPlanner = AStarPlanner(mode:.communicationWithCentralNodes , heuristics: { newHeuristics(environment: $0,player: $1, metric: $2)}, costFunction: costFunctionMaxDistSumPen,verbose: false)
//  
//        let bestResponsePlanner = AStarBestResponse(mode:.communicationWithCentralNodes, heuristics: {newHeuristics(environment: $0,player: $1, metric: $2, allDeadlines: $3)}, betterHeuristicsWeight: [1.0], costFunction: costFunctionMaxDistSumPen, verbose: false)
//        
//        
//        
//        let doubleOracleConfiguration = DoubleOracleConfiguration(numberOfInitialPlans: 1, maximumNumberOfAttemptsToImproveEquilibriumApprox: 2, initialPlansPlannerP1: initPlanner, initialPlansPlannerP2: initPlanner, bestResponsePlannerP1: [bestResponsePlanner], bestResponsePlannerP2: [bestResponsePlanner], epsilon: FRASLibConfig.defaultEpsilon, name: "test435435")
//        
//        var doubleOracle = try! DoubleOracleAlgorithm(game: game, configuration: doubleOracleConfiguration, plansCacheDir: nil, verbose: verbose)
//        
//        let equilibriumReached = try! doubleOracle.run(numberOfIterations: 20)
//        
//        XCTAssertTrue(equilibriumReached)
//        let stats = doubleOracle.log.map { $0.statItem }
//        
//        XCTAssert(abs(stats.last!.player1EqVal - 1.5) < 0.25)
//        XCTAssert(abs(stats.last!.player2EqVal - 1.5) < 0.25)
//        
//    }
//    
//    
//    func testAStarCommWithCentralNodes() {
//        // This takes around 3 minutes.
//        let i = 3
//        let verbose = true
//        let gridX = 3 * i + (i % 2 == 0 ? 1 : 0)
//        let gridY = 2 * i + (i % 2 == 0 ? 0 : 1)
//        let gridZ = 1
//        let communicationRange = ceil(sqrt(Double(gridX) / 2 + Double(gridY) / 2 + Double(i) / 2))
//        let gridGameConfig = FRASGridGameGeneratorConfig(x: gridX, y: gridY, z: gridZ, numUAVP1: i, p1Relays: [], numUAVP2: i, p2Relays: [], numResources: i, numberOfControlNodes: 1, spreadResources: false, length: Double(10 * (gridX - 1)), width: Double(10 * (gridY - 1)), height: Double(1 * gridZ), communicationRange: communicationRange, communicationMode: .communicationWithCentralNodes, sensors: [], resourceRequiredSensors: [:], uavSensors: [:])
//        let game = generateSimpleGridGame(from: gridGameConfig)
//        
//     
//        
//        let initPlanner = AStarPlanner(mode:.communicationWithCentralNodes , heuristics: { newHeuristics(environment: $0,player: $1, metric: $2)}, costFunction: costFunctionMaxDistSumPen,verbose: false)
//        
//        let bestResponsePlanner = AStarBestResponse(mode:.communicationWithCentralNodes, heuristics: {newHeuristics(environment: $0,player: $1, metric: $2, allDeadlines: $3)}, betterHeuristicsWeight: [1.0], costFunction: costFunctionMaxDistSumPen, verbose: false)
//        
//        
//       
//        
//        let doubleOracleConfiguration = DoubleOracleConfiguration(numberOfInitialPlans: 1, maximumNumberOfAttemptsToImproveEquilibriumApprox: 2, initialPlansPlannerP1: initPlanner, initialPlansPlannerP2: initPlanner, bestResponsePlannerP1: [bestResponsePlanner], bestResponsePlannerP2: [bestResponsePlanner], epsilon: FRASLibConfig.defaultEpsilon, name: "test22")
//        
//        var doubleOracle = try! DoubleOracleAlgorithm(game: game, configuration: doubleOracleConfiguration, plansCacheDir: nil, verbose: verbose)
//        
//        let equilibriumReached = try! doubleOracle.run(numberOfIterations: 20)
//        
//        XCTAssertTrue(equilibriumReached)
//        let stats = doubleOracle.log.map { $0.statItem }
//        
//        XCTAssert(abs(stats.last!.player1EqVal - 1.5) < 0.25)
//        XCTAssert(abs(stats.last!.player2EqVal - 1.5) < 0.25)
//        
//        print(stats)
//    }

}
