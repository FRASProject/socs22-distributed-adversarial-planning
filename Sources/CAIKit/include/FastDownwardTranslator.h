//
//  File.h
//  
//
//  Created by Pavel Rytir on 9/15/21.
//

#ifndef FastDownwardTranslator_h
#define FastDownwardTranslator_h

#include <stdio.h>

const char* fastdownward_translate(const char* domainPddl, const char* problemPddl, int verbose);

#endif /* FastDownwardTranslator */
