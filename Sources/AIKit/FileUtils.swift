//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/12/20.
//

import Foundation

public enum FileUtils {
  public static func allFilesExists(_ filePaths: [String]) -> Bool {
    filePaths.allSatisfy {FileManager.default.fileExists(atPath: $0)}
  }
  public static func topMostCommonDirectory(_ filePaths: [String]) -> String {
    let commonPath = filePaths.reduce(filePaths.first!, {
      $0.commonPrefix(with: $1)
    })
    if let lastSlash = commonPath.lastIndex(of: "/") {
      return String(commonPath[...lastSlash])
    } else {
      return ""
    }
  }
}
