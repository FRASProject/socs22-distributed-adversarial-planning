//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/21/21.
//

import Foundation

public final class DistributedAdversarialInitOracle: InitOracle {
  public typealias PS = TemporalPlan
  
  private let game: PDDLGameFDRRepresentationAnalyticsLogic
  private let pddlPlanner: PDDLTemporalPlanner
  private let config: AIKitData
  
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    pddlPlanner: PDDLTemporalPlanner,
    config: AIKitData)
  {
    self.game = game
    self.pddlPlanner = pddlPlanner
    self.config = config
  }
  
  public func findPlans(
    for player: Int,
    maxNumberOfPlans: Int,
    plansCacheDir: URL?) throws -> (
      plans: [TemporalPlan],
      log: AIKitData,
      computationDuration: [TimeInterval])
  {
    assert(player == 1 || player == 2)
    let cacheFilePrefix = "player\(player)Init"
    let planCache = plansCacheDir.map {$0.appendingPathComponent(cacheFilePrefix + "jointPlan.sol")}
    let player = player - 1
    let distributedPlanner = try DistributedAdversarialPlanner(
      game: game,
      player: player,
      planner: pddlPlanner,
      costFunction: [:],
      config: config,
      planCacheDir: plansCacheDir,
      cacheFilePrefix: cacheFilePrefix)
    let (plan, planningDuration) = try distributedPlanner.computeAnyPlan()
    let groundingDuration = game.groundingComputationDuration / Double(game.players.count)
    if let planCache = planCache {
      try plan.text.write(to: planCache, atomically: false, encoding: .utf8)
    }
    return (
      [plan],
      .dictionary([
        "log": .string("NA"),
        "planningDuration": .double(planningDuration),
        "groundingDuration": .double(groundingDuration)]),
      [planningDuration + groundingDuration])
  }
  
  public let shortDescription: String = "DistributedAdversarialInitPlanner"
}
