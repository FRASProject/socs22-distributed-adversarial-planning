//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/24/21.
//

import Foundation
//import cwrapper
import temporal_downward
import MathLib

public final class DistributedAdversarialPlanner {
  private let currentPlayerSoftGoalsCriticalActions: [[(
    criticalAction: PlanAction,
    criticalFact: PDDLNameLiteral,
    adversarialActions: Set<PlanAction>
  )]]
  private let goalCriticalActions: [[Int]]
  private let game: PDDLGameFDRRepresentationAnalyticsLogic
  private let player: Int
  private let opponent: Int
  private let unitsAvailableGoals: [Set<Int>]
  private let goalsUnits: [Set<Int>]
  private let allGoalsIndices: Set<Int>
  private let costFunction: [PDDLNameLiteral: [Int]]
  private let config: AIKitData
  private let planCacheDir: URL?
  private let cacheFilePrefix: String
  private let planner: PDDLTemporalPlanner
  private let preprocessorOutput: String
  private var verbose: Bool
  
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    player: Int,
    planner: PDDLTemporalPlanner,
    costFunction: [PDDLNameLiteral: [Int]],
    config: AIKitData,
    planCacheDir: URL?,
    cacheFilePrefix: String) throws
  {
    self.verbose = true
    self.game = game
    self.config = config
    self.planner = planner
    self.costFunction = costFunction
    self.currentPlayerSoftGoalsCriticalActions = game.softGoalsCriticalActions[player]
    self.player = player
    self.planCacheDir = planCacheDir
    self.cacheFilePrefix = cacheFilePrefix
    precondition(player == 0 || player == 1)
    self.opponent = (player + 1) % 2
    let unitsAvailableGoals = game.playersUnits[player].indices.map {
      Set(game.softGoalsCriticalActionsUnits[player][$0].enumerated().filter {!$0.element.isEmpty}.map {$0.offset})
    }
    self.allGoalsIndices = unitsAvailableGoals.reduce(Set<Int>()) {$0.union($1)}
    self.goalsUnits = allGoalsIndices.sorted().map { goalIndex in
      Set(unitsAvailableGoals.enumerated().filter {$0.element.contains(goalIndex)}.map {$0.offset})
    }
    self.unitsAvailableGoals = unitsAvailableGoals
    self.goalCriticalActions = currentPlayerSoftGoalsCriticalActions.map { $0.map {game.groundedPlayersProblems[player].operatorIndex[$0.criticalAction]!}}
    let grounder = FastDownwardGrounder(
      temporal: true,
      runPreprocessing: true)
    let cacheFile = game.groundingCacheDir?.appendingPathComponent("sas_repr_player\(player).json")
    self.preprocessorOutput = try grounder.ground(game.game.createProblemsForGrounding()[player],
    fdrCache: cacheFile)!.preprocessorOutput!
  }
  
  private func findOptimizedOrderGreedy(
    withSimulatedAnnealing: Bool) -> [(unitId: Int, goalsOrder: [Int])]
  {
    let criticalFactsWeights = Dictionary(
      uniqueKeysWithValues: costFunction.map {
        ($0.key, game.getWeight(of: $0.key, for: player))})
    let orderingNotOptimized = SoftGoalsHeuristics.calculateDeleteRelaxationOrderHeuristics(
      factsCostFunction: costFunction,
      criticalFactsWeights: criticalFactsWeights,
      groundedProblem: game.groundedPlayersProblems[player],
      softGoals: game.softGoals[player],
      goalCriticalActionClusters: game.goalCriticalActionClusters[player],
      playersUnits: game.playersUnits[player],
      debugVerbose: false)
    let ordering: RelationDiGraphPath
    if withSimulatedAnnealing {
      guard let startTemperature = config["temperature"]?.double,
            let step = config["temperatureStep"]?.double
      else {
        fatalError("Invalid configuration file of order heur with sim anneal!")
      }
      ordering = SoftGoalsHeuristics.optimizeOrder(
        order: orderingNotOptimized,
        game: game,
        player: player,
        costFunction: costFunction,
        startTemperature: startTemperature,
        temperatureStep: step)
    } else {
      ordering = orderingNotOptimized
    }
    return SoftGoalsHeuristics.convert(
      relation: ordering,
      fdrCoding: game.groundedPlayersProblems[player],
      goalCriticalActions: goalCriticalActions,
      units: game.playersUnits[player])
  }
  
  private func findOptimizedOrderHDTG() -> [(unitId: Int, goalsOrder: [Int])]
  {
    guard let startTemperature = config["temperature"]?.double,
          let step = config["temperatureStep"]?.double,
          let constantK = config["constantK"]?.double
    else {
      fatalError("Invalid configuration file of order heur with sim anneal!")
    }
    temporal_fd_init_all(preprocessorOutput)
    let optimizer = CriticalActionsPartialOrderAbstractionOptimizer(
      game: game,
      player: player,
      costFunction: costFunction,
      currentPlayerSoftGoalsCriticalActions: currentPlayerSoftGoalsCriticalActions,
      orderInitMode: .first)
    let state = optimizer.startSimulatedAnnealing(
      startTemperature: startTemperature,
      temperatureStep: step,
      constantK: constantK,
      verbose: true)
    
    if verbose {
      print("Actions times estimates")
      let actionTimes = CriticalActionsPartialOrderAbstractionOptimizer.actionTimeEstimate(order: state)
      let fdrCoding = game.groundedPlayersProblems[player]
      let planActionTimes = actionTimes.map {
        (fdrCoding.operators[$0.key].asPlanAction.description, "\($0.value)", $0.key)}.sorted(by: {$0.0 < $1.0})
      if let maxWidth = planActionTimes.map({ $0.0.count }).max() {
        planActionTimes.forEach {
          print($0.0.padding(toLength: maxWidth, withPad: " ", startingAt: 0) + " time:" +
                $0.1.padding(toLength: 6, withPad: " ", startingAt: 0) + " index: \($0.2)")
        }
      }
    }
    temporal_fd_deinit()
    return SoftGoalsHeuristics.convert(
      relation: state,
      fdrCoding: game.groundedPlayersProblems[player],
      goalCriticalActions: goalCriticalActions,
      units: game.playersUnits[player])
  }
  
  private func merge(plans: [TemporalPlan]) -> TemporalPlan {
    let (mergedPlan, removedActions) = PlanMerger.mergingWithWaiting(
      plans: plans,
      initState: game.game.problem.initState,
      in: game.game.domain)
    if !removedActions.isEmpty {
      print("Warning. Some action removed in merging")
      print(removedActions)
    }
    plans.enumerated().forEach {
      print("Plan \($0.offset)")
      print($0.element.textNicely)
    }
    print("Merged")
    print(mergedPlan.textNicely)
    
    return mergedPlan
  }
//  private func convert(relation: RelationDiGraphPath) -> [(unitId: Int, goalsOrder: [Int])] {
//    let order = RelationDiGraphPath(graph: relation.graph, transitiveClosure: true)
//    let units = game.playersUnits[player]
//    let fdrCoding = game.groundedPlayersProblems[player]
//    let goalActions = currentPlayerSoftGoalsCriticalActions.map { $0.map {fdrCoding.operatorIndex[$0.criticalAction]!}}
//
//    var unitOrders = [(unitId: Int, goalsOrder: [Int])]()
//    for (unitId, unit) in units.enumerated() {
//      let unitCriticalActions = order.elements.filter {fdrCoding.operators[$0].parameters.contains(unit)}
//      let sortedCriticalAction = unitCriticalActions.sorted(by: {order.isARelatedToB(a: $0, b: $1)})
//      let goalsOrder = sortedCriticalAction.map { criticalAction in
//        goalActions.firstIndex(where: {$0.contains(criticalAction)})!}
//      unitOrders.append((unitId: unitId, goalsOrder: goalsOrder))
//    }
//    return unitOrders
//  }
//  private func findUnitsPlans(order: RelationDiGraphPath) throws -> ([TemporalPlan], [Double]) {
//    let unitOrders = convert(relation: order)
//    return try findUnitsPlans(unitOrders)
//  }
  
  private func findUnitsPlans(_ unitsOrders: [(unitId: Int, goalsOrder: [Int])]) throws -> ([TemporalPlan], [Double]) {
    var plans = [TemporalPlan]()
    var durations = [Double]()
    for (unitId, goalsOrder) in unitsOrders {
      let (plan, _, _, duration) = try findPlan(unitId: unitId, goalsOrder: goalsOrder)
      plans.append(plan)
      durations.append(duration)
    }
    return (plans, durations)
  }
  
  private func findPlan(unitId: Int, goalsOrder: [Int]) throws -> (
    plan:TemporalPlan,
    cost: Double,
    log: String,
    duration: Double)
  {
    if goalsOrder.isEmpty {
      print("Empty goals.")
      return (TemporalPlan(), 0.0, "No goals. Planning skipped", 0.0)
    }
    let problem = game.game.getPlanningProblem(for: player)
          .keepingOnly(player: player)
    let unit = game.playersUnits[player][unitId]
    let (criticalActionFact, orderingConverted) = SoftGoalsHeuristics.convertDataForOrderHeuristics(
      ordering: [unit: goalsOrder],
      game: game,
      player: player)
    let modifiedProblem = problem.restrictingToSingleObjectOf(type: PDDLDomain.unitType, to: unit)
//    let modifiedProblem = problem.restrictingObjectsOf(
//      type: PDDLDomain.unitType,
//      to:[unit])
 //     .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
//      .adding(
//        criticalFactCostFunction: costFunction,
//        criticalFactCriticalActions: game.game.domain.findPossibleCriticalActionsAndFacts())
      .keepingOnlyGoals(goalsOrder)
      .addingOrderHeuristics(
        criticalActionFact: criticalActionFact,
        allCriticalActionNames: Set(game.playerAllCriticalAction[player].map {$0.name}),
        ordering: orderingConverted)
      .convertingSoftGoalsToHardGoalsRemovingMetrics()
      .addingMinimizeTotalTimeMetric()
    let planCache = planCacheDir.map {
      $0.appendingPathComponent("\(cacheFilePrefix)_subplan\(unit).json")}
    let (plans, costs, log, _, durations) = try planner.findFirst(
      problemPDDL: modifiedProblem.problem.text,
      domainPDDL: modifiedProblem.domain.text,
      planCache: planCache)
    if let plan = plans.last {
      return (
        plan.removingParametersWith(prefixes: ["c"]).removingSuffixFromActionNames(),
        costs.last!,
        log,
        durations.last!)
    } else {
      throw AIKitError(message: "Cannot find init plan for unit \(unit)")
    }
  }
  
  private func findInitOrderHDTG() -> [(unitId: Int, goalsOrder: [Int])] {
    let order = CriticalActionsPartialOrderAbstractionOptimizer(
      game: game,
      player: player,
      costFunction: costFunction,
      currentPlayerSoftGoalsCriticalActions: currentPlayerSoftGoalsCriticalActions,
      orderInitMode: .first).initState
    return SoftGoalsHeuristics.convert(
      relation: order,
      fdrCoding: game.groundedPlayersProblems[player],
      goalCriticalActions: goalCriticalActions,
      units: game.playersUnits[player])
  }
  
  public func computeAnyPlan() throws -> (TemporalPlan, Double) {
    precondition(costFunction.isEmpty)
    let order = findInitOrderHDTG()
    let (unitsPlans, unitsDurations) = try findUnitsPlans(order)
    let plan = merge(plans: unitsPlans)
    return (plan, unitsDurations.sum)
  }
  
  public func computeOptimizedPlan() throws -> (
    plan: TemporalPlan,
    planningDuration: Double,
    simAnnDuration: Double)
  {
    precondition(!costFunction.isEmpty)
    let optzer = config["optzer"]?.string ?? ""
    let orderOptimizerStart = Date()
    let order: [(unitId: Int, goalsOrder: [Int])]
    switch optzer {
    case "":
      order = findOptimizedOrderHDTG()
    case "greedy":
      order = findOptimizedOrderGreedy(withSimulatedAnnealing: false)
    case "greedysa":
      order = findOptimizedOrderGreedy(withSimulatedAnnealing: true)
    default:
      fatalError("Invalid distributed planner configuration.")
    }
    let orderOptimizerComputationDuration = -orderOptimizerStart.timeIntervalSinceNow
    let (unitsPlans, unitsDurations) = try findUnitsPlans(order)
    let plan = merge(plans: unitsPlans)
    return (plan, unitsDurations.sum, orderOptimizerComputationDuration)
  }
}
