//
//  File.swift
//  
//
//  Created by Pavel Rytir on 7/23/21.
//

import Foundation

open class StrategiesEvaluatorExperiment<
  GSP1: GameStrategyProfile,
  GSP2: GameStrategyProfile,
  E: PureStrategyEvaluator> : ExperimentProtocol
where
GSP1.MSP1.PS == GSP2.MSP1.PS,
GSP1.MSP1.PS == E.PSP1,
GSP1.MSP2.PS == GSP2.MSP2.PS,
GSP1.MSP2.PS == E.PSP2
{
  public typealias ResultData = ResultArray
  public struct ResultArray: ExperimentData {
    public let values: [String: Result]
    public let state: ExperimentDataItemState
  }
  public struct Result : Codable {
    public let sP1P1xSP2P2ValueP1: Double
    public let sP1P1xSP2P2ValueP2: Double
    public let sP1P2xSP2P1ValueP1: Double
    public let sP1P2xSP2P1ValueP2: Double
  }
  public let strategies1: [(config: ExperimentParameters, fromCache: Bool, data: GSP1)]
  public let strategies2: [(config: ExperimentParameters, fromCache: Bool, data: GSP2)]
  public let evaluator: E
  private let pathFileNameInternal: String
  private let fileNameInfix: String
  private let longestPrefix1: ExperimentIndex
  private let longestPrefix2: ExperimentIndex
  public var parameters: ExperimentParameters
  public var resultData: ResultData?
  public var fromCache: Bool?
  public var failError: Error?
  public let recompute: Bool = false
  public init(
    parameters: ExperimentParameters,
    evaluator: E,
    strategies1: [(config: ExperimentParameters, fromCache: Bool, data: GSP1)],
    strategies2: [(config: ExperimentParameters, fromCache: Bool, data: GSP2)])
  {
    self.strategies1 = strategies1
    self.strategies2 = strategies2
    self.evaluator = evaluator
    self.longestPrefix1 = strategies1.map {$0.config}.reduce(strategies1.first!.config.index) { $0.longestCommonPrefix(with: $1.index) }
    self.longestPrefix2 = strategies2.map {$0.config}.reduce(strategies2.first!.config.index) { $0.longestCommonPrefix(with: $1.index) }
    let commonPrefix = longestPrefix1.longestCommonPrefix(with: longestPrefix2)
    
    let fileNamePart1 = longestPrefix1.subtracting(prefix: commonPrefix).pathItems(filteredKeys: parameters.filteredKeys, onlyValuePathKeys: parameters.onlyValuePathKeys).joined(separator: "+")
    let fileNamePart2 = longestPrefix2.subtracting(prefix: commonPrefix).pathItems(filteredKeys: parameters.filteredKeys, onlyValuePathKeys: parameters.onlyValuePathKeys).joined(separator: "+")
    self.fileNameInfix = fileNamePart1 + "___" + fileNamePart2
    
    let pathItems = commonPrefix.pathItems(
      filteredKeys: parameters.filteredKeys,
      onlyValuePathKeys: parameters.onlyValuePathKeys)
    self.pathFileNameInternal = pathItems.joined(separator: "/")
    self.parameters = parameters
  }
  
  public var cachePathFileName: String {
    pathFileNameInternal + "/" + fileNamePrefix + fileNameInfix + fileNameSufix
  }
  
  public var fileNameSufix: String {
    ".json"
  }
  
  public var fileNamePrefix: String {
    "evaluation"
  }
  
  public func runBody() throws {
    var result = [String: Result]()
    for strategy1 in strategies1 {
      for strategy2 in strategies2 {
        let key1 = strategy1.config.index.subtracting(prefix: longestPrefix1).pathItems(filteredKeys: parameters.filteredKeys, onlyValuePathKeys: parameters.onlyValuePathKeys).joined(separator: "+")
        let key2 = strategy2.config.index.subtracting(prefix: longestPrefix2).pathItems(filteredKeys: parameters.filteredKeys, onlyValuePathKeys: parameters.onlyValuePathKeys).joined(separator: "+")
        let (sp1p1xsp2p2P1Value, sp1p1xsp2p2P2Value) = try NormalFormGame.evaluateStrats(
          evaluator: evaluator,
          player1Plans: strategy1.data.player1.strategies,
          player2Plans: strategy2.data.player2.strategies,
          player1PlansDistribution: strategy1.data.player1.distribution,
          player2PlansDistribution: strategy2.data.player2.distribution,
          outputFileName: nil)
        let (sp1p2xsp2p1P1Value, sp1p2xsp2p1P2Value) = try NormalFormGame.evaluateStrats(
          evaluator: evaluator,
          player1Plans: strategy2.data.player1.strategies,
          player2Plans: strategy1.data.player2.strategies,
          player1PlansDistribution: strategy2.data.player1.distribution,
          player2PlansDistribution: strategy1.data.player2.distribution,
          outputFileName: nil)
        result[key1 + "___" + key2] = Result(
                        sP1P1xSP2P2ValueP1: sp1p1xsp2p2P1Value,
                        sP1P1xSP2P2ValueP2: sp1p1xsp2p2P2Value,
                        sP1P2xSP2P1ValueP1: sp1p2xsp2p1P1Value,
                        sP1P2xSP2P1ValueP2: sp1p2xsp2p1P2Value)
      }
    }
    save(result: ResultArray(values: result, state: .finished))
  }
}
