//
//  File.swift
//  
//
//  Created by Pavel Rytir on 3/15/21.
//

import Foundation

//public enum ExperimentDoublePruningDO {
//  public static func run(with config: [String: AIKitData]) throws {
//    let initExperimentParameters = ExperimentParameters()
//    let games = config["games"]!.array!.map { $0.dictionary! }// as! [[String: Any]]
//    let gameParameters = games.flatMap {
//      initExperimentParameters.merging(from: $0)
//    }
//    let dodpParameters = gameParameters.flatMap {
//      $0.merging(from: config["DoublePruningDoubleOracle"]!.dictionary!)
//    }
//    let dodpExperiments = dodpParameters.map { DoublePruningDoubleOracleExperiment( parameters: $0) }
//    try dodpExperiments.forEach {try $0.run()}
//    
//    let dodpExploitability = dodpExperiments.map {
//      PDDLStrategyExpoitabilityExperiment(
//        strategy: $0.resultData!,
//        strategyState: $0.resultData!.state,
//        parameters: $0.parameters.merging(
//          from: config["Exploitability"]!.dictionary!).first!)
//      
//    }
//    
//    try dodpExploitability.forEach { try $0.runBody() }
//    
//    let doParameters = gameParameters.flatMap {
//      $0.merging(from: config["DoubleOracle"]!.dictionary!)
//    }
//    let doExperiments = doParameters.map { PDDLDoubleOracleExperiment( parameters: $0) }
//    try doExperiments.forEach {try $0.run()}
//    
//    let doExploitability = doExperiments.map {
//      PDDLStrategyExpoitabilityExperiment(
//        strategy: $0.resultData!,
//        strategyState: $0.resultData!.state,
//        parameters: $0.parameters.merging(
//          from: config["Exploitability"]!.dictionary!).first!)
//      
//    }
//    
//    try doExploitability.forEach { try $0.runBody() }
//    
//    let doData = doExperiments.map { (experiment) -> [String: AIKitData] in
//      [
//        "domain": experiment.parameters["DOMAIN"]!,
//        "problem": experiment.parameters["GAME"]!,
//        "algorithm": experiment.parameters["ALG"]!,
//        "version": .string("na"),
//        "heuristics" : experiment.parameters["heuristics"]!,
//        "runningTime": .double(experiment.resultData!.totalTime),
//        "iterations": .integer(experiment.resultData!.statistics.count),
//        "eqSupport": .integer(experiment.resultData!.player1Equilibrium.filter {
//                                $0 > AIKitConfig.defaultEpsilon}.count)
//      ]
//    }
//    
//    let dodpData = dodpExperiments.map { (experiment) -> [String: AIKitData] in
//      [
//        "domain": experiment.parameters["DOMAIN"]!,
//        "problem": experiment.parameters["GAME"]!,
//        "algorithm": experiment.parameters["ALG"]!,
//        "version": experiment.parameters["V"]!,
//        "heuristics": .string("na"),
//        "runningTime": .double(experiment.resultData!.totalTime),
//        "iterations": .integer(experiment.resultData!.statistics.count),
//        "eqSupport": .integer(experiment.resultData!.player1Equilibrium.filter {
//                                $0 > AIKitConfig.defaultEpsilon}.count)
//      ]
//    }
//    let dodpExplTable = dodpExploitability.map { (exploitabilityExperiment) -> [String: AIKitData] in
//      [
//        "p1Exploitability": .double(exploitabilityExperiment.resultData!.exploitability["p1Exploitability"]!),
//        "domain": exploitabilityExperiment.parameters["DOMAIN"]!,
//        "problem": exploitabilityExperiment.parameters["GAME"]!,
//        "algorithm": exploitabilityExperiment.parameters["ALG"]!,
//        "heuristics": .string("na"),
//        "version": exploitabilityExperiment.parameters["V"]!
//      ]}
//    
//    let doExplTable = doExploitability.map { (exploitabilityExperiment) -> [String: AIKitData] in
//      [
//        "p1Exploitability": .double(exploitabilityExperiment.resultData!.exploitability["p1Exploitability"]!),
//        "domain": exploitabilityExperiment.parameters["DOMAIN"]!,
//        "problem": exploitabilityExperiment.parameters["GAME"]!,
//        "algorithm": exploitabilityExperiment.parameters["ALG"]!,
//        "heuristics" : exploitabilityExperiment.parameters["heuristics"]!,
//        "version": .string("na")
//      ]}
//    
//    let table = TableUtils.leftJoin(
//      doData + dodpData,
//      doExplTable + dodpExplTable,
//      on: ["domain", "problem","algorithm","version", "heuristics"])
//    
//    let tableAggregator = AggregateTableGenerator(
//      rows: table,
//      cols: nil,
//      filenamePrefix: "dodp",
//      parameters: ExperimentParameters())
//    try tableAggregator.runBody()
//    
//    
//  }
//}
