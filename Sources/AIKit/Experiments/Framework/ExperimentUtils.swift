//
//  File.swift
//  
//
//  Created by Pavel Rytir on 7/14/21.
//

import Foundation

public enum ExperimentUtils {
  public static func runParallelStr<E:ExperimentProtocol> (
    _ experiments: [E],
    callback: ((AIKitTable<AIKitData>) -> ())? = nil) throws -> [(experiment: String, error: Error)]
  where E.ResultData: ExperimentDataResultDictionary
  {
    if let callback = callback {
      return try runParallel(experiments, callback: { finishedExperiments in
        //let finishedExperiments = finishedExperiments as! [Experiment<D>]
        let table = AIKitTable(rows: finishedExperiments.compactMap { $0.resultDictionary })
        callback(table)
      }).map { ($0.description, $0.failError!) }
    } else {
      return try runParallel(experiments).map { ($0.description, $0.failError!) }
    }
  }
  public static func runParallel<E: ExperimentProtocol>(
    _ experiments: [E],
    callback: (([E]) -> ())? = nil) throws -> [E]
  {
    
    let ntasks = ExperimentManager.defaultManager.maxNumberOfConcurrentTasks
    var finishedExperimentsIndices = [Int]()
//    #if os(Linux)
//    let queue = DispatchQueue(
//      label: "ParallelExperiments",
//      qos: .userInitiated,
//      attributes: .concurrent)
//    let allTasksFinishedGroup = DispatchGroup()
//    let semaphore = DispatchSemaphore(value: ntasks)
//    for experiment in experiments {
//      semaphore.wait()
//      allTasksFinishedGroup.enter()
//      queue.async {
//        try! experiment.start()
//        semaphore.signal()
//        allTasksFinishedGroup.leave()
//      }
//    }
//    allTasksFinishedGroup.wait()
//    #else
    let dataQueue = OperationQueue()
    dataQueue.maxConcurrentOperationCount = 1
    let queue = OperationQueue()
    queue.maxConcurrentOperationCount = ntasks
    experiments.enumerated().forEach { (idx, experiment) in
      queue.addOperation {
        do {
          try experiment.run()
          if let callback = callback {
            dataQueue.addOperation {
              finishedExperimentsIndices.append(idx)
              let finishedExperiments = finishedExperimentsIndices.sorted().map { experiments[$0] }
              print("Queue progress \(finishedExperiments.count)/\(experiments.count)")
              callback(finishedExperiments)
            }
          }
        } catch {
          experiment.failError = error
          print(error)
        }
      }
    }
    queue.waitUntilAllOperationsAreFinished()
    dataQueue.waitUntilAllOperationsAreFinished()
//    #endif
    
    let failedExperiments = experiments.filter {$0.failError != nil}
    
//    if !experiments.allSatisfy({!$0.failed}) {
//      fatalError("Some experiment failed. Implement better handling.")
//    }
    
    print("queue finished. Number of failed experiments: \(failedExperiments.count)")
    return failedExperiments
  }
}
