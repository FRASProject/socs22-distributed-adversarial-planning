//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/16/20.
//

import Foundation

public struct ExperimentDataFilter: Hashable {
  public let data: [String: AIKitData]
  public func projection(on keys: [String]) -> ExperimentDataFilter {
    ExperimentDataFilter(
      data: Dictionary(
        uniqueKeysWithValues: keys.filter{data[$0] != nil}.map {($0, data[$0]!)}))
  }
  public var valuesStringRepresentation: String {
    var keyValues = Array(data)
    keyValues.sort(by: { $0.key < $1.key })
    return keyValues.map {
      $0.value.stringRepresentation}.joined(separator: "_")
  }
  public var shortStringLongNumericRepresentation: String {
    var keyValues = Array(data)
    keyValues.sort(by: { $0.key < $1.key })
    var result = [String]()
    for (_, value) in keyValues {
      switch value {
      case let .double(d):
        result.append(String(d))
      case let .integer(i):
        result.append(String(i))
      case let .bool(b):
        result.append(b ? "T" : "F")
      case .null:
        result.append(".")
      case let .string(s):
        result.append(s)
      default:
        result.append("Invalid")
      }
    }
    return result.joined(separator: "_")
  }
}

extension ExperimentDataFilter: ExpressibleByDictionaryLiteral {
  public init(dictionaryLiteral elements: (String, AIKitData)...) {
    self.data = Dictionary(uniqueKeysWithValues: elements)
  }
}

extension ExperimentParameters {
  public func contains(_ filter: ExperimentDataFilter) -> Bool {
    for (key, value) in filter.data {
      if data[key] != value {
        return false
      }
    }
    return true
  }
  public func createFilter(for keys: [String]) -> ExperimentDataFilter {
    ExperimentDataFilter(
      data: Dictionary(uniqueKeysWithValues: keys.filter{data[$0] != nil}.map {($0, data[$0]!)}))
  }
}
