//
//  File.swift
//  
//
//  Created by Pavel Rytir on 7/24/21.
//

import Foundation

public struct ExperimentIndex: Hashable, Equatable, Codable {
  public let data: [String: AIKitData]
  public let pathKeys: [[String]]
  public init() {
    self.data = [:]
    self.pathKeys = []
  }
  public init(data: [String: AIKitData], pathKeys: [[String]]) {
    precondition(Set(pathKeys.flatMap {$0}) == Set(data.keys))
    self.data = data
    self.pathKeys = pathKeys
  }
  public var indexKeys: [String] {
    pathKeys.flatMap {$0}
  }
  public var stringRepresentation: String {
    indexKeys.map {"\($0)-\(data[$0]!.stringRepresentation)"}.joined(separator: "_")
  }
  public var valuesShortText: String {
    indexKeys.map {
      let s = data[$0]!.stringRepresentation
      return s.prefix(3) + (s.count > 3 ? "\(s.suffix(1))" : "")
    }.joined()
  }
  public subscript(_ key: String) -> AIKitData? {
    data[key]
  }
  public var indexValues: [AIKitData] {
    pathKeys.flatMap {$0}.map {data[$0]!}
  }
  public var levelsCount: Int {
    pathKeys.count
  }
  public var lastLevel: Int {
    pathKeys.count - 1
  }
  
  public var asTable: AIKitTable<AIKitData> {
    AIKitTable(columns: data.mapValues {[$0]})
  }
  
  public func pathItems(
    filteredKeys: Set<String> = [],
    onlyValuePathKeys: Set<String> = []) -> [String]
  {
    pathKeys.indices.map {
      pathItem(
        $0,
        filteredKeys: filteredKeys,
        onlyValuePathKeys: onlyValuePathKeys)
    }
  }
  
  private func pathItem(
    _ n: Int,
    filteredKeys: Set<String> = [],
    onlyValuePathKeys: Set<String> = []) -> String
  {
    pathKeys[n].filter {!filteredKeys.contains($0)}.map {
      if onlyValuePathKeys.contains($0) {
        return data[$0]!.stringRepresentation
      } else {
        return "\($0)-\(data[$0]!.stringRepresentation)"
      }
    }.joined(separator: "_")
  }
  
  public func appendingLevelsKeysSorted(_ levels: [[String: AIKitData]]) -> ExperimentIndex {
    let newLevels = levels.map { $0.map {$0.key}.sorted() }
    var newData = data
    for level in levels {
      newData.merge(level, uniquingKeysWith: { _, b in b })
    }
    return ExperimentIndex(data: newData, pathKeys: pathKeys + newLevels)
  }
  public func addingKeysSorted(
    toLevel level: Int,
    keyValue: [String: AIKitData]) -> ExperimentIndex
  {
    var levelKeys = pathKeys[level]
    levelKeys.append(contentsOf: keyValue.map {$0.key})
    levelKeys.sort()
    var newPathKeys = pathKeys
    newPathKeys[level] = levelKeys
    let newData = data.merging(keyValue, uniquingKeysWith: { _, _ in fatalError()})
    return ExperimentIndex(data: newData, pathKeys: newPathKeys)
  }
  public func longestCommonPrefix(with other: ExperimentIndex) -> ExperimentIndex {
    var prefixPathKeys = [[String]]()
    var commonData = [String: AIKitData]()
    for (currentKeys, otherKeys) in zip(pathKeys, other.pathKeys) {
      var commonKeys = [String]()
      for (key, otherKey) in zip(currentKeys, otherKeys) {
        if key != otherKey || data[key] != other.data[otherKey] {
          return ExperimentIndex(data: commonData, pathKeys: prefixPathKeys)
        }
        commonKeys.append(key)
        commonData[key] = data[key]
      }
      if currentKeys.count != otherKeys.count {
        break
      }
      prefixPathKeys.append(commonKeys)
    }
    return ExperimentIndex(data: commonData, pathKeys: prefixPathKeys)
  }
  public func isPrefix(of other: ExperimentIndex) -> Bool {
    if pathKeys.count > other.pathKeys.count {
      return false
    }
    for (currentKeys, otherKeys) in zip(pathKeys, other.pathKeys) {
      if currentKeys != otherKeys {
        return false
      }
      if !currentKeys.allSatisfy({data[$0] == other.data[$0]}) {
        return false
      }
    }
    return true
  }
  public func subtracting(prefix: ExperimentIndex) -> ExperimentIndex {
    precondition(prefix.isPrefix(of: self))
    let newPathKeys = Array(pathKeys[prefix.pathKeys.count...])
    let newData = data.filter { prefix.data[$0.key] == nil }
    return ExperimentIndex(data: newData, pathKeys: newPathKeys)
  }
  public func removing(key: String) -> ExperimentIndex {
    var newData = data
    newData.removeValue(forKey: key)
    let newPathKeys = pathKeys.map {$0.filter {$0 != key}}
    return ExperimentIndex(data: newData, pathKeys: newPathKeys)
  }
}
