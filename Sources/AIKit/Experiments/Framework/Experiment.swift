//
//  Experiment.swift
//  AIKit
//
//  Created by Pavel Rytir on 9/15/20.
//

import Foundation

@available(*, deprecated)
open class BaseExperiment<D: Codable>: Experiment<D> {
  public override required init(
    parameters: ExperimentParameters) {
      super.init(parameters: parameters)
    }
}

@available(*, deprecated)
open class Experiment<D: Codable>: ExperimentProtocol {
  public typealias ResultData = D
  public let parameters: ExperimentParameters
  public var resultData: D?
  public var failError: Error?
  public var fromCache: Bool?
//  public var subexperiments: [ExperimentProtocol]
//  public var failedSubexperiments: [ExperimentProtocol]
  
  open var fileNamePrefix: String {
    fatalError("Implement me")
  }
  open var fileNameSufix: String {
    fatalError("Implement me")
  }
  
  open var recompute: Bool {
    fatalError("Implement me")
  }
  
  
  
  
  public init(parameters: ExperimentParameters)
  {
    self.parameters = parameters
    self.resultData = nil
    self.failError = nil
//    self.subexperiments = []
//    self.failedSubexperiments = []
  }
  
  
  
  
  
  open func runBody() throws {
    fatalError("Implement me")
  }
  
  
  
  
  
  
  
  
}






public enum ExperimentDataItemState: String, Codable {
  case finished
  case failedDontRecompute
}

public protocol ExperimentDataResultDictionary {
  var asDictionary: [String: AIKitData] { get }
}
extension ExperimentDataResultDictionary {
  public var asTable: AIKitTable<AIKitData> {
    AIKitTable(columns: asDictionary.mapValues {[$0]})
  }
}

public protocol ExperimentData : Codable {
  var state: ExperimentDataItemState { get }
}




