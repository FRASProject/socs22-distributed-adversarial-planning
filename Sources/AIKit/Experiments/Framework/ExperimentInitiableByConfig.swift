//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/23/21.
//

import Foundation

public protocol ExperimentInitiableByConfig: ExperimentProtocol {
  init(parameters: ExperimentParameters)
}

