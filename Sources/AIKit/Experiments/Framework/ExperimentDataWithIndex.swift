//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/23/21.
//

import Foundation

public struct ExperimentDataWithIndex<D> {
  public let index: ExperimentIndex
  public let data: D
  public init(index: ExperimentIndex, data: D) {
    self.index = index
    self.data = data
  }
  public func getIndexValue(for key: String) -> AIKitData? {
    return index[key]
  }
}

extension ExperimentDataWithIndex: Codable where D : Codable {}

extension ExperimentDataWithIndex where D: ExperimentDataResultDictionary {
  public var resultDictionary: [String: AIKitData] {
    let d = data.asDictionary
    let id = index.data
    return id.merging(d, uniquingKeysWith: { a, b in
      if a != b {
        return .array([a,b])
      } else {
        return a
      }
    })
  }
  public func resultDictionaryWithIndexSuffix(
    without prefixIndex: ExperimentIndex? = nil) -> [String: AIKitData]
  {
    let modifiedIndex = prefixIndex != nil ? index.subtracting(prefix: prefixIndex!) : index
    let keySuffix = modifiedIndex.stringRepresentation
    let d = Dictionary(
      uniqueKeysWithValues: data.asDictionary.map {("\($0.key)__\(keySuffix)", $0.value)})
    return d
  }
}

public struct ExperimentDataWithMultiIndex<D> {
  public let indices: [ExperimentIndex]
  public let data: D
  public func getIndexValue(for key: String) -> AIKitData? {
    let values = indices.compactMap {$0[key]}
    if values.isEmpty { return nil }
    if values.count == 1 { return values.first! }
    return .array(values)
  }
}

extension ExperimentDataWithMultiIndex where D: ExperimentDataResultDictionary {
  public func resultDictionaryWithIndexSuffix(
    without prefixIndex: ExperimentIndex? = nil) -> [String: AIKitData]
  {
    let modifiedIndices = prefixIndex != nil ? indices.map {$0.subtracting(prefix: prefixIndex!)} : indices
    let keySuffix = modifiedIndices.map { $0.stringRepresentation }.joined(separator: "__")
    let d = Dictionary(
      uniqueKeysWithValues: data.asDictionary.map {("\($0.key)__\(keySuffix)", $0.value)})
    return d
  }
}

extension ExperimentDataWithMultiIndex: Codable where D : Codable {}
