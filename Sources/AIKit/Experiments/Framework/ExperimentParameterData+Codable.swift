//
//  File.swift
//  
//
//  Created by Pavel Rytir on 6/27/21.
//

import Foundation

extension AIKitData : Codable {
  public func encode(to encoder: Encoder) throws {
    var container = encoder.singleValueContainer()
    switch self {
    case let .integer(value):
      try container.encode(value)
    case let .double(value):
      try container.encode(value)
    case let .bool(value):
      try container.encode(value)
    case let .string(value):
      try container.encode(value)
    case .null:
      try container.encodeNil()
    case let .array(value):
      try container.encode(value)
    case let .dictionary(value):
      try container.encode(value)
    }
  }
  
//  enum CodingKeys: CodingKey {
//    case integer, double, bool, string, null, array, dictionary
//  }
  
  public struct ExperimentParamaterDataDecodingError: Error {}
  
  public init(from decoder: Decoder) throws {
    let container = try decoder.singleValueContainer() //
    if let integer = try? container.decode(Int.self) {
      self = .integer(integer)
    } else if let doubleValue = try? container.decode(Double.self) {
      self = .double(doubleValue)
    } else if let boolValue = try? container.decode(Bool.self) {
      self = .bool(boolValue)
    } else if let stringValue = try? container.decode(String.self) {
      self = .string(stringValue)
    } else if container.decodeNil() {
      self = .null
    } else if let arrayValue = try? container.decode(Array<AIKitData>.self) {
      self = .array(arrayValue)
    } else if let dictionaryValue = try? container.decode(Dictionary<String, AIKitData>.self) {
      self = .dictionary(dictionaryValue)
    } else {
      throw ExperimentParamaterDataDecodingError()
    }
  }
}
