//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/24/21.
//

import Foundation

public protocol FDRPlanner: FDRRawPlanner {
  func getFDRLastPlan() -> (
    [PlannerClassicalOutput],
    [Double],
    String,
    String,
    [Double])
  var mode: PlannerType { get }
}

extension FDRPlanner {
  public func parseFDRPlannerOutput(_ data: ([String], String, String, [Double])) -> (
    [PlannerClassicalOutput],
    [Double],
    String,
    String,
    [Double])
  {
    let parsedPlans = data.0.map {PlanParser.parsePlannerOutput(
      plannerOutput: $0) }
    return (parsedPlans.map {$0.0}, parsedPlans.map {$0.1!}, data.1, data.2, data.3)
//    if let rawPlan = data.0 {
//      let (parsedPlan, cost) = PlanParser.parsePlannerOutput(
//        plannerOutput: rawPlan)
//      return (parsedPlan, cost, data.1, data.2, data.3)
//    } else {
//      return (nil, nil, data.1, data.2, data.3)
//    }
  }
  public func getFDRLastPlan() -> (
    [PlannerClassicalOutput],
    [Double],
    String,
    String,
    [Double]) {
    parseFDRPlannerOutput(getLastRawPlan())
  }
  
  public func findFirst(
    representationFDR: String,
    planCache: URL?) throws ->
  ([PlannerClassicalOutput], [Double], String, String, [Double])
  {
    try parseFDRPlannerOutput(findFirst(
      representationFDR: representationFDR,
                planCache: planCache))
  }
  public func findBest(representationFDR: String, planCache: URL?) throws ->
  ([PlannerClassicalOutput], [Double], String, String, [Double])
  {
    try parseFDRPlannerOutput(findBest(
      representationFDR: representationFDR,
                planCache: planCache))
  }
  
  public func findGoodEnough(
    representationFDR: String,
    isGoodEnoughChecker: (PlannerClassicalOutput) throws -> Bool,
    planCache: URL?) throws ->
  ([PlannerClassicalOutput], [Double], String, String, [Double])
  {
    try start(representationFDR: representationFDR, planCache: planCache)
    while try waitForNextPlan() {
      let plan = getFDRLastPlan()
      if let candidatePlan = plan.0.last, try isGoodEnoughChecker(candidatePlan) {
        try reset()
        return plan
      }
    }
    let plan = getFDRLastPlan()
    try reset()
    return plan
  }
}
