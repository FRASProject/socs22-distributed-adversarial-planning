//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/24/21.
//

import Foundation

public protocol FDRRawPlanner {
  /// Starts plan computation. This function returns immeadiately.
  /// - Parameters:
  ///   - problemPDDL: Problem PDDL.
  ///   - domainPDDL: Domain PDDL.
  ///   - planCache: URL of a file that contains a plan cache. The computed plan will be saved here.
  ///                If the file allready exists and contains a plan for the same problem and domain
  ///                PDDLs. Then planner is not started and the content of the cache is used as a plan.
  /// - Throws: PlannerError if the planner could not be started.
  ///
  func start(representationFDR: String, planCache: URL?) throws
  /// Saves the computed plan into cache, if the cache was set, and reset the planner and prepares it for a new computation.
  func reset() throws
  func waitForNextPlan() throws -> Bool
  var shortDescription: String { get }
  func getLastRawPlan() -> (
    plan: [String],
    log: String,
    errorLog: String,
    duration: [Double])
}

extension FDRRawPlanner {
  public func findFirst(representationFDR: String, planCache: URL?) throws ->
  (plan: [String],
   log: String,
   errorLog: String,
   duration: [Double])
  {
    try start(representationFDR: representationFDR, planCache: planCache)
    if try waitForNextPlan() {
      let plan = getLastRawPlan()
      try reset()
      return plan
    } else {
      let (_, log, errorLog, duration) = getLastRawPlan()
      try reset()
      return ([], log, errorLog, duration)
    }
  }
  public func findBest(representationFDR: String, planCache: URL?) throws ->
    ([String], String, String, [Double])
  {
    try start(representationFDR: representationFDR, planCache: planCache)
    while try waitForNextPlan() {}
    let plan = getLastRawPlan()
    try reset()
    return plan
  }
}
