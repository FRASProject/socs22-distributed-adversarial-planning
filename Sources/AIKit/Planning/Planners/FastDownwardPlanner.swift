//
//  FastDownwardPlanner.swift
//  Planner
//
//  Created by Pavel Rytir on 12/11/19.
//

import Foundation
import MathLib

/// Provides an API for execution of FastDownward planner.
open class FastDownwardPlanner: PDDLPlanner, FDRPlanner {
  public enum State {
    case iddle, running, finished, failed
  }
  
  public private(set) var computationStart: Date
  public private(set) var state: State
  public private(set) var lastPlan: String?
  public private(set) var plans: [String]
  public private(set) var plansComputationDurations: [Double]
  private var planFilesRead: Set<String>
  public private(set) var lastLog: String?
  public private(set) var lastErrorLog: String?
  public private(set) var computationDuration: Double?
  public private(set) var planCache: URL?
  public let plannerConfig: Config
  public let mode: PlannerType = .classical
  public var verbose: Bool
  public private(set) var anytime: Bool
  public var maxTime: Int?
  public private(set) var searchStringType: String
  public private(set) var searchString: String
  public private(set) var searchParameters: [String]
  private var tempDirURL: URL?
  private var problemPDDL: String?
  private var domainPDDL: String?
  private var representationFDR: String?
  private var proc: Process!
  private var outPipeHandle: FileHandle?
  public let planFilenamePrefix: String
  public static var fastDownwardPlanFileName = "plan_problem.pddl.SOL"
  public static var symKPlanFileName = "sas_plan"
  //private var previousPlannerOutputs: Set<String>
  private var waitForNextWillReturnCache: Bool
  
  /// Returns an initialized FastDownwardPlanner class.
  /// - Parameters:
  ///   - searchString: FastDownward's search string or allias name.
  ///   - searchStringType: Specify if searchString is a search string or allias name.
  ///   - plannerConfig: Config that contains path to FastDownward planner.
  ///   - anytime: Anytime planner returns plans during the compution.
  ///              Not anytime planner returns plan after it finishes.
  ///   - maxTime: Maximum computation time. After this time the planner is terminated.
  ///   - verbose: Verbose.
  public init(
    searchString:String,
    searchStringType : String,
    searchParameters: [String],
    planFilenamePrefix: String,
    plannerConfig : Config,
    anytime: Bool,
    maxTime: Int,
    verbose : Bool = false)
  {
    self.verbose = verbose
    tempDirURL = nil
    self.anytime = anytime
    self.maxTime = maxTime
    self.planFilenamePrefix = planFilenamePrefix
    self.searchString = searchString
    self.searchStringType = searchStringType
    self.searchParameters = searchParameters
    precondition(searchStringType.isEmpty != searchParameters.isEmpty, "Use either searchString or searchParameters. Not both.")
    precondition(searchStringType == "ALIAS" || searchStringType == "SEARCH" || !searchParameters.isEmpty)
    self.plannerConfig = plannerConfig
    self.computationStart = Date()
    self.plans = []
    self.plansComputationDurations = []
    self.planFilesRead = []
    self.state = .iddle
    self.lastPlan = nil
    self.computationDuration = nil
    self.planCache = nil
    self.lastLog = nil
    self.lastErrorLog = nil
    self.waitForNextWillReturnCache = false
  }
  
  public var computationDeadline: Double? {
    guard let maxTime = maxTime else {
      return nil
    }
    return Double(maxTime)
  }
  
  private var isAfterDeadline: Bool {
    guard let computationDeadline = computationDeadline else {
      return false
    }
    return -computationStart.timeIntervalSinceNow > computationDeadline
  }
  
  public func start(representationFDR: String, planCache: URL?) throws {
    guard state != .running else {
      fatalError("Planner already started!")
    }
    self.representationFDR = representationFDR
    if let planCache = planCache,
       let (plannerOutput, duration, log) = FDRCache.loadPlannerOutputFromCache(
        representationFDR: representationFDR,
        planCache: planCache)
    {
      lastPlan = plannerOutput
      state = .finished
      computationDuration = duration
      lastLog = log
      lastErrorLog = "plan loaded from cache - no error log implemented - todo"
      waitForNextWillReturnCache = true
      return
    }
    self.planCache = planCache
    let temporaryDirectory = "tmp_" + UUID().uuidString
    if verbose {
      print("TEMP1 " + temporaryDirectory)
    }
    try FileManager.default.createDirectory(
      atPath: temporaryDirectory,
      withIntermediateDirectories: false)
    self.tempDirURL = URL(fileURLWithPath: temporaryDirectory, isDirectory: true)
    //print(temporaryDirectory)
    try representationFDR.write(
      to: self.tempDirURL!.appendingPathComponent("output.sas"),
      atomically: true,
      encoding: String.Encoding.utf8)
    proc = Process()
    proc.executableURL = URL(fileURLWithPath: plannerConfig.plannerExecutablePath)
    proc.arguments = self.searchParameters
    print(plannerConfig.plannerExecutablePath)
    print(self.searchParameters)
    proc.currentDirectoryURL = self.tempDirURL
    let pipe = Pipe()
    proc.standardInput = pipe
    //verbose = true
    if !verbose {
      proc.standardOutput = FileHandle.nullDevice
    } else {
      outPipeHandle = nil
    }
    do {
      try proc.run()
    } catch {
      self.state = .failed
      throw error
    }
    self.computationStart = Date()
    self.state = .running
    let writeHandle = pipe.fileHandleForWriting
    let dataChunks = representationFDR.split(separator: "\n").map {"\($0)\n".data(using: .utf8)!}
    var dataLine = 0
    writeHandle.writeabilityHandler = {
      do {
        if dataLine < dataChunks.count  {
          try $0.write(contentsOf: dataChunks[dataLine])
          dataLine += 1
        } else {
          try $0.close()
        }
      } catch {
        print("Warning: not all data sent to the planner. Sent \(dataLine)/\(dataChunks.count) lines.")
        dataLine = dataChunks.count
      }
    }
    if verbose {
      print("started FDR (FastDownwardPlanner.swift)")
    }
  }
  
  
  /// Starts FastDownward planner. After the start the function returns imeadiately.
  /// - Parameters:
  ///   - problemPDDL: Problem PDDL
  ///   - domainPDDL: Domain PDDL
  ///   - planCache: URL of file where to store computed plan. If there is a cache that
  ///                match problem and domain PDDLs. The planner is not started and
  ///                the plan is loaded from the cache.
  public func start(problemPDDL: String, domainPDDL: String, planCache: URL?) throws {
    guard state != .running else {
      fatalError("Planner already started!")
    }
    self.problemPDDL = problemPDDL
    self.domainPDDL = domainPDDL
    if let planCache = planCache,
       let (plannerOutput, duration, log) = PDDLCache.loadPlannerOutputFromCache(
        pddl: problemPDDL,
        domainPDDL: domainPDDL,
        planCache: planCache)
    {
      lastPlan = plannerOutput
      plans = [plannerOutput]
      state = .finished
      computationDuration = duration
      plansComputationDurations = [duration]
      lastLog = log
      lastErrorLog = "plan loaded from cache - no error log implemented - todo"
      waitForNextWillReturnCache = true
      return
    }
    self.planCache = planCache
    let temporaryDirectory = "tmp_" + UUID().uuidString
    if verbose {
      print("TEMP1 " + temporaryDirectory)
    }
    try FileManager.default.createDirectory(
      atPath: temporaryDirectory,
      withIntermediateDirectories: false)
    self.tempDirURL = URL(fileURLWithPath: temporaryDirectory, isDirectory: true)
    try problemPDDL.write(
      to: self.tempDirURL!.appendingPathComponent("problem.pddl"),
      atomically: true,
      encoding: String.Encoding.utf8)
    try domainPDDL.write(
      to: self.tempDirURL!.appendingPathComponent("domain.pddl"),
      atomically: true,
      encoding: String.Encoding.utf8)
    
    computationStart = Date()
    proc = Process()
    proc.executableURL = URL(fileURLWithPath: plannerConfig.plannerExecutablePath)
    proc.arguments = [tempDirURL!.path, searchStringType, searchString, String(verbose ? 1 : 0)]
    proc.currentDirectoryURL = self.tempDirURL
    if verbose {
      print("TEMP2 " + self.tempDirURL!.path)
    }
    do {
      try proc.run()
    } catch {
      self.state = .failed
      throw error
    }
    self.state = .running
    if verbose {
      print("started (FastDownwardPlanner.swift)")
    }
  }
  
  /// Saves the computed plan into cache, if the cache was set, and reset the planner and prepares it for a new computation.
  public func reset() throws {
    if verbose {
      print("reset (FastDownwardPlanner.swift)")
    }
    switch self.state {
    case .iddle:
      return
    case .running:
      if proc.isRunning {
        try self.terminateProcess()
      }
      fallthrough
    case .finished:
      if let planCache = planCache, lastPlan != nil, problemPDDL != nil {
        try PDDLCache.savePlannerOutputToCache(
          pddl: problemPDDL!,
          domainPDDL: domainPDDL!,
          plannerOutput: lastPlan!,
          computationDuration: computationDuration!,
          plannerLog: lastLog!,
          plannerErrorOutput: lastErrorLog!,
          planCache: planCache)
      } else if let planCache = planCache, lastPlan != nil, representationFDR != nil {
        try FDRCache.savePlannerOutputToCache(
          representationFDR: representationFDR!,
          plannerOutput: lastPlan!,
          computationDuration: computationDuration!,
          plannerLog: lastLog!,
          plannerErrorOutput: lastErrorLog!,
          planCache: planCache)
      }
      fallthrough
    case .failed:
      self.problemPDDL = nil
      self.domainPDDL = nil
      self.representationFDR = nil
      if let tempDirUrl = self.tempDirURL {
        try? FileManager.default.removeItem(at: tempDirUrl)
      }
      self.tempDirURL = nil
      self.plans = []
      self.plansComputationDurations = []
      self.planFilesRead = []
      self.state = .iddle
      self.lastPlan = nil
      self.lastLog = nil
      self.computationDuration = nil
      self.planCache = nil
    }
  }
  
  private func checkForSolution() throws -> String? {
    let files = try FileManager.default.contentsOfDirectory(atPath: self.tempDirURL!.path).filter {
      $0.hasPrefix(planFilenamePrefix)
    }
    let newFiles = files.filter {!self.planFilesRead.contains($0)}.sorted()
    
    for newFile in newFiles {
      let plan = try String(
        contentsOf: self.tempDirURL!.appendingPathComponent(newFile),
        encoding: String.Encoding.utf8)
      self.plans.append(plan)
      self.plansComputationDurations.append(-self.computationStart.timeIntervalSinceNow)
      self.planFilesRead.insert(newFile)
    }
    
    if let best = newFiles.last {
      return try String(
        contentsOf: self.tempDirURL!.appendingPathComponent(best),
        encoding: String.Encoding.utf8)
    } else {
      return nil
    }
  }
  
  
  /// Wait until the planner finds a new plan.
  /// - Returns: True if new plan is available. False if the planner terminated and did not find a plan.
  public func waitForNextPlan() throws -> Bool {
    if waitForNextWillReturnCache {
      self.waitForNextWillReturnCache = false
      return true
    }
    
    guard self.state != .iddle else {
      fatalError("Invalid planner state")
    }
    
    if self.state != .running {
      return false
    }
    
    if self.anytime {
      while proc.isRunning && !self.isAfterDeadline {
        if let solution = try checkForSolution() {
          self.lastPlan = solution
          (self.lastLog,self.lastErrorLog) = loadLogs()
          self.computationDuration = -computationStart.timeIntervalSinceNow
          return true
        }
        Thread.sleep(forTimeInterval: 1)
      }
    } else {
      while proc.isRunning && !self.isAfterDeadline {
        Thread.sleep(forTimeInterval: 1)
      }
    }
    
    if !proc.isRunning && proc.terminationStatus != 0 {
      self.state = .failed
      throw Error(
        message: "Planner returned non-zero code: \(proc.terminationStatus)",
        errorCode: Int(proc.terminationStatus))
    }
    
    if proc.isRunning && self.isAfterDeadline {
      try self.terminateProcess()
    }
    self.state = .finished
    let solution = try checkForSolution()
    if solution != nil {
      self.computationDuration = -computationStart.timeIntervalSinceNow
      self.lastPlan = solution
      (self.lastLog,self.lastErrorLog) = loadLogs()
      //try? FileManager.default.removeItem(at: self.tempDirURL!)
      return true
    } else {
      return false
    }
  }
  
  private func loadLogs() -> (String, String) {
    let log = (try? String(
      contentsOf: self.tempDirURL!.appendingPathComponent("plannerFRASLog.txt"),
      encoding: String.Encoding.utf8)) ??
    "Logs not found - todo implement for docker."
    let plannerErrorOutput = (try? String(
      contentsOf: self.tempDirURL!.appendingPathComponent("plannerFRASLogErr.txt"))
      ) ?? "No log"
    return (log,plannerErrorOutput)
  }
  
  /// Returns the last found plan. It return nil if planner have not found any plan.
  public func getLastRawPlan() -> (
    plan: [String],
    log: String,
    errorLog: String,
    duration: [Double]) {
    //if let plan = self.lastPlan {
    if !plans.isEmpty {
      return (plans, lastLog!, lastErrorLog!, plansComputationDurations)
    } else {
      let elapsedTime = -self.computationStart.timeIntervalSinceNow
      let (log, errorLog) = loadLogs()
      return ([], log, errorLog, [elapsedTime])
    }
  }
  
  public var shortDescription: String {
    return "FastDownward \(searchString) Timeout \(maxTime ?? -1)"
  }
  
  private func terminateProcess() throws {
    if verbose {
      print("Attempting to stop the planner \(Date()) - FastDownwardPlanner.swift")
      print("pid: \(self.proc.processIdentifier)")
    }
    #if os(Linux)
    if verbose {
      print("Sending term signal (FastDownwardPlanner.swift) to \(self.proc.processIdentifier)")
    }
    kill(self.proc.processIdentifier, SIGTERM) // proc.terminate does not work correctly on linux.
    proc.terminate()
    #else
    proc.terminate()
    #endif
    Thread.sleep(forTimeInterval: 1)
    
    // Waiting at most 180 sec for planner to be terminated.
    for _ in 1...72 {
      if !proc.isRunning {
        break
      }
      Thread.sleep(forTimeInterval: 5)
    }
    
    if proc.isRunning {
      fatalError("Could not terminate the planner!")
      // Makes no sense to do other experiments, since the running planner is using resources.
      //throw Error(message: "Could not terminate the planner!")
    }
    if verbose {
      print("Planner terminated at \(Date())")
    }
  }
  
  /// Contains paths to fastdownward executables.
  public struct Config : Codable {
    public init(
      plannerExecutablePath: String,
      anyTimePlanCheckerExecutablePath: String)
    {
      self.plannerExecutablePath = plannerExecutablePath
      self.anyTimePlanCheckerExecutablePath = anyTimePlanCheckerExecutablePath
    }
    public let plannerExecutablePath : String
    public let anyTimePlanCheckerExecutablePath : String
  }
  
  /// An error thrown by FastDownward planner.
  public struct Error : PDDLPlannerError {
      public init(message: String) {
          self.message = message
          self.errorCode = nil
      }
      public init(message: String, errorCode: Int) {
          self.message = message
          self.errorCode = errorCode
      }
      public let message: String
      public let errorCode: Int?
  }
}
