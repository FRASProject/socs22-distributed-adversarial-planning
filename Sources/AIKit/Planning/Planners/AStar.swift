//
//  AStarGraph.swift
//  MathLib
//
//  Created by Pavel Rytir on 5/6/19.
//

import Foundation
import MathLib

public func findPlanAStar<P: PlanningProblem, H: PlanningHeuristic>(
  problem: P,
  heuristic: H,
  pruneFunction: ((P.State) -> Bool)?,
  continueFunction: (() -> Bool)? = nil,
  checkContinueCount: Int? = nil,
  maxSetSize: Int = 2_000_000,
  verbose: Bool = false) -> (path: [P.Action], solutionCost: Double, log: AStarLog)?
  where P.State == H.State
{
  var finishedNodes = Set<P.State>()
  var fScore = [P.State: Double]()
  var gScore = [P.State: Double]()
  var predecesor = [P.State: (P.State,P.Action)]()
  var openSet = OpenMinSet<P.State, Double>()
  var heapOrderId = 0
  var maxFrontierSize = 0
  var numberOfIterations = 0
  var numberOfInnerIterations = 0
  let initNodeFScore = heuristic.estimate(problem.initState)
  openSet.push(problem.initState, score: initNodeFScore)
  maxFrontierSize += 1
  heapOrderId += 1
  fScore[problem.initState] = initNodeFScore
  gScore[problem.initState] = 0.0
  
  let verbose = true
  
  while let current = openSet.pop() {
    
    maxFrontierSize = max(maxFrontierSize, openSet.count + 1)
    
    if numberOfIterations % 1000 == 0 {
      if finishedNodes.count > maxSetSize || openSet.count > maxSetSize {
        print("Reached maximum set size \(maxSetSize). Terminating.")
        return nil
      }
    }
    
    if verbose && numberOfIterations % 100 == 0 {
      print("Iteration: \(numberOfIterations) Finished set size: \(finishedNodes.count) Frontier size: \(openSet.count) F-score: \(fScore[current]!) Cost: \(gScore[current]!)")
      let h = heuristic.estimate(current)
      print(h)
    }
    
    if let checkContinueCount = checkContinueCount,
      numberOfIterations % checkContinueCount == 0,
      let continueFunction = continueFunction,
      !continueFunction()
    {
      if verbose {
        print("ASTAR planner terminated.")
      }
      return nil
    }
    
    if problem.isFinal(current) {
      let finalCost = gScore[current]!
      if verbose {
        print("Final state found")
        print("Iterations: \(numberOfIterations) Inner iterations, \(numberOfInnerIterations) Finished set size: \(finishedNodes.count) Frontier size: \(openSet.count) F-score: \(fScore[current]!) Cost: \(finalCost)")
      }
      let log = AStarLog(
        closedSetSize: finishedNodes.count,
        maxOpenSetSize: maxFrontierSize,
        numberOfIterations: numberOfIterations,
        numberOfInnerIterations: numberOfIterations,
        solutionCost: gScore[current]!,
        solutionFScore: fScore[current]!)
      return (
        path: reconstructPath(to: current, predecesor: predecesor),
        solutionCost: finalCost,
        log: log)
    }
    finishedNodes.insert(current)
    numberOfIterations += 1
    
    if let pruneFunction = pruneFunction, pruneFunction(current) {
      continue
    }
    
    let actions = problem.availableActions(at: current)
    for action in actions {
      numberOfInnerIterations += 1
      let neighbor = problem.apply(action: action, on: current)
      let transitionCost = problem.cost(of: action)
      let contained = finishedNodes.contains(neighbor)
      if !contained {
        let tentativeGScore = gScore[current]! + transitionCost
        if tentativeGScore < gScore[neighbor] ?? Double.infinity {
          gScore[neighbor] = tentativeGScore
          predecesor[neighbor] = (current, action)
          let heuristicValue = heuristic.estimate(neighbor)
          let newFScore = tentativeGScore + heuristicValue
          fScore[neighbor] = newFScore
          if openSet.contains(neighbor) {
            openSet.decreaseKey(neighbor, to: newFScore)
          } else {
            openSet.push(neighbor, score: newFScore)
            heapOrderId += 1
          }
        }
      }
    }
  }
  return nil
}

fileprivate func reconstructPath<S: PlanningState, A: PlanActionProtocol>(
  to state: S,
  predecesor: [S: (S,A)]) -> [A]
{
  var path = [A]()
  var current = state
  while let (previousNode, actionsTo) = predecesor[current] {
    path.append(actionsTo)
    current = previousNode
  }
  path.reverse()
  return path
}

public struct AStarLog: CustomStringConvertible {
  public let closedSetSize : Int
  public let maxOpenSetSize : Int
  public let numberOfIterations : Int
  public let numberOfInnerIterations : Int
  public let solutionCost : Double
  public let solutionFScore : Double
  public var description: String {
    return "Iterations, \(numberOfIterations), Inner iterations, \(numberOfInnerIterations), Finished set size, \(closedSetSize), Max Frontier size, \(maxOpenSetSize), F-score, \(solutionFScore), Cost, \(solutionCost)"
  }
}
