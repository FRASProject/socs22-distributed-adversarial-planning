//
//  PDDLPlannerFactory.swift
//  AIKit
//
//  Created by Pavel Rytir on 7/18/20.
//

import Foundation

public enum PDDLPlannerFactory {
  
  public static func makeTemporal(
    by config: AIKitData,
    frasConfig: [String: AIKitData]) throws -> PDDLTemporalPlanner
  {
    guard let name = config["pddlPlannerName"]?.string else {
      throw AIKitError(message:"Invalid configuration. Missing planner name.")
    }
    switch name {
    case "CPT5":
      guard let cpt5config = frasConfig["CPT5"]?.dictionary,
            let plannerExecutablePath = cpt5config["plannerExecutablePath"]?.string,
            let maxTime = config["maxTime"]?.integer else {
        throw AIKitError(message: "Invalid FRAS config file")
      }
      let verbose = config["verbose"]?.bool ?? false
      let cpt5Planner = CPT5Planner(
        plannerExecutablePath: plannerExecutablePath,
        maxTime: maxTime,
        verbose: verbose)
      return cpt5Planner
    default:
      throw AIKitError(message: "Unknown planner")
    }
  }
  
  /// Makes a PDDLPlanner specified by a config.
  /// - Parameters:
  ///   - config: Config describing the planner.
  ///   - frasConfig: General FRAS configuration with paths to executables.
  public static func make(
    by config: AIKitData,
    frasConfig: [String: AIKitData],
    fastDownwardAliases: [String: [String]]
  ) throws -> PDDLPlanner
  {
    guard let name = config["pddlPlannerName"]?.string else {
      throw AIKitError(message:"Invalid configuration. Missing planner name.")
    }
    switch name {
    case "FASTDOWNWARD", "FASTDOWNWARDNG":
      guard let maxTime = config["maxTime"]?.integer,
            let searchStringAbv = frasConfig["FASTDOWNWARDSEARCHSTRINGS"]?.dictionary?.mapValues({ $0.string!}),
            let anyTime = config["anytime"]?.bool
      else
      {
        throw AIKitError(message: "Invalid config - missing maxtime.")
      }
      let verbose = config["verbose"]?.bool ?? false
      if name == "FASTDOWNWARD" {
        let searchString: String
        let searchStringType: String
        if let searchStrKey = config["searchString"]?.string {
          searchString = searchStringAbv[searchStrKey]!
          searchStringType = "SEARCH"
        } else if let alias = config["alias"]?.string {
          searchString = alias
          searchStringType = "ALIAS"
        } else {
          throw AIKitError(message:"Invalid config. Specify search string or alias.")
        }
        let fdConfig = try scanFrasConfigForFastDownwardConfig(
          frasConfig["FASTDOWNWARD"]!.dictionary!)
        let fastDownwardplanner = FastDownwardPlanner(
          searchString: searchString,
          searchStringType: searchStringType,
          searchParameters: [],
          planFilenamePrefix: FastDownwardPlanner.fastDownwardPlanFileName,
          plannerConfig: fdConfig,
          anytime: anyTime,
          maxTime: maxTime,
          verbose: verbose)
        return fastDownwardplanner
      } else {
#if DISABLE_CFASTDOWNWARD
        fatalError("Not compiled with FASTDOWNWARDNG support.")
#else
        let aliasKey = config["PlAl"]!.string!
        let searchParameters = fastDownwardAliases[aliasKey] ??
        (aliasKey == "ASTARPOTENT1" ?
         FastDownwardPlannerNG.astarPotentParameters : nil)
        let fastDownwardplanner = FastDownwardPlannerNG(
          searchParameters: searchParameters!,
          anytime: anyTime,
          maxTime: maxTime,
          verbose: verbose)
        return fastDownwardplanner
#endif
      }
    default:
      throw AIKitError(message: "Unknown planner")
    }
  }
  
  /// Makes config for FastDownward
  /// - Parameter config: FRAS config with paths to executables.
  static private func scanFrasConfigForFastDownwardConfig(_ config: [String: AIKitData]) throws ->
  FastDownwardPlanner.Config {
    guard let plannerExecutablePath = config["plannerExecutablePath"]?.string,
          let anyTimePlanCheckerExecutablePath = config["anyTimePlanCheckerExecutablePath"]?.string else
    {
      throw AIKitError(message: "Invalid FRAS config file")
    }
    let config = FastDownwardPlanner.Config(
      plannerExecutablePath: plannerExecutablePath,
      anyTimePlanCheckerExecutablePath: anyTimePlanCheckerExecutablePath)
    return config
  }
  
}

