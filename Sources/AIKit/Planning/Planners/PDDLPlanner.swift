//
//  PDDLPlanner.swift
//  Planner
//
//  Created by Pavel Rytir on 1/10/20.
//

import Foundation

/// PDDLPlanner finds a plan for a planning problem that is specified by a problem and domain PDDLs.
public protocol PDDLPlanner: PDDLRawPlanner {
  func start(problem: PDDLPlanningProblem, planCache: URL?) throws
  func getLastPlan() -> (
    [PlannerClassicalOutput],
    [Double],
    String,
    String,
    [Double])
  var mode: PlannerType { get }
}

public enum PlannerType {
  case temporal, classical
}

extension PDDLPlanner {
  public func start(problem: PDDLPlanningProblem, planCache: URL?) throws {
    try start(problemPDDL: problem.problem.text, domainPDDL: problem.domain.text, planCache: planCache)
  }
  private func parse(_ data: ([String], String, String, [Double])) -> (
    [PlannerClassicalOutput],
    [Double],
    String,
    String,
    [Double])
  {
    let parsedPlans = data.0.map {PlanParser.parsePlannerOutput(
      plannerOutput: $0) }
    return (parsedPlans.map {$0.0}, parsedPlans.map{$0.1!}, data.1, data.2, data.3)
  }
  public func getLastPlan() -> (
    [PlannerClassicalOutput],
    [Double],
    String,
    String,
    [Double]) {
    parse(getLastRawPlan())
  }
  
  public func findFirst(
    problemPDDL: String,
    domainPDDL: String,
    planCache: URL?) throws ->
  ([PlannerClassicalOutput], [Double], String, String, [Double])
  {
    try parse(findFirst(
                problemPDDL: problemPDDL,
                domainPDDL: domainPDDL,
                planCache: planCache))
  }
  public func findBest(problemPDDL: String, domainPDDL: String, planCache: URL?) throws ->
  ([PlannerClassicalOutput], [Double], String, String, [Double])
  {
    try parse(findBest(
                problemPDDL: problemPDDL,
                domainPDDL: domainPDDL,
                planCache: planCache))
  }
  
  public func findGoodEnough(
    problemPDDL: String,
    domainPDDL: String,
    isGoodEnoughChecker: (PlannerClassicalOutput) throws -> Bool,
    planCache: URL?) throws ->
  ([PlannerClassicalOutput], [Double], String, String, [Double])
  {
    try start(problemPDDL: problemPDDL, domainPDDL: domainPDDL, planCache: planCache)
    while try waitForNextPlan() {
      let plan = getLastPlan()
      if let candidatePlan = plan.0.last, try isGoodEnoughChecker(candidatePlan) {
        try reset()
        return plan
      }
    }
    let plan = getLastPlan()
    try reset()
    return plan
  }
}
