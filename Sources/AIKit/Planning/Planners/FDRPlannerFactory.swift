//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/24/21.
//

import Foundation

public enum FDRPlannerFactory {
  public static func make(
    by config: AIKitData,
    frasConfig: [String: AIKitData],
    fastDownwardAliases: [String: [String]]
  ) throws -> FDRPlanner
  {
    guard let name = config["pddlPlannerName"]?.string,
          let maxTime = config["maxTime"]?.integer,
          let anyTime = config["anytime"]?.bool else
          {
            throw AIKitError(message:"Invalid configuration. Missing planner name.")
          }
    let verbose = config["verbose"]?.bool ?? false
    switch name {
    case "FDSYMK":
      guard let topK = config["topK"]?.integer else {
        throw AIKitError(message:"Invalid configuration. Missing planner name.")
      }
      let symkPlanner = FastDownwardPlanner(
        searchString: "",
        searchStringType: "",
        searchParameters: [
          "--search",
          "symk-bd(plan_selection=unordered(num_plans=\(topK)))",
          //"symk-bd(plan_selection=top_k(num_plans=\(topK)))"
        ],
        planFilenamePrefix: FastDownwardPlanner.symKPlanFileName,
        plannerConfig: FastDownwardPlanner.Config(
          plannerExecutablePath: frasConfig["FDSYMK"]!["plannerExecutablePath"]!.string!,
          anyTimePlanCheckerExecutablePath: "NA"),
        anytime: anyTime,
        maxTime: maxTime,
        verbose: verbose)
      return symkPlanner
    case "FASTDOWNWARDNG":
#if DISABLE_CFASTDOWNWARD
        fatalError("Not compiled with FASTDOWNWARDNG support.")
#else
      let aliasKey = config["PlAl"]!.string!
      let searchParameters = fastDownwardAliases[aliasKey] ??
      (aliasKey == "ASTARPOTENT1" ?
       FastDownwardPlannerNG.astarPotentParameters : nil)
      let fastDownwardplanner = FastDownwardPlannerNG(
        searchParameters: searchParameters!,
        anytime: anyTime,
        maxTime: maxTime,
        verbose: verbose)
      return fastDownwardplanner
#endif
    default:
      throw AIKitError(message: "Unknown planner")
    }
  }
}
