//
//  PDDLParser.swift
//  AIKit
//
//  Created by Pavel Rytir on 8/29/20.
//

import Foundation

public enum PlanParser {
  public static func removeComments(_ s: String) -> String {
    var lines = [String]()
    for line in s.components(separatedBy: "\n") where !line.isEmpty && !line.starts(with: ";") {
      lines.append(line)
    }
    return lines.joined(separator: "\n")
  }
  
  public static func convertToRawPlanWithTimestamps(_ plan: TemporalPlan, t1: Int) -> String {
    var lines = [String]()
    for action in plan.actions {
      precondition(action.start.isMultiple(of: t1))
      precondition(action.duration.isMultiple(of: t1))
      let start = action.start / t1
      let duration = action.duration / t1
      let line = "(" + action.name + " " + action.parameters.joined(separator: " ") +
        " t\(start) t\(duration) t\(start + duration))"
      lines.append(line)
    }
    return lines.joined(separator: "\n")
  }
  
//  public static func parseTemporalPlannerOutput(plannerOutput: String) -> (TemporalPlan, Double?) {
//
//  }
  
  public static func parsePlannerOutput(plannerOutput: String) -> (PlannerClassicalOutput, Double?) {
    var pddlPlan = [PlanAction]()
    var cost: Double? = nil
    for line in plannerOutput.components(separatedBy: "\n") where !line.isEmpty {
      if !line.starts(with: ";")  {
        let actionPart = line[line.index(after: line.firstIndex(of: "(")!)..<line.lastIndex(of: ")")!]
        let actionComponents = actionPart.components(separatedBy: " ")
        let actionName = actionComponents[0] //.uppercased()
        let actionArguments = Array(actionComponents[1...].map { $0.lowercased() })
        let pddlCommand = PlanAction(
          name: actionName,
          parameters: actionArguments)
        pddlPlan.append(pddlCommand)
      } else if line.contains("cost") {
        if let range = line.range(of: #"-?\d+\.?\d*"#, options: .regularExpression) {
          cost = Double(line[range])!
        }
      }
    }
    return (PlannerClassicalOutput(raw: plannerOutput, parsed: ClassicalPlan(actions: pddlPlan), cost: cost), cost)
  }
}
