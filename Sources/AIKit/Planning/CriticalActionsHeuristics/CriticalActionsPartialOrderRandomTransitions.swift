//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/17/22.
//

import Foundation
import MathLib

protocol CriticalActionsPartialOrderRandomTransitions {
  var game: PDDLGameFDRRepresentationAnalyticsLogic { get }
  var player: Int { get }
  var currentPlayerSoftGoalsCriticalActions: [[(
    criticalAction: PlanAction,
    criticalFact: PDDLNameLiteral,
    adversarialActions: Set<PlanAction>
  )]] { get }
  func randomNextState(from order: RelationDiGraphPath) -> RelationDiGraphPath?
}
extension CriticalActionsPartialOrderRandomTransitions {
  public func randomNextState(from order: RelationDiGraphPath) -> RelationDiGraphPath? {
    Bool.random() ? flipEdge(in: order) : exchangeCriticalActions(in: order)
  }
  private func flipEdge(in order: RelationDiGraphPath) -> RelationDiGraphPath {
    var flipCandidates = Set(order.graph.diEdges.map {$0.value})
    while true {
      guard let flipEdge = flipCandidates.randomElement() else {
        return order
      }
      var flippedGraph = order.graph
      flippedGraph.remove(edge: flipEdge)
      if !flippedGraph.isTherePath(from: flipEdge.start, to: flipEdge.end) {
        flippedGraph.add(
          edge: IntDiEdge(id: flipEdge.id, start: flipEdge.end, end: flipEdge.start))
        return RelationDiGraphPath(graph: flippedGraph)
      } else {
        flipCandidates.remove(flipEdge)
      }
    }
  }
  private func exchangeCriticalActions(in order: RelationDiGraphPath) -> RelationDiGraphPath {
    var orderGraph = order.graph
    let fdrCoding = game.groundedPlayersProblems[player]
    let randomGoalActionsToModify = Set(currentPlayerSoftGoalsCriticalActions.randomElement()!.map {$0.criticalAction}.map {fdrCoding.operatorIndex[$0]!})
    let verticesToRemove = orderGraph.diVertices.filter {randomGoalActionsToModify.contains($0.key)}
    verticesToRemove.forEach {
      orderGraph.remove(vertex: $0.value)
    }
    let newVertexIdToAdd = randomGoalActionsToModify.randomElement()!
    let otherCriticalActions = orderGraph.diVertices.keys
    orderGraph.add(vertex: IntVertex(newVertexIdToAdd))
    var graphIdsGenerator = IntGraphIndicesGenerator(graph: orderGraph)
    for action1 in otherCriticalActions {
      if fdrCoding.action1InfluencesAction2(action1: action1, action2: newVertexIdToAdd) ||
          fdrCoding.action1InfluencesAction2(action1: newVertexIdToAdd, action2: action1)
      {
        _ = orderGraph.addEdge(from: action1, to: newVertexIdToAdd, indexGenerator: &graphIdsGenerator.edgeIdGenerator)
      }
    }
    return RelationDiGraphPath(graph: orderGraph)
  }
}
