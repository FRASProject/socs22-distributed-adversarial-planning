//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/15/21.
//

import Foundation
import MathLib
import temporal_downward

public struct CriticalActionsPartialOrderAbstractionOptimizer: CriticalActionsPartialOrderRandomTransitions, SimulatedAnnealing {
  public typealias State = RelationDiGraphPath
  public enum InitMode {
    case first, random
  }
  let game: PDDLGameFDRRepresentationAnalyticsLogic
  let player: Int
  let costFunction: [PDDLNameLiteral: [Int]]
  let currentPlayerSoftGoalsCriticalActions: [[(
    criticalAction: PlanAction,
    criticalFact: PDDLNameLiteral,
    adversarialActions: Set<PlanAction>
  )]]
  let orderInitMode: InitMode
  let costNormalizer: Double
  
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    player: Int,
    costFunction: [PDDLNameLiteral: [Int]],
    currentPlayerSoftGoalsCriticalActions: [[(
      criticalAction: PlanAction,
      criticalFact: PDDLNameLiteral,
      adversarialActions: Set<PlanAction>
    )]],
    orderInitMode: InitMode
  ) {
    self.game = game
    self.player = player
    self.costFunction = costFunction
    self.currentPlayerSoftGoalsCriticalActions = currentPlayerSoftGoalsCriticalActions
    self.orderInitMode = orderInitMode
    self.costNormalizer = 1000.0 / Double(game.game.maxGameValue.max()!)
  }
  
  public var initState: RelationDiGraphPath {
    let goalCriticalActions = currentPlayerSoftGoalsCriticalActions.map { goalCluster -> PlanAction in
      let criticalActions = goalCluster.map {$0.criticalAction}
      switch orderInitMode {
      case .first:
        return criticalActions.first!
      case .random:
        return criticalActions.randomElement()!
      }
    }
    let fdrCoding = game.groundedPlayersProblems[player]
    let selectedCriticalActions = goalCriticalActions.map { fdrCoding.operatorIndex[$0]!}
    
    var influenceGraph = IntGraph(isolatedVertices: selectedCriticalActions)
    var graphIdsGenerator = IntGraphIndicesGenerator(graph: influenceGraph)
    for action1 in selectedCriticalActions {
      for action2 in selectedCriticalActions where action1 != action2 {
        if fdrCoding.action1InfluencesAction2(action1: action1, action2: action2) {
          if !influenceGraph.isEdge(action1, action2) {
            _ = influenceGraph.addEdge(v1: action1, v2: action2, indexGenerator: &graphIdsGenerator.edgeIdGenerator)
          }
        }
      }
    }
    
    let infulenceGrapOrientation = influenceGraph.findSomeAcyclicOrientation()
    let partialOrder = RelationDiGraphPath(graph: infulenceGrapOrientation)
    return partialOrder
  }
  
  public static func actionTimeEstimate(order: RelationDiGraphPath) -> [Int: Int]
  {
    var time = [Int: Int]()
    func computeTime(_ actionId: Int) -> Int {
      if let actionTime = time[actionId] { return actionTime }
      var actionMaxTime: Int?
      for preAction in order.elements {
        if order.isARelatedToB(a: preAction, b: actionId) {
          let preActionTime = computeTime(preAction)
          let duration = Int(temporal_fd_dur(Int32(preAction)).rounded())
          precondition(duration >= 0)
          let dist = Int(temporal_fd_action_dist(Int32(preAction), Int32(actionId)).rounded())
          precondition(dist >= 0)
          precondition(dist < 10000)
          let actionTime = preActionTime + duration + dist
          actionMaxTime = max(actionMaxTime ?? actionTime, actionTime)
        }
      }
      if let actionMaxTime = actionMaxTime {
        time[actionId] = actionMaxTime
        return actionMaxTime
      }
      let actionTime = Int(temporal_fd_grounded_action_dist(Int32(actionId)).rounded())
      precondition(actionTime >= 0)
      precondition(actionTime < 10000)
      time[actionId] = actionTime
      return actionTime
    }
    
    order.elements.forEach { _ = computeTime($0)}
    return time
  }
  
  public func cost(of order: RelationDiGraphPath) -> Double? {
    let actionsTimes = Self.actionTimeEstimate(order: order)
    let cost = order.elements.map { actionId -> Int in
      let factCostFunction = costFunction[
        game.playersCriticalActionCriticalFact[player][
          game.groundedPlayersProblems[player].operators[actionId].asPlanAction]!]!
      let actionTime = actionsTimes[actionId]!
      if actionTime < factCostFunction.count {
        return factCostFunction[actionTime]
      } else {
        return factCostFunction.last!
      }
    }.sum
    return Double(cost) * costNormalizer
  }
  
  
}
