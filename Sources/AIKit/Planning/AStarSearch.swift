//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/22.
//

import Foundation
import MathLib

public protocol AStarSearch {
  associatedtype State: Hashable
  associatedtype Action
  var initState: State { get }
  func cost(of action: Action) -> Double
  func heuristics(of state: State) -> Double
  func apply(action: Action, in state: State) -> State
  func isGoal(state: State) -> Bool
  func availableActions(in state: State) -> [Action]
}

extension AStarSearch {
  public func aStarSearch() -> (cost: Double, path: [Action])? {
    var closedSet = Set<State>()
    var gScore = [State: Double]()
    var predecesor = [State: (State, Action)]()
    var openSet = OpenMinSet<State, Double>()
    
    gScore[initState] = 0.0
    openSet.push(initState, score: heuristics(of: initState))
    var foundGoalState: State? = nil
    
    while let minimumFromOpenSet = openSet.pop() {
      if isGoal(state: minimumFromOpenSet) {
        foundGoalState = minimumFromOpenSet
        break
      }
      closedSet.insert(minimumFromOpenSet)
      for action in availableActions(in: minimumFromOpenSet) {
        let newState = apply(action: action, in: minimumFromOpenSet)
        if closedSet.contains(newState) { continue }
        let newStateGScore = gScore[minimumFromOpenSet]! + cost(of: action)
        assert(newStateGScore >= 0.0)
        if newStateGScore < gScore[newState] ?? Double.infinity {
          gScore[newState] = newStateGScore
          predecesor[newState] = (minimumFromOpenSet, action)
          let newFScore = newStateGScore + heuristics(of: newState)
          if openSet.contains(newState) {
            openSet.decreaseKey(newState, to: newFScore)
          } else {
            openSet.push(newState, score: newFScore)
          }
        }
      }
    }
    
    guard let foundGoalState = foundGoalState else {
      return nil
    }
    let finalCost = gScore[foundGoalState]!
    var currentState = foundGoalState
    var path = [Action]()
    while let (predecesorState, action) = predecesor[currentState] {
      path.append(action)
      currentState = predecesorState
    }
    path.reverse()
    return (cost: finalCost, path: path)
  }
}
