//
//  PlanningAction.swift
//  AIKit
//
//  Created by Pavel Rytir on 7/12/20.
//

import Foundation

public protocol PlanActionProtocol: Hashable {
  var name: String { get }
  var parameters: [String] { get }
}

public struct PlanAction: PlanActionProtocol, Codable, Comparable {
  public let name: String
  public let parameters: [String]
  public init(name: String, parameters: [String]) {
    self.name = name
    self.parameters = parameters
  }
  public static func < (lhs: PlanAction, rhs: PlanAction) -> Bool {
    lhs.name < rhs.name ||
      (lhs.name == rhs.name && lhs.parameters.lexicographicallyPrecedes(rhs.parameters))
  }
}

extension PlanAction {
  public init(_ action: PlanTemporalAction) {
    self.name = action.name
    self.parameters = action.parameters
  }
  public func removing(parameters parametersToRemove: Set<String>) -> PlanAction {
    PlanAction(name: name, parameters: parameters.subtracting(parametersToRemove))
  }
  public init(_ action: TemporalFDROperator) {
    self.name = action.name
    self.parameters = action.parameters
  }
  public init(_ action: BitVectorAction) {
    self.name = action.name
    self.parameters = action.parameters
  }
  public var description: String {
    return "\(name) \(parameters.joined(separator: " "))"
  }
}
