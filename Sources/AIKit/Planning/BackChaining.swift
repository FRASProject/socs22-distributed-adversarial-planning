//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/29/20.
//

import Foundation
import MathLib

public protocol BackChainingGoal {

}

public protocol BackChainingAction: Hashable {
  associatedtype E: BackChainingExpression
  var conditionalEffects: [BackChainingConditionalExpression<E>] { get }
  var preconditions: E { get }
}

public protocol BackChainingFact: Hashable {}

public protocol BackChainingState: Hashable, Sequence where Element == F, E.S == Self {
  associatedtype F: BackChainingFact
  associatedtype E: BackChainingExpression
  func intersection(with other: Self) -> Self
  func union(with other: Self) -> Self
  func contains(fact: F) -> Bool
  mutating func set(fact:F)
  mutating func remove(fact: F)
  var countNonzeroFacts: Int { get }
  init(expression: E)
  init(fact: F)
  init()
}

extension BackChainingState {
  public init(fact: F) {
    var state = Self()
    state.set(fact: fact)
    self = state
  }
  public init(_ facts: [F]) {
    var state = Self()
    for fact in facts {
      state.set(fact: fact)
    }
    self = state
  }
  public init(expression: E) {
    var state = Self()
    for fact in expression.positive {
      state.set(fact: fact)
    }
    for fact in expression.negative {
      state.remove(fact: fact)
    }
    self = state
  }
}

public protocol BackChainingExpression: Hashable {
  associatedtype S: BackChainingState
  var positive: S { get }
  var negative: S { get }
  init(positive: S, negative: S)
}

public struct BackChainingConditionalExpression<E: BackChainingExpression>: Hashable {
  public let conditions: E
  public let effects: E
}

public enum BackChaining {
  public static func start<A: BackChainingAction, S, E>(goal: E, initState: S, operators: [A]) ->
  Set<Set<A>> where E.S == S, E == A.E
  {
    var disjunctiveLandmarks = Set<Set<A>>()
    var open = Queue<S>()
    var openSet = Set<S>()
    var closed = Set<S>()
    open.enqueue(goal.positive)
    openSet.insert(goal.positive)
    
    for fact in initState {
      closed.insert(S(fact: fact))
    }
    
    while let current = open.dequeue() {
      openSet.remove(current)
      closed.insert(current)
      if current != initState {
        var currentLandmarks = Set<A>()
        var unionPreconditions = S()
        for action in operators {
          for conditionalEffect in action.conditionalEffects {
            // if conditionalEffect.effects.positive.intersection(with: current).countNonzeroFacts > 0 {
            let intersection = conditionalEffect.effects.positive.intersection(with: current)
//            print(intersection.countNonzeroFacts)
//            print(conditionalEffect.effects.positive.countNonzeroFacts)
            if intersection.countNonzeroFacts == conditionalEffect.effects.positive.countNonzeroFacts &&
                intersection.countNonzeroFacts > 0 {
              currentLandmarks.insert(action)
              let actionPreconditions = action.preconditions.positive
              unionPreconditions = unionPreconditions.union(with: actionPreconditions)
            }
          }
        }
        
        for fact in unionPreconditions {
          let factState = S(fact: fact)
          if !openSet.contains(factState) && !closed.contains(factState) {
            open.enqueue(factState)
            openSet.insert(factState)
          }
        }
        disjunctiveLandmarks.insert(currentLandmarks)
      }
    }
    return disjunctiveLandmarks
  }
  public static func split<E: BackChainingExpression>(goal: E) -> [E] {
    goal.positive.map {
      E(
        positive: E.S(fact: $0),
        negative: E.S())
    }
  }
}
