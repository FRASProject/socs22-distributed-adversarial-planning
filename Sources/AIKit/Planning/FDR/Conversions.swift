//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/2/21.
//

import Foundation

extension TemporalProblemFDRCoding {
  public var variableValueFact: [(Int, Int, PDDLAtomicNameFormula)] {
    variables.enumerated().flatMap { (offset, element) -> [(Int, Int, PDDLAtomicNameFormula)] in
      let ordered = Array(element).sorted(by: {$0.key < $1.key})
      if ordered.isEmpty || ordered.first!.key < 0 {
        return []
      }
      return ordered.compactMap {
        if let action = convert(atom:$0.value) {
          return (offset, $0.key, action)
        } else {
          return nil
        }
      }
    }
  }
  private func convert(atom: String) -> PDDLAtomicNameFormula? {
    let atomTrimmed = atom.trimmingCharacters(in: .whitespacesAndNewlines)
    if atomTrimmed == "<none of those>" {
      return nil
    }
    let parenthesisIndex = atomTrimmed.firstIndex(of: "(")!
    let name = String(
      atomTrimmed[atomTrimmed.index(atomTrimmed.startIndex, offsetBy: 5)..<parenthesisIndex])
    let insideParentheses = atomTrimmed[
      atomTrimmed.index(after: parenthesisIndex)..<atomTrimmed.index(before: atomTrimmed.endIndex)]
    let paremeters = insideParentheses.components(separatedBy: ",").map {
      String($0.trimmingCharacters(in: .whitespaces).dropFirst().dropLast()) }
    return PDDLAtomicNameFormula(name: name, parameters: paremeters)
  }
}
