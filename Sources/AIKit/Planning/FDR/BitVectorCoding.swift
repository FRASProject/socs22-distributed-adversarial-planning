//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/2/21.
//

import Foundation
import MathLib

public struct BitVectorCoding {
  public let facts: [PDDLAtomicNameFormula]
  public let operators: [BitVectorAction]
  public let initState: BitVectorState
  public let goal: SimpleExpression<BitVectorState>
  public static let maxNumberOfFacts = 1024 * 1024
}

extension BitVectorCoding {
  public init(from temporalCoding: BitVectorTemporalCoding) {
    self.facts = temporalCoding.facts
    self.operators = temporalCoding.operators.map {BitVectorAction(temporal: $0)}
    self.initState = temporalCoding.initState
    self.goal = temporalCoding.goal
  }
  public func findIndex(of fact: PDDLAtomicNameFormula) -> Int? {
    facts.firstIndex(of: fact)
  }
}

public struct BitVectorTemporalCoding {
  public let facts: [PDDLAtomicNameFormula]
  public let operators: [BitVectorTemporalAction]
  public let initState: BitVectorState
  public let goal: SimpleExpression<BitVectorState>
  public static let maxNumberOfFacts = 1024 * 1024
}

public struct BitVectorAction: BackChainingAction {
  public typealias E = SimpleExpression<BitVectorState>
  public let name: String
  public let parameters: [String]
  public let conditionalEffects: [BackChainingConditionalExpression<E>]
  public let preconditions: E
}

extension BitVectorAction {
  public init(temporal action: BitVectorTemporalAction) {
    self.name = action.name
    self.parameters = action.parameters
    self.preconditions = action.preconditionAtStart.union(
      with: action.preconditionOverAll).union(with: action.preconditionAtEnd)
    
    var conflictingEffects = Set<BackChainingConditionalExpression<E>>()
    var resolvedEffects = [BackChainingConditionalExpression<E>]()
    for effectAtStart in action.effectsAtStart {
      for effectAtEnd in action.effectsAtEnd {
        if effectAtStart.effects.positive.intersection(
            with: effectAtEnd.effects.negative).countNonzeroFacts > 0 ||
            effectAtEnd.effects.positive.intersection(
                with: effectAtStart.effects.negative).countNonzeroFacts > 0
            {
          conflictingEffects.insert(effectAtStart)
          conflictingEffects.insert(effectAtEnd)
          let resolvedEffect = BackChainingConditionalExpression(
            conditions: effectAtStart.conditions,
            effects: effectAtEnd.effects)
          resolvedEffects.append(resolvedEffect)
        }
      }
    }
    
    self.conditionalEffects = action.effectsAtStart.filter {
      !conflictingEffects.contains($0)} + action.effectsAtEnd.filter {
        !conflictingEffects.contains($0)} + resolvedEffects
  }
}

public struct BitVectorTemporalAction: Hashable {
  public typealias E = SimpleExpression<BitVectorState>
  //public let duration ???
  public let name: String
  public let parameters: [String]
  public let preconditionAtStart: E
  public let preconditionOverAll: E
  public let preconditionAtEnd: E
  public let effectsAtStart: [BackChainingConditionalExpression<E>]
  public let effectsAtEnd: [BackChainingConditionalExpression<E>]
}


extension BitVectorTemporalCoding {
  public init(from fdrCoding: TemporalProblemFDRCoding) {
    let variableValueFact = fdrCoding.variableValueFact
    self.facts = variableValueFact.map {$0.2}
    let translationTable = Dictionary(
      uniqueKeysWithValues: variableValueFact.enumerated().map {
        (Tuple($0.element.0, $0.element.1),($0.element.2,$0.offset))
      })
    func convertPrecond(_ p: [(variable: Int, value: Int)]) -> SimpleExpression<BitVectorState> {
      var positive = [Int]()
      var negative = [Int]()
      for precond in p where precond.value != -1 {
        if let fact = translationTable[Tuple(precond.variable, precond.value)] {
          positive.append(fact.1)
        } else {
          negative.append(contentsOf: fdrCoding.variables[precond.variable].map {
            translationTable[Tuple(precond.variable, $0.key)]!.1
          })
        }
      }
      return SimpleExpression(
          positive: BitVectorState(positive),
          negative: BitVectorState(negative))
    }
    func convertEffect(_ p: [TemporalFDRConditionEffect]) ->
    [BackChainingConditionalExpression<BitVectorTemporalAction.E>]
    {
      p.map { conditionEffect in
        let positive: [Int]
        let positiveEffect: [Int]
        let negative: [Int]
        let negativeEffect: [Int]
        precondition(conditionEffect.newValue != -1)
        if conditionEffect.oldValueCondition == -1 {
          positive = []
          negative = []
          if let fact = translationTable[
              Tuple(conditionEffect.variable, conditionEffect.newValue)] {
            positiveEffect = [fact.1]
            negativeEffect = []
          } else {
            positiveEffect = []
            negativeEffect = fdrCoding.variables[conditionEffect.variable].compactMap {
              translationTable[Tuple(conditionEffect.variable, $0.key)]?.1
            }
          }
        } else {
          if let fact = translationTable[
              Tuple(conditionEffect.variable, conditionEffect.oldValueCondition)]
          {
            positive = [fact.1]
            negative = []
          } else {
            positive = []
            negative = fdrCoding.variables[conditionEffect.variable].compactMap {
              translationTable[Tuple(conditionEffect.variable, $0.key)]?.1
            }
          }
          if let fact = translationTable[
              Tuple(conditionEffect.variable, conditionEffect.newValue)] {
            positiveEffect = [fact.1]
            negativeEffect = positive
          } else {
            positiveEffect = []
            negativeEffect = fdrCoding.variables[conditionEffect.variable].compactMap {
              translationTable[Tuple(conditionEffect.variable, $0.key)]?.1
            }
          }
        }
        return BackChainingConditionalExpression(
          conditions: SimpleExpression(
            positive: BitVectorState(positive),
            negative: BitVectorState(negative)),
          effects: SimpleExpression(
            positive: BitVectorState(positiveEffect),
            negative: BitVectorState(negativeEffect)))
      }
    }
    self.operators = fdrCoding.operators.map { op in
      BitVectorTemporalAction(
        name: op.name,
        parameters: op.parameters,
        preconditionAtStart: convertPrecond(op.preconditionAtStart),
        preconditionOverAll: convertPrecond(op.preconditionOverAll),
        preconditionAtEnd: convertPrecond(op.preconditionAtEnd),
        effectsAtStart: convertEffect(op.effectsAtStart),
        effectsAtEnd: convertEffect(op.effectsAtEnd))
    }
    self.initState = BitVectorState(
      fdrCoding.initState.prefix(fdrCoding.normalVariables.count).enumerated().compactMap {
        translationTable[Tuple($0.offset, $0.element)]?.1
      })
    self.goal = SimpleExpression(
      positive: BitVectorState(fdrCoding.goal.map {
                                translationTable[Tuple($0.variable, $0.value)]!.1 }),
      negative: BitVectorState())
  }
}

extension Int : BackChainingFact {}

extension BitVectorState: CustomDebugStringConvertible {
  public var debugDescription: String {
    "[\(self.map { String($0) }.joined(separator: ","))]"
  }
}

public struct BitVectorState: BackChainingState {
  public __consuming func makeIterator() -> Iterator {
    return Iterator(self.bitVector)
  }
  
  public struct Iterator: IteratorProtocol {
    public typealias Element = Int
    private var currentIndex: Int
    private var bitVector: [UInt32]
    fileprivate init(_ bitVector: [UInt32]) {
      self.bitVector = bitVector
      self.currentIndex = -1
    }
    public mutating func next() -> Int? {
      if currentIndex == -1 {
        if let bucketIndex = bitVector.firstIndex(where: { $0 != 0}) {
          let bucket = bitVector[bucketIndex]
          let index = findNextNonzeroBit(bucket, after: -1)!
          currentIndex = bucketIndex * BitVectorState.bucketSize + index
          return currentIndex
        } else {
          return nil
        }
      } else {
        let bucketIndex = currentIndex / BitVectorState.bucketSize
        let indexInBucket = currentIndex % BitVectorState.bucketSize
        let bucket = bitVector[bucketIndex]
        if let index = findNextNonzeroBit(bucket, after: indexInBucket) {
          currentIndex = bucketIndex * BitVectorState.bucketSize + index
          return currentIndex
        } else {
          if let newBucketIndex = bitVector.dropFirst(
              bucketIndex + 1).firstIndex(where: { $0 != 0})
          {
            let newBucket = bitVector[newBucketIndex]
            let index = findNextNonzeroBit(newBucket, after: -1)!
            currentIndex = newBucketIndex * BitVectorState.bucketSize + index
            return currentIndex
          } else {
            return nil
          }
        }
      }
    }
    private func findNextNonzeroBit(_ bucket: UInt32, after offset: Int) -> Int? {
      let maskedBucket: UInt32
      if offset != -1 {
        let mask = ~UInt32(((1 << (offset + 1)) - 1))
        maskedBucket = bucket & mask
      } else {
        maskedBucket = bucket
      }
      if maskedBucket == 0 {
        return nil
      } else {
        return maskedBucket.trailingZeroBitCount
      }
    }
  }
  public func intersection(with other: BitVectorState) -> BitVectorState {
    var newVector = zip(self.bitVector, other.bitVector).map(&)
    let newSize = Swift.max(self.bitVector.count, other.bitVector.count)
    newVector.reserveCapacity(newSize)
    (newVector.count..<newSize).forEach { _ in newVector.append(0)}
    return BitVectorState(newVector)
  }
  
  public func union(with other: BitVectorState) -> BitVectorState {
    var newVector = zip(self.bitVector, other.bitVector).map(|)
    let minSize = Swift.min(self.bitVector.count, other.bitVector.count)
    if self.bitVector.count > other.bitVector.count {
      newVector.append(contentsOf: self.bitVector[minSize...])
    } else {
      newVector.append(contentsOf: other.bitVector[minSize...])
    }
    return BitVectorState(newVector)
  }
  
  public mutating func set(fact: Int) {
    let bucketNumber = fact / Self.bucketSize
    if bitVector.count <= bucketNumber {
      bitVector.reserveCapacity(bucketNumber + 1)
      (bitVector.count...bucketNumber).forEach { _ in bitVector.append(0)}
    }
    
    let bucket = bitVector[bucketNumber]
    let bitInBucket = fact % Self.bucketSize
    let newBucket = bucket | (1 << bitInBucket)
    bitVector[bucketNumber] = newBucket
  }
  
  public mutating func remove(fact: Int) {
    let bucketNumber = fact / Self.bucketSize
    let bucket = bitVector[bucketNumber]
    let bitInBucket = fact % Self.bucketSize
    let newBucket = bucket & ~(1 << bitInBucket)
    bitVector[bucketNumber] = newBucket
  }
  
  public var countNonzeroFacts: Int {
    bitVector.map { $0.nonzeroBitCount }.reduce(0, +)
  }
  public var count: Int {
    countNonzeroFacts
  }
  
  public typealias F = Int
  public typealias E = SimpleExpression<Self>
  
  private var bitVector: [UInt32]
  
  public init() {
    self.bitVector = []
  }
  private init(_ bitVector: [UInt32]) {
    self.bitVector = bitVector
  }
  public static let bucketSize = 32
  
  public func contains(fact: F) -> Bool {
    let bucket = bitVector[fact / Self.bucketSize]
    let bitInBucket = fact % Self.bucketSize
    return (bucket >> bitInBucket) % 2 == 1
  }
  public func contains(_ fact: F) -> Bool {
    contains(fact: fact)
  }
  
}
