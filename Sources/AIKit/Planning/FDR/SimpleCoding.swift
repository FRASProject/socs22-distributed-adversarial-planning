//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/30/20.
//

import Foundation

public struct SimpleCoding {
  public let facts: [PDDLAtomicNameFormula]
  //public let operators: [TemporalFDROperator]
}

extension PDDLAtomicNameFormula: BackChainingFact {}

public struct SimpleState: BackChainingState {
  public init() {
    facts = []
  }
  private init(facts: Set<F>) {
    self.facts = facts
  }
  
  public typealias F = PDDLAtomicNameFormula
  public typealias E = SimpleExpression<Self>
  
  private var facts: Set<F>
  
  public func intersection(with other: SimpleState) -> SimpleState {
    SimpleState(facts: facts.intersection(other.facts))
  }
  public func union(with other: SimpleState) -> SimpleState {
    SimpleState(facts: facts.union(other))
  }
  public mutating func set(fact: PDDLAtomicNameFormula) {
    facts.insert(fact)
  }
  public mutating func remove(fact: PDDLAtomicNameFormula) {
    facts.remove(fact)
  }
  public var countNonzeroFacts: Int {
    facts.count
  }
  public var count: Int {
    facts.count
  }
  public func makeIterator() -> Set<F>.Iterator {
    facts.makeIterator()
  }
  public func contains(fact: F) -> Bool {
    facts.contains(fact)
  }
  public func contains(_ fact: F) -> Bool {
    facts.contains(fact)
  }
}

public struct SimpleExpression<S: BackChainingState>: BackChainingExpression {
  public let positive: S
  public let negative: S
  public init(positive: S, negative: S) {
    self.positive = positive
    self.negative = negative
  }
}

extension SimpleExpression {
  public func union(with other: Self) -> Self {
    SimpleExpression(
      positive: self.positive.union(with: other.positive),
      negative: self.negative.union(with: other.negative))
  }
}
