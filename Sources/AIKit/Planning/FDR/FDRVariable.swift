//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/27/21.
//

import Foundation

public struct FDRVariable {
  public let name: String
  public let axiomLayer: Int
  public let values: [String]
}

extension FDRVariable {
  public var text: String {
    var result = [String]()
    result.append("begin_variable")
    result.append(name)
    result.append("\(axiomLayer)")
    result.append("\(values.count)")
    result.append(contentsOf: values)
    result.append("end_variable")
    return result.joined(separator: "\n")
  }
}
