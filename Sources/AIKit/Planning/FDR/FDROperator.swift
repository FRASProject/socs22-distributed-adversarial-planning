//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/27/21.
//

import Foundation

public struct FDROperator {
  public let name: String
  public let parameters: [String]
  public var planAction: PlanAction {
    PlanAction(name: name, parameters: parameters)
  }
  public let preconditions: [(variable: Int, value: Int)]
  public let effects: [FDRConditionEffect]
  public let cost: Int
  public func settingCost(to newCost: Int) -> FDROperator {
    FDROperator(
      name: name,
      parameters: parameters,
      preconditions: preconditions,
      effects: effects,
      cost: newCost)
  }
  public func multiplyingCostBy(factor: Double) -> FDROperator {
    FDROperator(
      name: name,
      parameters: parameters,
      preconditions: preconditions,
      effects: effects,
      cost: Int((Double(cost) * factor).rounded()))
  }
  public func increasingCostBy(increment: Int) -> FDROperator {
    FDROperator(
      name: name,
      parameters: parameters,
      preconditions: preconditions,
      effects: effects,
      cost: cost + increment)
  }
  public var variables: Set<Int> {
    var result = Set<Int>()
    result.formUnion(preconditions.map {$0.variable})
    result.formUnion(effects.flatMap {$0.allVariables})
    return result
  }
}

extension FDROperator {
  public var text: String {
    var result = [String]()
    result.append("begin_operator")
    result.append("\(name) " + parameters.joined(separator: " "))
    result.append("\(preconditions.count)")
    result.append(contentsOf: preconditions.map {"\($0.variable) \($0.value)"})
    result.append("\(effects.count)")
    result.append(contentsOf: effects.map {$0.text})
    result.append("\(cost)")
    result.append("end_operator")
    return result.joined(separator: "\n")
  }
  public var description: String {
    "\(name) " + parameters.joined(separator: " ")
  }
}

public struct FDRConditionEffect {
  public let variable: Int
  public let oldValueCondition: Int
  public let newValue: Int
  public let preconditions: [FDRFact]
  public var allVariables: Set<Int> {
    var result = Set<Int>()
    result.insert(variable)
    result.formUnion(preconditions.map {$0.variable})
    return result
  }
}

extension FDRConditionEffect {
  public var text: String {
    "\(preconditions.count) " +
    preconditions.map {"\($0.variable) \($0.value) "}.joined(separator: "") +
    "\(variable) \(oldValueCondition) \(newValue)"
  }
}

