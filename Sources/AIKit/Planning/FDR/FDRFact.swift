//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/27/21.
//

import Foundation

public struct FDRFact {
  let variable: Int
  let value: Int
  public func translate(_ table: [Int: Int]) -> FDRFact? {
    table[variable].map { FDRFact(variable: $0, value: value) }
  }
}
