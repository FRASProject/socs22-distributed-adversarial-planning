//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/18/21.
//

import Foundation

public protocol PlanTemporalActionProtocol: PlanActionProtocol {
  var start: Int { get }
  var duration: Int { get }
}

public struct PlanTemporalAction: PlanTemporalActionProtocol, Codable {
  public let start: Int
  public let name: String
  public let parameters: [String]
  public let duration: Int
  public init(start: Int, name: String, parameters: [String], duration: Int) {
    self.start = start
    self.name = name
    self.parameters = parameters
    self.duration = duration
  }
  public func updating(time: Int) -> PlanTemporalAction {
    PlanTemporalAction(
      start: time,
      name: name,
      parameters: parameters,
      duration: duration)
  }
  public var originalName: String {
    if let projIndex = name.range(of: PDDLConstants.projectedActionSuffix)?.lowerBound {
      return String(name[..<projIndex])
    } else {
      return name
    }
  }
}

extension PlanTemporalAction {
  public var text: String {
    "\(start): (\(name) \(parameters.joined(separator: " ")) ) [\(duration)]"
  }
  public func puncturing(parameters excludedParameters: [String]) -> PlanTemporalAction {
    PlanTemporalAction(
      start: start,
      name: name,
      parameters: excludedParameters.subtracting(Set(excludedParameters)),
      duration: duration)
  }
  public func isEqual(to other: PlanTemporalAction, exluding exludedParameters: [String]) -> Bool {
    self.puncturing(parameters: exludedParameters) == other.puncturing(
      parameters: exludedParameters)
  }
  public func containedParameters(_ parametersToCheck: Set<String>) -> [String] {
    parameters.filter {parametersToCheck.contains($0)}
  }
}

extension PlanTemporalAction : Comparable {
  public static func < (lhs: PlanTemporalAction, rhs: PlanTemporalAction) -> Bool {
    if lhs.start != rhs.start {
      return lhs.start < rhs.start
    } else if lhs.name != rhs.name {
      return lhs.name < rhs.name
    } else if lhs.parameters != rhs.parameters {
      return lhs.parameters.lexicographicallyPrecedes(rhs.parameters)
    } else {
      return lhs.duration < rhs.duration
    }
  }
}
