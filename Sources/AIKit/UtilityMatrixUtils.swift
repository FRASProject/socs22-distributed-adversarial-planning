//
//  evaluation.swift
//  FRASLib
//
//  Created by Pavel Rytir on 6/9/18.
//

import Foundation
import MathLib

public enum UtilityMatrixUtils {
  static public func constructUtilityMatrix<E:PureStrategyEvaluator>(
    evaluator: E,
    player1PureStrategies: [E.PSP1],
    player2PureStrategies: [E.PSP2]) throws -> (player1: Matrix, player2: Matrix)
  {
    let numPlansP1 = player1PureStrategies.count
    let numPlansP2 = player2PureStrategies.count
    
    var player1Utility = Matrix(rows: numPlansP1, columns: numPlansP2)
    var player2Utility = Matrix(rows: numPlansP1, columns: numPlansP2)
    
    for rowPlayer1 in 0..<numPlansP1 {
      for colPlayer2 in 0..<numPlansP2 {
        let u = try evaluator.evaluate(stratP1: player1PureStrategies[rowPlayer1],
                                   stratP2: player2PureStrategies[colPlayer2])
        player1Utility[rowPlayer1, colPlayer2] = u.0
        player2Utility[rowPlayer1, colPlayer2] = u.1
      }
    }
    return (player1: player1Utility, player2: player2Utility)
  }
}
