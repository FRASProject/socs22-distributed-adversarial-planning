//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/21.
//

import Foundation

public class PDDLSerialInitOracle: InitOracle {
  public typealias PS = TemporalPlan
  public let game: PDDLGameFDRRepresentationAnalyticsLogic
  private let pddlPlanner: PDDLPlanner
  //private let pddlGrounder: FastDownwardGrounder
  //private let actionInstances: [[[String: Set<PlanAction>]]]
  //private let planningProblemPlayer1: PDDLPlanningProblem
  //private let planningProblemPlayer2: PDDLPlanningProblem
  private let config: AIKitData
  
  /// Short text description of the planner.
  public var shortDescription: String {
    "PDDLInitOracle"
  }
  
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    config: AIKitData,
    pddlPlanner: PDDLPlanner) throws
  {
    self.game = game
    self.config = config
    self.pddlPlanner = pddlPlanner
//    self.planningProblemPlayer1 = game.game.getPlanningProblem(for: 0)
//      .keepingOnly(player: 0)
//      .convertingSoftGoalsToHardGoalsRemovingMetrics()
//    self.planningProblemPlayer2 = game.game.getPlanningProblem(for: 1)
//      .keepingOnly(player: 1)
//      .convertingSoftGoalsToHardGoalsRemovingMetrics()
    //self.pddlGrounder = FastDownwardGrounder(temporal: true)
//    self.actionInstances = try [
//      planningProblemPlayer1.groundUnitSubproblems(grounder: pddlGrounder),
//      planningProblemPlayer2.groundUnitSubproblems(grounder: pddlGrounder)
//    ]
  }
  public func findPlans(
    for player: Int,
    maxNumberOfPlans: Int,
    plansCacheDir: URL?
  ) throws -> (
    plans: [PS],
    log: AIKitData,
    computationDuration: [TimeInterval])
  {
    assert(player == 1 || player == 2)
    let planCache = plansCacheDir.map {$0.appendingPathComponent("initPlan1Player\(player).sol")}
    let cacheFilePrefix = "initPlan1Player\(player)"
    let player = player - 1
    //let playerObject = game.players[player]
    let problem = game.game.getPlanningProblem(for: player)
      .keepingOnly(player: player)
      .convertingSoftGoalsToHardGoalsRemovingMetrics()
    let tempProblemForMaxDeadlineComputation = problem.convertingToClassicalPlanningProblem(removeDurationFunctions: true)
    let maxDeadlineTimestamp = tempProblemForMaxDeadlineComputation.maxDuration!/tempProblemForMaxDeadlineComputation.durationOfUnitTimestamp!
    let costFunction = Dictionary(uniqueKeysWithValues: game.serialCriticalFacts.map {
      ($0, [Int](repeating: 0, count: maxDeadlineTimestamp)) //  + [50, 100]
    })
    let cooperativeGame = try IterativeBestResponse(
      game: game,
      mode: .costDecreasing(maxNumberOfIterations: 100),
      config: config,
      player: player,
      planCacheDir: plansCacheDir,
      cacheFilePrefix: cacheFilePrefix,
      planner: pddlPlanner,
      costFunction: costFunction,
      verbose: true)
    
    try cooperativeGame.run(maxNumberOfIterations: 20)
    
    let plan = cooperativeGame.currentJointPlan
    if let planCache = planCache {
      try plan.text.write(to: planCache, atomically: false, encoding: .utf8)
    }
    
    let log = cooperativeGame.currentPlansLog
    let duration = cooperativeGame.totalDuration!
    return ([plan], log, [duration])
  }
}
