//
//  PDDLBestResponseOracle.swift
//  FRASLib
//
//  Created by Pavel Rytir on 7/4/20.
//

import Foundation
import MathLib

public class PDDLBestResponseOracle: BestResponseOracle {
  public enum Mode {
    case lastPlanOnly, allPlans
  }
  public static let maxCost = 100.0
  public typealias PS = TemporalPlan
  private let game: PDDLGameFDRRepresentationAnalyticsLogic
  private let pddlPlanner: PDDLPlanner
  private let heuristics: PDDLInitOracle.Heuristics
  private var debugVerbose: Bool
  private let mode: Mode
  private let simAnnealStartTemperature: Double?
  private let simAnnealTemperatureStep: Double?
  
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    pddlPlanner: PDDLPlanner,
    heuristics: PDDLInitOracle.Heuristics,
    simAnnealStartTemperature: Double? = nil,
    simAnnealTemperatureStep: Double? = nil,
    mode: Mode = .lastPlanOnly)
  {
    self.game = game
    self.pddlPlanner = pddlPlanner
    self.heuristics = heuristics
    self.mode = mode
    self.debugVerbose = false
    self.simAnnealTemperatureStep = simAnnealTemperatureStep
    self.simAnnealStartTemperature = simAnnealStartTemperature
  }
  
  public func findBestResponse(
    player: Int,
    to opponent: Int,
    opponentStrat: MixedStrategy<PS>,
    utilityLowerBound: Double,
    plansCacheFile: URL?,
    continueFunction: (() -> Bool)?,
    solutionChecker: SolutionChecker) throws -> (
      bestResponseStrategy: [PS],
      cost: [Double],
      log: AIKitData,
      computationDuration: TimeInterval)
  {
    assert(player == 1 || player == 2)
    assert(opponent == 1 || opponent == 2)
    assert(player != opponent)
    let player = player - 1
    let opponent = opponent - 1
    
    let normalCostFunction = PDDLCriticalFactCostUtils.extractCostFunction(
      from: opponentStrat,
      units: Set(game.playersUnits[opponent]),
      actionCriticalFacts: game.actionWithoutUnitsCriticalFacts).merging(
        game.criticalFacts.map {($0, [0])}, uniquingKeysWith: { a, _ in a })
    let criticalFactsWeights = Dictionary(
      uniqueKeysWithValues: normalCostFunction.map {
        ($0.key, game.getWeight(of: $0.key, for: player))})
    let weightedFactsCostFunction = PDDLCriticalFactCostUtils.scaleCostFunction(
      normalCostFunction,
      weights: criticalFactsWeights)
    let problem: PDDLPlanningProblem
    let parameterWithPrefixesToRemove: [String]
    debugVerbose = true
    switch heuristics {
    case .none:
      parameterWithPrefixesToRemove = []
      problem = game.game.getPlanningProblem(for: player)
        .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
        .keepingOnly(player: player)
        .adding(
          criticalFactCostFunction: weightedFactsCostFunction,
          criticalFactCriticalActions: game.game.domain.findPossibleCriticalActionsAndFacts())
        .convertingSoftGoalsToHardGoalsRemovingMetrics()
        .addingTotalCostFunction()
        .addingMinimizeTotalCostMetric()
    case .pruning:
      parameterWithPrefixesToRemove = []
      let reachableGoals = SoftGoalsHeuristics.determineReachableGoalsByPruningHeuristics(
        from: opponentStrat,
        game: game,
        player: player)
      if debugVerbose { print("Reachable goals \(reachableGoals)") }
      problem = game.game.getPlanningProblem(for: player)
        .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
        .keepingOnly(player: player)
        .adding(
          criticalFactCostFunction: weightedFactsCostFunction,
          criticalFactCriticalActions: game.game.domain.findPossibleCriticalActionsAndFacts())
        .keepingOnlyGoals(reachableGoals)
        .convertingSoftGoalsToHardGoalsRemovingMetrics()
        .addingTotalCostFunction()
        .addingMinimizeTotalCostMetric()
    case .ordering, .orderingSimAnneal:
      parameterWithPrefixesToRemove = ["c"] // remove count parameters.
      let orderingNotOptimized = SoftGoalsHeuristics.calculateDeleteRelaxationOrderHeuristics(
        factsCostFunction: weightedFactsCostFunction,
        criticalFactsWeights: criticalFactsWeights,
        groundedProblem: game.groundedPlayersProblems[player],
        softGoals: game.softGoals[player],
        goalCriticalActionClusters: game.goalCriticalActionClusters[player],
        playersUnits: game.playersUnits[player],
        debugVerbose: debugVerbose)
      let ordering: RelationDiGraphPath
      if heuristics == .orderingSimAnneal {
        if debugVerbose { print("NotOptOrdering : \(orderingNotOptimized)") }
        ordering = SoftGoalsHeuristics.optimizeOrder(
          order: orderingNotOptimized,
          game: game,
          player: player,
          costFunction: weightedFactsCostFunction,
          startTemperature: simAnnealStartTemperature!,
          temperatureStep: simAnnealTemperatureStep!)
      } else {
        ordering = orderingNotOptimized
      }
      let goalCriticalActions = game.softGoalsCriticalActions[player].map {
        $0.map {game.groundedPlayersProblems[player].operatorIndex[$0.criticalAction]!}}
      let convertedOrdering: [String: [Int]] = SoftGoalsHeuristics.convert(
        relation: ordering,
        fdrCoding: game.groundedPlayersProblems[player],
        goalCriticalActions: goalCriticalActions,
        units: game.playersUnits[player])
      let reachableGoals = convertedOrdering.values.map {Set($0)}.reduce(Set()) {$0.union($1)}.sorted()
      let (criticalActionFact, orderingConverted) = SoftGoalsHeuristics.convertDataForOrderHeuristics(
        ordering: convertedOrdering,
        game: game,
        player: player)
      if debugVerbose { print("Ordering : \(ordering)") }
      problem = game.game.getPlanningProblem(for: player)
        .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
        .keepingOnly(player: player)
        .adding(
          criticalFactCostFunction: weightedFactsCostFunction,
          criticalFactCriticalActions: game.game.domain.findPossibleCriticalActionsAndFacts())
        .keepingOnlyGoals(reachableGoals)
        .addingOrderHeuristics(
          criticalActionFact: criticalActionFact,
          allCriticalActionNames: Set(game.playerAllCriticalAction[player].map {$0.name}),
          ordering: orderingConverted)
        .convertingSoftGoalsToHardGoalsRemovingMetrics()
        .addingTotalCostFunction()
        .addingMinimizeTotalCostMetric()
    }
    
    let (plannerOutput, cost, log, _, duration) = try pddlPlanner.findGoodEnough(
      problemPDDL: problem.problem.text,
      domainPDDL: problem.domain.text,
      isGoodEnoughChecker: {
        if let parsedPlan = $0.parsed {
          let plan = TemporalPlan(
            classicalPlan: parsedPlan,
            durationOfUnitTimestamp: problem.durationOfUnitTimestamp!,
            units: problem.problem.units,
            removingNoopActions: true)
            .removingParametersWith(prefixes: parameterWithPrefixesToRemove)
          return try solutionChecker.isGoodEnough(strategy: [plan], player: player + 1)
        } else {
          return false
        }
      },
      planCache: plansCacheFile)
    
    //    let (plannerOutput, cost, log, _, duration) = try pddlPlanner.findFirst(
    //      problemPDDL: problem.problem.text,
//      domainPDDL: problem.domain.text,
//      planCache: plansCacheFile)
    
    if !plannerOutput.isEmpty {
      switch mode {
      case .lastPlanOnly:
        let plan = TemporalPlan(
          classicalPlan: plannerOutput.last!.parsed!,
          durationOfUnitTimestamp: problem.durationOfUnitTimestamp!,
          units: problem.problem.units,
          removingNoopActions: true)
          .removingParametersWith(prefixes: parameterWithPrefixesToRemove)
        return ([plan], [cost.last!], .string(log), duration.last!)
      case .allPlans:
        let plans = plannerOutput.map {
          TemporalPlan(
            classicalPlan: $0.parsed!,
            durationOfUnitTimestamp: problem.durationOfUnitTimestamp!,
            units: problem.problem.units,
            removingNoopActions: true)
            .removingParametersWith(prefixes: parameterWithPrefixesToRemove)
        }
        return (plans, cost, .string(log), duration.last!)
      }
      
    } else {
      return ([], [], .string(log), duration.last ?? 0)
    }
  }
  
  public var shortDescription: String {
    "PDDLBestResponseOracle"
  }
}
