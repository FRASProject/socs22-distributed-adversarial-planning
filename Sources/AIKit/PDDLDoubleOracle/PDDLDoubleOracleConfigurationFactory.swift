//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/23/21.
//

import Foundation

public enum PDDLDoubleOracleConfigurationFactory {
  public static func makeBestResponsePlanners(
    config: AIKitData
  ) throws -> (p1BR: PDDLPlanner, p2BR: PDDLPlanner)
  {
    guard let p1BestResponsePlannerConfig = config["p1BestResponsePlanner"],
          let p2BestResponsePlannerConfig = config["p2BestResponsePlanner"]
    else {
      fatalError("Invalid configuration file!")
    }
    return (
      p1BR: try PDDLPlannerFactory.make(
        by: p1BestResponsePlannerConfig,
        frasConfig: ExperimentManager.defaultManager.systemConfig,
        fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases),
      p2BR: try PDDLPlannerFactory.make(
        by: p2BestResponsePlannerConfig,
        frasConfig: ExperimentManager.defaultManager.systemConfig,
        fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases))
  }
  public static func makePlanners(
    config: AIKitData
  ) throws -> (p1Init: PDDLPlanner, p2Init: PDDLPlanner, p1BR: PDDLPlanner, p2BR: PDDLPlanner)
  {
    guard let p1InitPlannerConfig = config["p1InitPlanner"],
          let p2InitPlannerConfig = config["p2InitPlanner"]
    else {
      fatalError("Invalid configuration file!")
    }
    let (p1BR, p2BR) = try Self.makeBestResponsePlanners(config: config)
    return (
      p1Init: try PDDLPlannerFactory.make(
        by: p1InitPlannerConfig,
        frasConfig: ExperimentManager.defaultManager.systemConfig,
        fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases),
      p2Init: try PDDLPlannerFactory.make(
        by: p2InitPlannerConfig,
        frasConfig: ExperimentManager.defaultManager.systemConfig,
        fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases),
      p1BR: p1BR,
      p2BR: p2BR)
  }
  public static func makeSerial(
    for game: PDDLGameFDRRepresentationAnalyticsLogic,
    config: AIKitData
  ) throws -> DoubleOracleConfiguration<PDDLPlansEvaluator,
                                        PDDLSerialInitOracle,
                                        PDDLSerialBestResponseOracle,
                                        PDDLSerialInitOracle,
                                        PDDLSerialBestResponseOracle>
  {
    
    let planners = try Self.makePlanners(config: config)
//    let costAdversarySyncRatio = config["AdvSyncR"]?.double ?? 1.0
//    let cooperativeCostSpread = config["coopCostSpread"]?.integer ?? 0
//    let cooperativeCostBase = config["coopCostBase"]?.integer ?? 0
    let player1InitOracle = try PDDLSerialInitOracle(
      game: game,
      config: config,
      pddlPlanner: planners.p1Init
    )
    let player2InitOracle = try PDDLSerialInitOracle(
      game: game,
      config: config,
      pddlPlanner: planners.p2Init
    )
    let mode: IterativeBestResponse.Mode.Short
    switch config["IterativeBRMode"]?.string {
    case "sa":
      mode = .simulatedAnnealing
    default:
      mode = .costDecreasing
    }
    let player1BestResponseOracle: PDDLSerialBestResponseOracle
    let player2BestResponseOracle: PDDLSerialBestResponseOracle
    switch mode {
    case .costDecreasing:
      player1BestResponseOracle = try PDDLSerialBestResponseOracle(
        game: game,
        mode: .costDecreasing(maxNumberOfIterations: 100),
        config: config,
        pddlPlanner: planners.p1BR
      )
      player2BestResponseOracle = try PDDLSerialBestResponseOracle(
        game: game,
        mode: .costDecreasing(maxNumberOfIterations: 100),
        config: config,
        pddlPlanner: planners.p2BR
      )
    case .simulatedAnnealing:
      let constantK = config["constantK"]?.double
      let step = config["step"]?.double
      let temperature = config["temperature"]?.double
      player1BestResponseOracle = try PDDLSerialBestResponseOracle(
        game: game,
        mode: .simulatedAnnealing(temperature: temperature!, step: step!, constantK: constantK!),
        config: config,
        pddlPlanner: planners.p1BR
      )
      player2BestResponseOracle = try PDDLSerialBestResponseOracle(
        game: game,
        mode: .simulatedAnnealing(temperature: temperature!, step: step!, constantK: constantK!),
        config: config,
        pddlPlanner: planners.p2BR
      )
    }
    
    
    let evaluator = PDDLPlansEvaluator(game: game)
    
    let configuration = DoubleOracleConfiguration(
      numberOfInitialPlans: 1,
      maximumNumberOfAttemptsToImproveEquilibriumApprox: 2,
      initialPlansPlannerP1: player1InitOracle,
      initialPlansPlannerP2: player2InitOracle,
      bestResponsePlannerP1: player1BestResponseOracle,
      bestResponsePlannerP2: player2BestResponseOracle,
      strategiesEvaluator: evaluator,
      epsilon: AIKitConfig.defaultEpsilon,
      name: "Configuration name not used")
    return configuration
  }
  
  public static func makeDoublePruningWithNormalBackup(
    for game: PDDLGameFDRRepresentationAnalyticsLogic,
    config: AIKitData
  ) throws -> DoubleOracleConfiguration<PDDLPlansEvaluator,
                                        PDDLDoublePruningOracle,
                                        CompositeSerialBestResponseOracle<
                                          PDDLDoublePruningOracle,
                                          PDDLBestResponseOracle>,
                                        PDDLDoublePruningOracle,
                                        CompositeSerialBestResponseOracle<
                                          PDDLDoublePruningOracle,
                                          PDDLBestResponseOracle>>
  {
    guard let plannerConfig = config["planner"],
          let variant = config["V"]?.string else
          {
      fatalError("Invalid configuration file!")
    }
    precondition(variant == "normalBackup")
    let planner = try PDDLPlannerFactory.make(
      by: plannerConfig,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    
    let doublePruningOracle = PDDLDoublePruningOracle(game: game, pddlPlanner: planner)
    let evaluator = PDDLPlansEvaluator(game: game)
    
    let normalBestResponseOracle = PDDLBestResponseOracle(
      game: game,
      pddlPlanner: planner,
      heuristics: .none
    )
    let bestResponseOracle = CompositeSerialBestResponseOracle(
      oracle1: doublePruningOracle,
      oracle2: normalBestResponseOracle)
    let configuration = DoubleOracleConfiguration(
      numberOfInitialPlans: 1,
      maximumNumberOfAttemptsToImproveEquilibriumApprox: 2,
      initialPlansPlannerP1: doublePruningOracle,
      initialPlansPlannerP2: doublePruningOracle,
      bestResponsePlannerP1: bestResponseOracle,
      bestResponsePlannerP2: bestResponseOracle,
      strategiesEvaluator: evaluator,
      epsilon: AIKitConfig.defaultEpsilon,
      name: "Configuration name not used")
    return configuration
  }
  
  public static func makeDoublePruningOnlyInit(
    for game: PDDLGameFDRRepresentationAnalyticsLogic,
    config: AIKitData
  ) throws -> DoubleOracleConfiguration<PDDLPlansEvaluator,
                                        PDDLDoublePruningOracle,
                                        PDDLBestResponseOracle,
                                        PDDLDoublePruningOracle,
                                        PDDLBestResponseOracle>
  {
    guard let plannerConfig = config["planner"],
          let variant = config["V"]?.string else
    {
      fatalError("Invalid configuration file!")
    }
    precondition(variant == "onlyInit")
    let planner = try PDDLPlannerFactory.make(
      by: plannerConfig,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    
    let doublePruningOracle = PDDLDoublePruningOracle(game: game, pddlPlanner: planner)
    let evaluator = PDDLPlansEvaluator(game: game)
    
    let configuration = DoubleOracleConfiguration(
      numberOfInitialPlans: 1,
      maximumNumberOfAttemptsToImproveEquilibriumApprox: 2,
      initialPlansPlannerP1: doublePruningOracle,
      initialPlansPlannerP2: doublePruningOracle,
      bestResponsePlannerP1: PDDLBestResponseOracle(
        game: game,
        pddlPlanner: planner,
        heuristics: .pruning),
      bestResponsePlannerP2: PDDLBestResponseOracle(
        game: game,
        pddlPlanner: planner,
        heuristics: .pruning),
      strategiesEvaluator: evaluator,
      epsilon: AIKitConfig.defaultEpsilon,
      name: "Configuration name not used")
    return configuration
    
  }
  
  public static func makeDoublePruning(
    for game: PDDLGameFDRRepresentationAnalyticsLogic,
    config: AIKitData
  ) throws -> DoubleOracleConfiguration<PDDLPlansEvaluator,
                                        PDDLDoublePruningOracle,
                                        PDDLDoublePruningOracle,
                                        PDDLDoublePruningOracle,
                                        PDDLDoublePruningOracle>
  {
    guard let plannerConfig = config["planner"],
          let variant = config["V"]?.string else
    {
      fatalError("Invalid configuration file!")
    }
    precondition(variant == "noBackup")
    let planner = try PDDLPlannerFactory.make(
      by: plannerConfig,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    
    let doublePruningOracle = PDDLDoublePruningOracle(game: game, pddlPlanner: planner)
    let evaluator = PDDLPlansEvaluator(game: game)
    
    let configuration = DoubleOracleConfiguration(
      numberOfInitialPlans: 1,
      maximumNumberOfAttemptsToImproveEquilibriumApprox: 2,
      initialPlansPlannerP1: doublePruningOracle,
      initialPlansPlannerP2: doublePruningOracle,
      bestResponsePlannerP1: doublePruningOracle,
      bestResponsePlannerP2: doublePruningOracle,
      strategiesEvaluator: evaluator,
      epsilon: AIKitConfig.defaultEpsilon,
      name: "Configuration name not used")
    return configuration
    
  }
  
  public static func makeBestResponseOracles(
    for game: PDDLGameFDRRepresentationAnalyticsLogic,
    config: AIKitData,
    heuristicsKeyPrefix: String
  ) throws -> (p1BR: PDDLBestResponseOracle, p2BR: PDDLBestResponseOracle)
  {
    let planners = try Self.makeBestResponsePlanners(config: config)
    let p1BRHeuristics = config["p1BestResponsePlanner"]!["\(heuristicsKeyPrefix)heuristics"]?.string ?? "none"
    let p2BRHeuristics = config["p2BestResponsePlanner"]!["\(heuristicsKeyPrefix)heuristics"]?.string ?? "none"
    if p1BRHeuristics == "doublePruning" {
      precondition(p2BRHeuristics == "doublePruning")
    }
    let simAnnStartTemp = config["simAnnStartTemp"]?.double
    let simAnnTempStep = config["simAnnTempStep"]?.double
    let player1BestResponseOracle = PDDLBestResponseOracle(
      game: game,
      pddlPlanner: planners.p1BR,
      heuristics: PDDLInitOracle.Heuristics(p1BRHeuristics),
      simAnnealStartTemperature: simAnnStartTemp,
      simAnnealTemperatureStep: simAnnTempStep
    )
    let player2BestResponseOracle = PDDLBestResponseOracle(
      game: game,
      pddlPlanner: planners.p2BR,
      heuristics: PDDLInitOracle.Heuristics(p2BRHeuristics),
      simAnnealStartTemperature: simAnnStartTemp,
      simAnnealTemperatureStep: simAnnTempStep
    )
    return (p1BR: player1BestResponseOracle, p2BR: player2BestResponseOracle)
  }
  
  public static func make(
    for game: PDDLGameFDRRepresentationAnalyticsLogic,
    config: AIKitData
  ) throws -> DoubleOracleConfiguration<
    PDDLPlansEvaluator,
    PDDLInitOracle,
    PDDLBestResponseOracle,
    PDDLInitOracle,
    PDDLBestResponseOracle>
  {
    let planners = try Self.makePlanners(config: config)
    let p1InitHeuristics = config["p1InitPlanner"]!["heuristics"]?.string ?? config["heuristics"]?.string ?? "none"
    let p2InitHeuristics = config["p2InitPlanner"]!["heuristics"]?.string ?? config["heuristics"]?.string ?? "none"
    let p1BRHeuristics = config["p1BestResponsePlanner"]!["heuristics"]?.string ?? config["heuristics"]?.string ?? "none"
    let p2BRHeuristics = config["p2BestResponsePlanner"]!["heuristics"]?.string ?? config["heuristics"]?.string ?? "none"
    if p1BRHeuristics == "doublePruning" {
      precondition(p2BRHeuristics == "doublePruning" &&
                    p1InitHeuristics == "doublePruning" &&
                    p2InitHeuristics == "doublePruning"
      )
    }
    let simAnnStartTemp = config["simAnnStartTemp"]?.double
    let simAnnTempStep = config["simAnnTempStep"]?.double
    let player1InitOracle = PDDLInitOracle(
      game: game,
      pddlPlanner: planners.p1Init, heuristics: PDDLInitOracle.Heuristics(p1InitHeuristics)
    )
    let player2InitOracle = PDDLInitOracle(
      game: game,
      pddlPlanner: planners.p2Init, heuristics: PDDLInitOracle.Heuristics(p2InitHeuristics)
    )
    let player1BestResponseOracle = PDDLBestResponseOracle(
      game: game,
      pddlPlanner: planners.p1BR,
      heuristics: PDDLInitOracle.Heuristics(p1BRHeuristics),
      simAnnealStartTemperature: simAnnStartTemp,
      simAnnealTemperatureStep: simAnnTempStep
    )
    let player2BestResponseOracle = PDDLBestResponseOracle(
      game: game,
      pddlPlanner: planners.p2BR,
      heuristics: PDDLInitOracle.Heuristics(p2BRHeuristics),
      simAnnealStartTemperature: simAnnStartTemp,
      simAnnealTemperatureStep: simAnnTempStep
    )
    
    let evaluator = PDDLPlansEvaluator(game: game)
    
    let configuration = DoubleOracleConfiguration(
      numberOfInitialPlans: 1,
      maximumNumberOfAttemptsToImproveEquilibriumApprox: 2,
      initialPlansPlannerP1: player1InitOracle,
      initialPlansPlannerP2: player2InitOracle,
      bestResponsePlannerP1: player1BestResponseOracle,
      bestResponsePlannerP2: player2BestResponseOracle,
      strategiesEvaluator: evaluator,
      epsilon: AIKitConfig.defaultEpsilon,
      name: "Configuration name not used")
    return configuration
  }
}
