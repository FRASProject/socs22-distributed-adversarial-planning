//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/24/21.
//

import Foundation

public struct PlanCostFunctionEvaluator: PureStrategyEvaluator {
  public typealias PSP1 = ClassicalPlan
  public typealias PSP2 = CostAllocation
  private let representationFDR: FDRCoding
  private let config: PlanningGame.Config
  private let criticalOperatorsIndices: Set<Int>
  public init(representationFDR: FDRCoding, criticalOperatorsIndices: [Int], config: PlanningGame.Config) {
    self.representationFDR = representationFDR
    self.config = config
    self.criticalOperatorsIndices = Set(criticalOperatorsIndices)
  }
  
  public func evaluate(stratP1 plan: PSP1, stratP2 costFunction: PSP2) -> (Double, Double) {
    let operators = representationFDR.operators
    let cost = plan.actions.map { action -> Int in
      let actionIndex = representationFDR.operatorNameIndex[action]!
      switch config.mode {
      case .costMultiplicative:
        return operators[actionIndex]!.cost * (costFunction[actionIndex] ?? 1)
      case .costAdditive:
        return operators[actionIndex]!.cost + (costFunction[actionIndex] ?? 0)
      case .costZero:
        if criticalOperatorsIndices.contains(actionIndex) {
          return costFunction[actionIndex] ?? 0
        } else {
          return operators[actionIndex]!.cost
        }
      }
      }.sum
    return (-Double(cost), Double(cost))
  }
}
