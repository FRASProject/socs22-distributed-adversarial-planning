//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/2/21.
//

import Foundation

public final class PlanningGamesExperiment: ExperimentInitiableByConfig
{
  public typealias ResultData = Result
  public struct Result: ExperimentData, ExperimentDataResultDictionary {
    public let state: ExperimentDataItemState
    public let index: ExperimentIndex
    public let gameDOResults: DoubleOracleResults<ClassicalPlan, CostAllocation>
    public let numberOfAbstractionVariables: Int
    public let numberOfAbstractionOperators: Int
    public let numberOfVariables: Int
    public let numberOfOperators: Int
    public let abstractGameDOResults: DoubleOracleResults<ClassicalPlan, CostAllocation>
    public let kPlans: PlanningGameTopKPlansExperiment.Result
    public let restrictedGamePlanNumbers: [Int]
    public let restrictedGameDOResults: [DoubleOracleResults<ClassicalPlan, CostAllocation>]
    public var asDictionary: [String: AIKitData] {
      let d = [
        "varNumber": .integer(numberOfVariables),
        "abstractVarNumber": .integer(numberOfAbstractionVariables)
      ].merging(
        gameDOResults.asDictionary,
        uniquingKeysWith: { a, _ in a })
        .merging(
          abstractGameDOResults.asDictionary.map {
        ($0.key + "_ABSTRDO", $0.value)},
        uniquingKeysWith: { a, _ in a })
      if let restrictedGameDOResult = restrictedGameDOResults.last {
        return d.merging(
          restrictedGameDOResult.asDictionary.map { ($0.key + "_KPLANSRESTRICTDO", $0.value) },
          uniquingKeysWith: { a, _ in a }).merging(
            ["delta": .double(abs(restrictedGameDOResult.player2EquilibriumValue - abstractGameDOResults.player2EquilibriumValue))], uniquingKeysWith: { a, _ in a })
      } else {
        return d
      }
    }
    public var tableRow: AIKitTable<AIKitData> {
      let table = AIKitTable<AIKitData>(columns: [
        "varNumber": [.integer(numberOfVariables)],
        "abstractVarNumber": [.integer(numberOfAbstractionVariables)]])
      var resultTable = table.joining(
        with: index.asTable,
        on: index.indexKeys).joining(
          with: gameDOResults.asTable,
          on: []).joining(
            with: abstractGameDOResults.asTable,
            on: index.indexKeys,
            overlappingColumnsSufix: "ABSTRDO")
      
      if let restricedDoTable = restrictedGameDOResults.last?.asTable {
        resultTable = resultTable.joining(
          with: restricedDoTable,
          on: [],
          overlappingColumnsSufix: "KPLANSRESTRICTDO")
        resultTable = resultTable.computingNewColumn(
          name: "delta") { table, rowIdx in
              .double(abs(table[rowIdx, "player2EquilibriumValueABSTRDO"]!.double! -
                          table[rowIdx, "player2EquilibriumValueKPLANSRESTRICTDO"]!.double!))
          }
      }
      
      
      resultTable = resultTable.joining(
        with: kPlans.asTable,
        on: [])
      
      return resultTable
    }
    
    private func graph12Add(xAxis: ChartUtils.Series) -> [ChartUtils.Dataset] {
      var result = [ChartUtils.Dataset]()
      result.append(ChartUtils.Dataset(
        name: "P2 value original game",
        type: .line,
        data: [
          "x" : xAxis,
          "y" : ChartUtils.Series([Double](
            repeating: gameDOResults.player2EquilibriumValue,
            count: restrictedGamePlanNumbers.count))
        ]))
      result.append(ChartUtils.Dataset(
        name: "P2 value abstract game",
        type: .line,
        data: [
          "x" : xAxis,
          "y" : ChartUtils.Series([Double](
            repeating: abstractGameDOResults.player2EquilibriumValue,
            count: restrictedGamePlanNumbers.count))
        ]))
      result.append(ChartUtils.Dataset(
        name: "P2 value restricted game by kPlans",
        type: .line,
        data: [
          "x" : xAxis,
          "y" : ChartUtils.Series(restrictedGameDOResults.map { $0.player2EquilibriumValue })
        ]))
      return result
    }
    
    public var graph1Data: [ChartUtils.Dataset] {
      let xAxis = ChartUtils.Series(restrictedGamePlanNumbers)
      return graph12Add(xAxis: xAxis)
    }
    
    public var graph2Data: [ChartUtils.Dataset] {
      let xAxis = ChartUtils.Series(restrictedGamePlanNumbers.map {
        kPlans.plansComputationDurations[$0]})
      let result = graph12Add(xAxis: xAxis)
//      result.append(ChartUtils.Dataset(
//        name: "DO original game time",
//        type: .xvline,
//        data: [
//          "x" : ChartUtils.Series([gameDOResults.totalTime]),
//        ]))
//      result.append(ChartUtils.Dataset(
//        name: "DO abstract game time",
//        type: .xvline,
//        data: [
//          "x" : ChartUtils.Series([abstractGameDOResults.totalTime]),
//        ]))
      return result
    }
    
    public var chart1Data: ChartUtils.ChartParametersData {
      ChartUtils.ChartParametersData(
        plotTitle: index.indexValues.map {$0.stringRepresentation}.joined(separator: "_"),
        datasets: graph1Data,
        parameters: [
          "xlabel": "Number of symK plans",
          "ylabel": "Player 2 value",
          "legend_location": "upper right"
        ])
    }
    public var chart2Data: ChartUtils.ChartParametersData {
      ChartUtils.ChartParametersData(
        plotTitle: index.indexValues.map {$0.stringRepresentation}.joined(separator: "_"),
        datasets: graph2Data,
        parameters: [
          "xlabel": "symK computation time",
          "ylabel": "Player 2 value",
          "legend_location": "upper right"
        ])
    }
  }
  public var parameters: ExperimentParameters
  public var resultData: Result?
  public var fromCache: Bool?
  public var failError: Error?
  
  public var description: String {
    "PlanningGamesExperiment " + parameters.pathDir
  }
  public let planningGame: PlanningGameFDRRepresentation
  public required init(parameters: ExperimentParameters) {
    self.planningGame = try! ExperimentManager.defaultManager.getPlanningGame(parameters.data)
    self.parameters = parameters
  }
  public func runBody() throws
  {
    //Identity abstraction
    let doExperiment = PlanningGameDOExperiment(
      representationFDR: planningGame.representationFDR,
      criticalOperatorsIndices: planningGame.criticalOperatorsIndices,
      gameConfig: planningGame.game.config,
      parameters: parameters.subParameters("DoubleOracle").addingFileParam("DOT", value: AIKitData("IDENT")))
    try doExperiment.run()
    //let doTable = doExperiment.resultDataTable
    //Abstraction
    //let abstractGame = planningGame
    let numberOfVariables = planningGame.representationFDR.variables.count
    let numberOfOperators = planningGame.representationFDR.operators.count
    print("Number of variables :\(numberOfVariables) Number of operators :\(numberOfOperators)")
    
    let domainName = parameters.data["DOMAIN"]!.string!
    let abstractFDR: FDRCoding
    if domainName == "rosta2" || domainName == "rosta3" {
      abstractFDR = PlanningGamesAbstractions.createAbstractionRosta2(representationFDR: planningGame.representationFDR)
    } else {
      abstractFDR = PlanningGamesAbstractions.createAbstraction(
        representationFDR: planningGame.representationFDR,
        criticalOperatorsIndices: planningGame.criticalOperatorsIndices).0
    }
    let abstractCriticalOperatorsIndices = planningGame.criticalOperatorsIndices.filter {abstractFDR.operators[$0] != nil}
    
    
    let numberOfAbstractionVariable = abstractFDR.variables.count
    let numberOfAbstractOperators = abstractFDR.operators.count
    print("ABSTRACT: Number of variables :\(numberOfAbstractionVariable) Number of operators :\(numberOfAbstractOperators)")
    //SolvingAbstractGame
    let doAbstExperiment = PlanningGameDOExperiment(
      representationFDR: abstractFDR,
      criticalOperatorsIndices: abstractCriticalOperatorsIndices,
      gameConfig: planningGame.game.config,
      parameters: parameters.subParameters("DoubleOracle").addingFileParam("DOT", value: AIKitData("ABSTR")))
    try doAbstExperiment.run()
    //let doAbstractTable = doAbstExperiment.resultDataTable
    // Find k-plans
    let kPlansExperiment = PlanningGameTopKPlansExperiment(
      costPlayerStrategy: doAbstExperiment.resultData!.player2Strategy,
      game: planningGame,
      parameters: parameters)
    try kPlansExperiment.run()
    // Solve original restricted game
    let stepK = parameters.data["kPlans"]!["stepK"]!.integer!
    let kPlans = kPlansExperiment.resultData!.plans
    //let kPlansTable = kPlansExperiment.resultDataTable
    var doRestrictedResults = [DoubleOracleResults<ClassicalPlan, CostAllocation>]()
    var doRestrictedResultsK = [Int]()
    for maxK in stride(from: stepK, to: kPlans.count, by: stepK) {
      let doRestrictedExperiment = PlanningGameRestrictedDO(
        parameters: parameters.addingFileParam("K", value: .integer(maxK)),
        planningGame: planningGame,
        planningStrategies: Array(kPlans[..<maxK]))
      try doRestrictedExperiment.run()
      doRestrictedResults.append(doRestrictedExperiment.resultData!)
      doRestrictedResultsK.append(maxK)
    }
    
    let results = Result(
      state: .finished,
      index: parameters.index,
      gameDOResults: doExperiment.resultData!,
      numberOfAbstractionVariables: numberOfAbstractionVariable,
      numberOfAbstractionOperators: numberOfAbstractOperators,
      numberOfVariables: numberOfVariables,
      numberOfOperators: numberOfAbstractOperators,
      abstractGameDOResults: doAbstExperiment.resultData!,
      kPlans: kPlansExperiment.resultData!,
      restrictedGamePlanNumbers: doRestrictedResultsK,
      restrictedGameDOResults: doRestrictedResults)
    
    try ChartUtils.makeLineGraph(
      chart: results.chart1Data,
      fileNames: [parameters.pathDir + "/restrictedKplansDO.png"])
    try ChartUtils.makeLineGraph(
      chart: results.chart2Data,
      fileNames: [parameters.pathDir + "/restrictedTimeDO.png"])
    save(result: results)
  }
  public var fileNamePrefix: String {
    "planningGame"
  }
  public var fileNameSufix: String {
    ".json"
  }
  public var recompute: Bool {
    true
  }
}
