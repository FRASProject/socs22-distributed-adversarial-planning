//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/5/21.
//

import Foundation
public enum PlanningGamesAbstractions {
  public static func createAbstraction(
    representationFDR: FDRCoding,
    criticalOperatorsIndices: [Int]) -> (
      representationFDR: FDRCoding,
      criticalOperatorsIndices: [Int])
  {
    let operatorVariables = Set(
      criticalOperatorsIndices.flatMap {representationFDR.operators[$0]!.variables})
    let goalVariables = [Array(representationFDR.goal.keys).sorted().first!]
    let abstractFDR = representationFDR.projecting(on: operatorVariables.union(goalVariables))!
    return (representationFDR: abstractFDR, criticalOperatorsIndices: criticalOperatorsIndices)
  }
  public static func createAbstractionRosta2(
    representationFDR: FDRCoding) -> FDRCoding
  {
    let atomNames = ["checked", "visited", "unvisited"]
    let variablesToRemove = representationFDR.variables.filter { (idx, variable) in
      variable.values.someSatisfies { value in
        if atomNames.someSatisfies({value.contains($0)}) {
          let s = value.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
          let n = Int(s)!
          return (n % 2) == 0
        } else {
          return false
        }
      }
    }.map {$0.key}
//    print("Abstr vars: \(abstractionVariables.sorted())")
//    abstractionVariables.sorted().forEach {
//      print("\($0) - \(representationFDR.variables[$0]!.values)")
//    }
    let abstractionVariables = Set(representationFDR.variables.keys).subtracting(variablesToRemove)
    let goalVariables = [Array(representationFDR.goal.keys).sorted().first!]
    let abstractFDR = representationFDR.projecting(on: Set(abstractionVariables).union(goalVariables))!
    return abstractFDR
  }
}
