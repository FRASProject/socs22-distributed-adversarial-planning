//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/30/21.
//

import Foundation

public final class PlanningGameDOSymkExperiment: ExperimentInitiableByConfig {
  public typealias ResultData = Result
  public typealias D = DoubleOracleResults<ClassicalPlan, CostAllocation>
  public struct Result: ExperimentDataResultDictionary, ExperimentData {
    public var asDictionary: [String : AIKitData] {
      let d1 = doResults.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce([:], {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      let d2 = doSymkResults.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce([:], {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      let d = d1.merging(d2.map {("\($0.key)_symk", $0.value)}, uniquingKeysWith: { a, _ in a})
      return d
    }
    public let state: ExperimentDataItemState
    public let doResults: [ExperimentDataWithIndex<D>]
    public let doSymkResults: [ExperimentDataWithIndex<D>]
    public let commonIndex: ExperimentIndex
  }
  public var parameters: ExperimentParameters
  public var resultData: ResultData?
  public var fromCache: Bool?
  public var failError: Error?
  
  public init(parameters: ExperimentParameters) {
    self.parameters = parameters
  }
  
  public func runBody() throws {
    let planningGame = try! ExperimentManager.defaultManager.getPlanningGame(parameters.data)
    let doubleOracleConfig = ExperimentManager.defaultManager.experimentConfig["DoubleOracle"]!.dictionary!
    
    let doParameters = parameters.merging(from: doubleOracleConfig)
    let doExperiments = doParameters.map {
      PlanningGameDOExperiment(
      representationFDR: planningGame.representationFDR,
      criticalOperatorsIndices: planningGame.criticalOperatorsIndices,
      gameConfig: planningGame.game.config,
      parameters: $0)
    }
    try runSubexperiments(doExperiments)
    let doData = doExperiments.compactMap {$0.asExperimentDataWithIndex}
    
    let doubleOracleSymkConfig = ExperimentManager.defaultManager.experimentConfig["DoubleOracleSymK"]!.dictionary!
    let doSymkParameters = parameters.merging(from: doubleOracleSymkConfig)
    let doSymkExperiments = doSymkParameters.map {
      PlanningGameDOExperiment(
      representationFDR: planningGame.representationFDR,
      criticalOperatorsIndices: planningGame.criticalOperatorsIndices,
      gameConfig: planningGame.game.config,
      parameters: $0)
    }
    try runSubexperiments(doSymkExperiments)
    let doSymkData = doSymkExperiments.compactMap {$0.asExperimentDataWithIndex}
    let result = Result(
      state: .finished,
      doResults: doData,
      doSymkResults: doSymkData,
      commonIndex: parameters.index)
    save(result: result)
  }
  public var fileNameSufix: String {
    ".json"
  }
  public var fileNamePrefix: String {
    "resultsDOSYMK"
  }
  public var recompute: Bool {
    false
  }
}
