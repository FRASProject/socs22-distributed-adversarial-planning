//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/29/22.
//

import Foundation

public protocol CounterfactualRegretMinimization where Node == IS.Node {
  associatedtype Node
  associatedtype IS: InformationSet
  associatedtype Action: Hashable
  func isTerminal(_ node: Node) -> Bool
  var root: Node { get }
  func reward(_ node: Node, of player: GameTwoPlayers) -> Double?
  func player(in node: Node) -> GameTwoPlayers
  func player(in informationSet: IS) -> GameTwoPlayers
  func apply(action: Action, in node: Node) -> Node
  func availableActions(at node: Node) -> [Action]
  func chanceNodeActionsDistributions(_ node: Node) -> [Action: Double]
  var informationSetsData: [IS: CFRInformationSetData<Action>] { get set }
  var cfrUpdating: CFRSettingsUpdating { get }
  var cfrAccumulatorWeighting: CFRAccumulatorWeighting { get }
  var cfrRegretMatching: CFRRegretMatching { get }
}

public enum GameTwoPlayers {
  case chance, player1, player2
  public var opponent: GameTwoPlayers? {
    switch self {
    case .chance:
      return nil
    case .player1:
      return .player2
    case .player2:
      return .player1
    }
  }
}


public protocol InformationSet: Hashable {
  associatedtype Node: Hashable
  init(_ node: Node)
}

public enum CFRSettingsUpdating {
  case historiesUpdating, infosetsUpdating
}

public enum CFRAccumulatorWeighting {
  case uniformAccWeighting, linearAccWeighting
}

public enum CFRRegretMatching {
  case regretMatchingNormal, rggretMatchingPlus
}

public struct CFRInformationSetData<Action: Hashable> {
  var regrets: [Action: Double]
  var averageStrategyAccumulator: [Action: Double]
  var regretUpdates: [Action: Double]
  var numberOfUpdates: Int
  var fixRMStrategy: Bool
  var fixAvgStrategy: Bool
  public init() {
    self.regrets = [:]
    self.averageStrategyAccumulator = [:]
    self.regretUpdates = [:]
    self.numberOfUpdates = 0
    self.fixRMStrategy = false
    self.fixAvgStrategy = false
  }
}

extension CounterfactualRegretMinimization {
  public mutating func runIteration(from informationSet: IS, player: GameTwoPlayers, numberOfIterations: Int) {
    for t in 1...numberOfIterations {
      runIteration(from: informationSet, player: player, iterationNumber: t)
    }
  }
  public mutating func runIteration(from informationSet: IS, player: GameTwoPlayers, iterationNumber: Int) {
    _ = runIteration(
      from: root,
      player: .player1,
      player1Probability: 1,
      player2Probability: 1,
      chanceNodeProbability: 1,
      iterationNumber: iterationNumber)
    updateRegrets(for: player)
  }
  
  private mutating func updateRegrets(for player: GameTwoPlayers) {
    for (informationSet, informationSetData) in informationSetsData {
      var informationSetData = informationSetData
      if self.player(in: informationSet) == player && !informationSetData.fixRMStrategy {
        for action in informationSetData.regrets.keys {
          switch cfrRegretMatching {
          case .regretMatchingNormal:
            informationSetData.regrets[action]! += informationSetData.regretUpdates[action]!
          case .rggretMatchingPlus:
            informationSetData.regrets[action] = max(0.0, informationSetData.regrets[action]! + informationSetData.regretUpdates[action]!)
          }
          informationSetData.regretUpdates[action] = 0.0
        }
        informationSetsData[informationSet] = informationSetData
      }
    }
  }
  
  private mutating func runIteration(
    from node: Node,
    player updatingPlayer: GameTwoPlayers,
    player1Probability: Double,
    player2Probability: Double,
    chanceNodeProbability: Double,
    iterationNumber: Int) -> Double
  {
    if player1Probability < AIKitConfig.probabilityEpsilon && player2Probability < AIKitConfig.probabilityEpsilon { return 0.0 }
    if isTerminal(node) { return reward(node, of: updatingPlayer)! }
    let nodePlayer = player(in: node)
    if nodePlayer == .chance {
      var nodeExpectedValue = 0.0
      for (action, actionProbability) in chanceNodeActionsDistributions(node) {
        let child = apply(action: action, in: node)
        let childValue = runIteration(
          from: child,
          player: updatingPlayer,
          player1Probability: player1Probability,
          player2Probability: player2Probability,
          chanceNodeProbability: chanceNodeProbability * actionProbability,
          iterationNumber: iterationNumber)
        nodeExpectedValue += actionProbability * childValue
      }
      return nodeExpectedValue
    }
    
    let epsilonUniform = 0.0
    let currentInformationSet = IS(node)
    var currentInformationSetData = informationSetsData[currentInformationSet] ?? CFRInformationSetData()
    let regrets = currentInformationSetData.regrets
    let regretsCnt = Double(regrets.count)
    let positiveRegretsSum = regrets.values.lazy.filter {$0 >= 0}.sum
    
    let rmProbabilities: [Action: Double]
    if positiveRegretsSum > 0 {
      rmProbabilities = regrets.mapValues {
        (1.0 - epsilonUniform) * max(0.0, $0 / positiveRegretsSum) + epsilonUniform / regretsCnt }
    } else {
      rmProbabilities = regrets.mapValues { _ in 1.0 / regretsCnt }
    }
    var childrenActionValues = [Action: Double]()
    var nodeExpectedValue = 0.0
    for action in availableActions(at: node) {
      let child = apply(action: action, in: node)
      let childValue: Double
      switch nodePlayer {
      case .chance:
        fatalError()
      case .player1:
        childValue = runIteration(
          from: child,
          player: updatingPlayer,
          player1Probability: player1Probability * rmProbabilities[action]!,
          player2Probability: player2Probability,
          chanceNodeProbability: chanceNodeProbability,
          iterationNumber: iterationNumber)
      case .player2:
        childValue = runIteration(
          from: child,
          player: updatingPlayer,
          player1Probability: player1Probability,
          player2Probability: player2Probability * rmProbabilities[action]!,
          chanceNodeProbability: chanceNodeProbability,
          iterationNumber: iterationNumber)
      }
      childrenActionValues[action, default: 0.0] += childValue
      nodeExpectedValue += rmProbabilities[action]! * childValue
    }
    
    if nodePlayer == updatingPlayer {
      for action in availableActions(at: node) {
        if !currentInformationSetData.fixRMStrategy {
          let cfActionRegret = (childrenActionValues[action]! - nodeExpectedValue) *
          chanceNodeProbability * (updatingPlayer == .player1 ? player2Probability : player1Probability)
          switch cfrUpdating {
          case .historiesUpdating:
            currentInformationSetData.regrets[action]! += cfActionRegret
          case .infosetsUpdating:
            currentInformationSetData.regretUpdates[action]! += cfActionRegret
          }
        }
        if !currentInformationSetData.fixAvgStrategy && currentInformationSetData.numberOfUpdates != iterationNumber {
          switch cfrAccumulatorWeighting {
          case .uniformAccWeighting:
            currentInformationSetData.averageStrategyAccumulator[action]! += (updatingPlayer == .player1 ? player1Probability : player2Probability) * rmProbabilities[action]!
          case .linearAccWeighting:
            currentInformationSetData.averageStrategyAccumulator[action]! += Double(iterationNumber) * (updatingPlayer == .player1 ? player1Probability : player2Probability) * rmProbabilities[action]!
          }
        }
      }
    }
    if currentInformationSetData.numberOfUpdates < iterationNumber {
      currentInformationSetData.numberOfUpdates += 1
    }
    informationSetsData[currentInformationSet] = currentInformationSetData
    return nodeExpectedValue
  }
  
  
  public func availableActionsPlayDistribution(in informationSet: IS) -> [Action: Double] {
    let averageStrategyAccumulator = informationSetsData[informationSet]!.averageStrategyAccumulator
    let sum = averageStrategyAccumulator.values.sum
    if sum == 0 {
      let cnt = Double(averageStrategyAccumulator.count)
      return averageStrategyAccumulator.mapValues { _ in 1.0 / cnt }
    } else {
      return averageStrategyAccumulator.mapValues { $0 / sum }
    }
  }
  mutating func runIteration(from informationSet: IS) -> Double {
    fatalError()
  }
}

