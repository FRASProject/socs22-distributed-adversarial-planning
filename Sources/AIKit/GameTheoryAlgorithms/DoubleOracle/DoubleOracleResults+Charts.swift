//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/17/21.
//

import Foundation
import Algorithms

extension DoubleOracleResults {
  public var progressValueChartData: [ChartUtils.Dataset] {
    let statistics = Array(statistics.dropFirst().dropLast())
    let player1Data = statistics.enumerated().filter {
      $0.element.player == 1}
    let player2Data = statistics.enumerated().filter {
      $0.element.player == 2}
    let xAxisPlayer1 = ChartUtils.Series(player1Data.map {$0.offset})
    let xAxisPlayer2 = ChartUtils.Series(player2Data.map {$0.offset})
    let xAxis = ChartUtils.Series(Array(statistics.indices))
    var result = [ChartUtils.Dataset]()
    result.append(ChartUtils.Dataset(
      name: "P1 value",
      type: .line,
      data: [
        "x": xAxis,
        "y": ChartUtils.Series(statistics.map {$0.player1EqVal})
      ]))
    result.append(ChartUtils.Dataset(
      name: "Upper bound (best resp val)",
      type: .line,
      data: [
        "x": xAxisPlayer1,
        "y": ChartUtils.Series(player1Data.map {
          $0.element.bestResponseValueP1 ?? $0.element.player1EqVal})
      ]))
    result.append(ChartUtils.Dataset(
      name: "Lower bound (best resp val)",
      type: .line,
      data: [
        "x": xAxisPlayer2,
        "y": ChartUtils.Series(player2Data.map {
          $0.element.bestResponseValueP1 ?? $0.element.player1EqVal})
      ]))
    return result
  }
  public var oraclesComputationTimeChartData: [ChartUtils.Dataset] {
    let statistics = Array(statistics.dropFirst())
    let xAxisPlayer1 = ChartUtils.Series(Array(statistics.enumerated().filter {
      $0.element.player == 1}.map {$0.offset}))
    let xAxisPlayer2 = ChartUtils.Series(Array(statistics.enumerated().filter {
      $0.element.player == 2}.map {$0.offset}))
    var result = [ChartUtils.Dataset]()
    result.append(ChartUtils.Dataset(
      name: "Player 1",
      type: .line,
      data: [
        "x": xAxisPlayer1,
        "y": ChartUtils.Series(statistics.filter {$0.player == 1}.map {$0.stepDuration})
      ]))
    result.append(ChartUtils.Dataset(
      name: "Player 2",
      type: .line,
      data: [
        "x": xAxisPlayer2,
        "y": ChartUtils.Series(statistics.filter {$0.player == 2}.map {$0.stepDuration})
      ]))
    return result
  }
  public var strategySupportChartData: [ChartUtils.Dataset] {
    let xAxis = ChartUtils.Series(Array(statistics.indices))
    var result = [ChartUtils.Dataset]()
    result.append(ChartUtils.Dataset(
      name: "Player 1",
      type: .line,
      data: [
        "x": xAxis,
        "y": ChartUtils.Series(statistics.map {$0.player1EquilibriumSupportSize})
      ]))
    result.append(ChartUtils.Dataset(
      name: "Player 2",
      type: .line,
      data: [
        "x": xAxis,
        "y": ChartUtils.Series(statistics.map {$0.player2EquilibriumSupportSize})
      ]))
    return result
  }
  public var strategySupportChart: ChartUtils.ChartParametersData {
    ChartUtils.ChartParametersData(
      plotTitle: "DO Equlibrium support size",
      datasets: strategySupportChartData,
      parameters: [
        "xlabel": "Iteration",
        "ylabel": "Support size",
        "legend_location": "upper right"
      ])
  }
  public var progressValueChart: ChartUtils.ChartParametersData {
    ChartUtils.ChartParametersData(
      plotTitle: "DO progress",
      datasets: progressValueChartData,
      parameters: [
        "xlabel": "Iteration",
        "ylabel": "Game value",
        "legend_location": "upper right"
      ])
  }
  public var oraclesComputationTimeChart: ChartUtils.ChartParametersData {
    ChartUtils.ChartParametersData(
      plotTitle: "DO oracles computation time",
      datasets: oraclesComputationTimeChartData,
      parameters: [
        "xlabel": "Iteration",
        "ylabel": "Time",
        "legend_location": "upper right"
      ])
  }
}
