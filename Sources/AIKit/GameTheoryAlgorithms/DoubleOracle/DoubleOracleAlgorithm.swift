//
//  doubleOracle.swift
//  FRASLib
//
//  Created by Pavel R on 15/06/2018.
//

import Foundation
import MathLib

/// Double Oracle Algorithm.
public class DoubleOracleAlgorithm<
  E: PureStrategyEvaluator,
  IOP1: InitOracle,
  IOP2: InitOracle,
  BROP1: BestResponseOracle,
  BROP2: BestResponseOracle>: SolutionChecker, Codable
where E.PSP1 == IOP1.PS,
      E.PSP1 == BROP1.PS,
      E.PSP2 == IOP2.PS,
      E.PSP2 == BROP2.PS,
      BROP1.OPPONENTPS == E.PSP2,
      BROP2.OPPONENTPS == E.PSP1
{
  public typealias PSP1 = E.PSP1
  public typealias PSP2 = E.PSP2
  private let numberOfInitialStrategies: Int
  private var strategiesEvaluator: E!
  private var initialPlansPlannerP1: IOP1!
  private var initialPlansPlannerP2: IOP2!
  private var bestResponsePlannerP1: BROP1!
  private var bestResponsePlannerP2: BROP2!
  public private(set) var player1PureStrategies: [PSP1]
  public private(set) var player2PureStrategies: [PSP2]
  
  /// Used for the checks if the algorithm already added a plan.
  private var player1PureStrategiesSet: Set<PSP1>
  private var player2PureStrategiesSet: Set<PSP2>
  
  public private(set) var player1UtilityMatrix: Matrix
  public private(set) var player2UtilityMatrix: Matrix
  public private(set) var player1EquilibriumApprox: [Double]
  public private(set) var player2EquilibriumApprox: [Double]
  public private(set) var player1EquilibriumValueApprox: Double
  public private(set) var player2EquilibriumValueApprox: Double
  private var previousPlayer1EquilibriumValue: Double?
  private var previousPlayer2EquilibriumValue: Double?
  
  private let matrixDataSemaphore: DispatchSemaphore
  
  /// Two doubles that differ less than epsilon are considered equal.
  private let epsilon: Double
  private let plansCacheDir: URL?
  private var verbose: Bool
  private var verbosePrintPlans: Bool
  
  /// Best responses computations counters.
  public private(set) var numberOfBestResponsesComputationsPlayer1: Int
  public private(set) var numberOfBestResponsesComputationsPlayer2: Int
  public private(set) var log : [DoubleOracleLogItem]
  
  /// Convergence counters.
  private var numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1: Int
  private var numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2: Int
  /// Convergence threshold.
  private let maximumNumberOfAttemptsToImproveEquilibriumApprox: Int
  
  public private(set) var startTime: Date
  private let offsetTime: Date?
  private let configurationName: String
  
  /// Initialize class and computes initial plans..
  /// - Parameters:
  ///   - numberOfInitialPlans: Number of initial plans.
  ///   - maximumNumberOfAttemptsToImproveEquilibriumApprox:
  ///   - initialPlansPlannerP1:
  ///   - initialPlansPlannerP2:
  ///   - bestResponsePlannerP1:
  ///   - bestResponsePlannerP2:
  ///   - configurationName:
  ///   - epsilon:
  ///   - plansCacheDirName:
  ///   - verbose:
  ///   - verbosePrintPlans:
  public init(
    numberOfInitialPlans: Int = 1,
    maximumNumberOfAttemptsToImproveEquilibriumApprox: Int,
    initialPlansPlannerP1: IOP1,
    initialPlansPlannerP2: IOP2,
    bestResponsePlannerP1: BROP1,
    bestResponsePlannerP2: BROP2,
    configurationName: String,
    epsilon: Double = AIKitConfig.defaultEpsilon,
    strategiesEvaluator: E,
    plansCacheDir plansCacheDirName: String? = nil,
    verbose: Bool = false,
    verbosePrintPlans: Bool = false) throws
  {
    self.startTime = Date()
    self.offsetTime = nil
    self.previousPlayer1EquilibriumValue = nil
    self.previousPlayer2EquilibriumValue = nil
    self.strategiesEvaluator = strategiesEvaluator
    self.configurationName = configurationName
    self.numberOfInitialStrategies = numberOfInitialPlans
    self.initialPlansPlannerP1 = initialPlansPlannerP1
    self.initialPlansPlannerP2 = initialPlansPlannerP2
    self.bestResponsePlannerP1 = bestResponsePlannerP1
    self.bestResponsePlannerP2 = bestResponsePlannerP2
    self.epsilon = epsilon
    if let plansCacheDirName = plansCacheDirName {
      self.plansCacheDir = URL(fileURLWithPath: plansCacheDirName, isDirectory: true)
    } else {
      self.plansCacheDir = nil
    }
    
    self.matrixDataSemaphore = DispatchSemaphore(value: 1)
    
    if let plansCacheDir = self.plansCacheDir {
      let fileManager = FileManager.default
      do {
        try fileManager.createDirectory(
          at: plansCacheDir,
          withIntermediateDirectories: true,
          attributes: nil)
      } catch {
        print("Plans cache directory allready exists!")
      }
    }
    
    self.verbose = verbose
    self.verbosePrintPlans = verbosePrintPlans
    self.numberOfBestResponsesComputationsPlayer1 = 0
    self.numberOfBestResponsesComputationsPlayer2 = 0
    self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1 = 0
    self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2 = 0
    self.maximumNumberOfAttemptsToImproveEquilibriumApprox = maximumNumberOfAttemptsToImproveEquilibriumApprox
    
    if verbose {
      print("DO       INITIALIZING")
      print("DO       FINDING DEFAULT PLANS")
    }
    
    let (player1Plans, logP1String, player1PlansCompDuration) = try self.initialPlansPlannerP1.findPlans(
      for: 1,
      maxNumberOfPlans: numberOfInitialPlans,
      plansCacheDir: plansCacheDir)
    
    self.player1PureStrategies = player1Plans
    if verbose {
      print("Player 1 plan found")
      print(String(format: "Elapsed time : %.2f sec.", -startTime.timeIntervalSinceNow))
    }
    if verbosePrintPlans {
      print("       P1 plans:")
      self.player1PureStrategies.forEach {
        print($0)
        //print(plans: $0)
      }
    }
    
    let (player2Plans, logP2String, player2PlansCompDuration) = try self.initialPlansPlannerP2.findPlans(
      for: 2,
      maxNumberOfPlans: numberOfInitialPlans,
      plansCacheDir: plansCacheDir)
    
    self.player2PureStrategies = player2Plans
    
    if verbose {
      print("Player 2 plan found")
      print(String(format: "Elapsed time : %.2f sec.", -startTime.timeIntervalSinceNow))
    }
    if verbosePrintPlans {
      print("       P2 plans:")
      self.player2PureStrategies.forEach {
        print($0)
        //print(plans: $0)
      }
    }
    
    self.player1PureStrategiesSet = Set<PSP1>(player1Plans)
    self.player2PureStrategiesSet = Set<PSP2>(player2Plans)
    if verbose {
      print("DO       COMPUTING INITIAL APPROXIMATION OF THE EQUILIBRIUM")
    }
    (self.player1UtilityMatrix, self.player2UtilityMatrix) = try UtilityMatrixUtils.constructUtilityMatrix(
      evaluator: self.strategiesEvaluator,
      player1PureStrategies: player1Plans,
      player2PureStrategies: player2Plans)

    (self.player1EquilibriumValueApprox, self.player1EquilibriumApprox) = NormalFormGame.computeEquilibriumTwoPlayersZeroSum(
      rowPlayer1Utility: player1UtilityMatrix)
    (self.player2EquilibriumValueApprox, self.player2EquilibriumApprox) = NormalFormGame.computeEquilibriumTwoPlayersZeroSum(
      columnPlayer2Utility: player2UtilityMatrix)
    
    let firstLogItem = DoubleOracleLogItem(
      iteration: 0,
      elapsedTime: -startTime.timeIntervalSinceNow,
      stepDuration: player1PlansCompDuration.last! + player2PlansCompDuration.last!,
      step: .initialization,
      player1Equilibrium: player1EquilibriumApprox,
      player2Equilibrium: player2EquilibriumApprox,
      player1EquilibriumValue: player1EquilibriumValueApprox,
      player2EquilibriumValue: player2EquilibriumValueApprox,
      previousPlayer1EquilibriumValue: nil,
      previousPlayer2EquilibriumValue: nil,
      plannerLog: .array([logP1String,logP2String]),
      plannerDescription: self.initialPlansPlannerP1.shortDescription + " <-> " +
        self.initialPlansPlannerP2.shortDescription,
      bestResponseValueP1: nil,
      bestResponseValueP2: nil)
    if verbose {
      print("DO FINISHED INIT: IT:  0: " + firstLogItem.description)
    }
    self.log = [firstLogItem]
  }
  
  public convenience init(
    configuration: DoubleOracleConfiguration<E, IOP1, BROP1, IOP2, BROP2>,
    plansCacheDir: String? = nil,
    verbose: Bool = false) throws
  {
    try self.init(
      numberOfInitialPlans: configuration.numberOfInitialPlans,
      maximumNumberOfAttemptsToImproveEquilibriumApprox: configuration.maximumNumberOfAttemptsToImproveEquilibriumApprox,
      initialPlansPlannerP1: configuration.initialPlansPlannerP1,
      initialPlansPlannerP2: configuration.initialPlansPlannerP2,
      bestResponsePlannerP1: configuration.bestResponsePlannerP1,
      bestResponsePlannerP2: configuration.bestResponsePlannerP2,
      configurationName: configuration.description,
      epsilon: configuration.epsilon,
      strategiesEvaluator: configuration.strategiesEvaluator,
      plansCacheDir: plansCacheDir,
      verbose: verbose)
  }
  
  public func setPlannersFrom(
    configuration: DoubleOracleConfiguration<E, IOP1, BROP1, IOP2, BROP2>)
  {
    initialPlansPlannerP1 = configuration.initialPlansPlannerP1
    initialPlansPlannerP2 = configuration.initialPlansPlannerP2
    bestResponsePlannerP1 = configuration.bestResponsePlannerP1
    bestResponsePlannerP2 = configuration.bestResponsePlannerP2
    strategiesEvaluator = configuration.strategiesEvaluator
  }
  
  private func appendToLog(
    step : DoubleOracleLogItem.Step,
    plannerLog: AIKitData,
    plannerDesc: String,
    duration: TimeInterval,
    bestResponseValP1: Double?,
    bestResponseValP2: Double?)
  {
    let item = DoubleOracleLogItem(
      iteration: log.count,
      elapsedTime: -startTime.timeIntervalSinceNow,
      stepDuration: duration,
      step: step,
      player1Equilibrium: player1EquilibriumApprox,
      player2Equilibrium: player2EquilibriumApprox,
      player1EquilibriumValue: player1EquilibriumValueApprox,
      player2EquilibriumValue: player2EquilibriumValueApprox,
      previousPlayer1EquilibriumValue: previousPlayer1EquilibriumValue,
      previousPlayer2EquilibriumValue: previousPlayer2EquilibriumValue,
      plannerLog: plannerLog,
      plannerDescription: plannerDesc,
      bestResponseValueP1: bestResponseValP1,
      bestResponseValueP2: bestResponseValP2)
    if verbose {
      print(String(format: "Elapsed time : %.2f sec.", -startTime.timeIntervalSinceNow))
      print(String(format: "FINISHED IT:%3d: ", log.count) + item.description)
      let p1EqStr = player1EquilibriumApprox.map {String(format: "%.2f", $0)}.joined(separator: " ")
      let p2EqStr = player2EquilibriumApprox.map {String(format: "%.2f", $0)}.joined(separator: " ")
      print("CUR EQUILIBRIUM P1 [" + p1EqStr + "] P2 [" + p2EqStr + "]")
    }
    log.append(item)
  }
  
  public func isGoodEnough(strategy: Any, player: Int) throws -> Bool {
    precondition(player == 1 || player == 2)
    if player == 1 {
      let strat = strategy as! [PSP1]
      let notAddedStrats = strat.filter {!player1PureStrategiesSet.contains($0)}
      if notAddedStrats.isEmpty {
        return false
      }
      let (_,_,p1Values,_) = try computeBestResponseValueP1(bestResponse: notAddedStrats)
      return p1Values.max()! > self.player1EquilibriumValueApprox + AIKitConfig.defaultEpsilon
    } else {
      let strat = strategy as! [PSP2]
      let notAddedStrats = strat.filter {!player2PureStrategiesSet.contains($0)}
      if notAddedStrats.isEmpty {
        return false
      }
      let (_,_,_,p2Values) = try computeBestResponseValueP2(bestResponse: strat)
      return p2Values.max()! > self.player2EquilibriumValueApprox + AIKitConfig.defaultEpsilon
    }
  }
  
  func computeBestResponseValueP2(bestResponse: [PSP2]) throws -> (
    p1Matrix: Matrix,
    p2Matrix: Matrix,
    p1BRValue: [Double],
    p2BRValue: [Double])
  {
    self.matrixDataSemaphore.wait()
    let player1Plans = self.player1PureStrategies
    let player1UtilityMatrix = self.player1UtilityMatrix
    let player2UtilityMatrix = self.player2UtilityMatrix
    let player1EquilibriumApprox = self.player1EquilibriumApprox
    self.matrixDataSemaphore.signal()
    
    let (player1UtilityColumn, player2UtilityColumn) = try UtilityMatrixUtils.constructUtilityMatrix(
      evaluator: self.strategiesEvaluator,
      player1PureStrategies: player1Plans,
      player2PureStrategies: bestResponse)
    var bestResponseValueP1 = [Double]()
    var bestResponseValueP2 = [Double]()
    for idx in bestResponse.indices {
      var columnPlayer2Strat = [Double](repeating: 0.0, count: bestResponse.count)
      columnPlayer2Strat[idx] = 1.0
      let bestResponseValue = NormalFormGame.evaluate(
        rowPlayer1Strat: player1EquilibriumApprox,
        columnPlayer2Strat: columnPlayer2Strat,
        rowPlayer1UtilityMatrix: player1UtilityColumn,
        columnPlayer2UtilityMatrix: player2UtilityColumn)
      bestResponseValueP1.append(bestResponseValue.player1)
      bestResponseValueP2.append(bestResponseValue.player2)
    }
    let newPlayer1UtilityMatrix = player1UtilityMatrix.appendRight(matrix: player1UtilityColumn)
    let newPlayer2UtilityMatrix = player2UtilityMatrix.appendRight(matrix: player2UtilityColumn)
    return (
      p1Matrix: newPlayer1UtilityMatrix,
      p2Matrix: newPlayer2UtilityMatrix,
      p1BRValue: bestResponseValueP1,
      p2BRValue: bestResponseValueP2)
  }
  
  func computeBestResponseValueP1(bestResponse: [PSP1]) throws -> (
    p1Matrix: Matrix,
    p2Matrix: Matrix,
    p1BRValue: [Double],
    p2BRValue: [Double])
  {
    self.matrixDataSemaphore.wait()
    let player2Plans = self.player2PureStrategies
    let player1UtilityMatrix = self.player1UtilityMatrix
    let player2UtilityMatrix = self.player2UtilityMatrix
    let player2EquilibriumApprox = self.player2EquilibriumApprox
    self.matrixDataSemaphore.signal()

    let (player1UtilityRow, player2UtilityRow) = try UtilityMatrixUtils.constructUtilityMatrix(
      evaluator: self.strategiesEvaluator,
      player1PureStrategies: bestResponse,
      player2PureStrategies: player2Plans)
    var bestResponseValueP1 = [Double]()
    var bestResponseValueP2 = [Double]()
    for idx in bestResponse.indices {
      var rowPlayer1Strat = [Double](repeating: 0.0, count: bestResponse.count)
      rowPlayer1Strat[idx] = 1.0
      let bestResponseValue = NormalFormGame.evaluate(
        rowPlayer1Strat: rowPlayer1Strat,
        columnPlayer2Strat: player2EquilibriumApprox,
        rowPlayer1UtilityMatrix: player1UtilityRow,
        columnPlayer2UtilityMatrix: player2UtilityRow)
      bestResponseValueP1.append(bestResponseValue.player1)
      bestResponseValueP2.append(bestResponseValue.player2)
    }
    
    let newPlayer1UtilityMatrix = player1UtilityMatrix.appendBellow(matrix: player1UtilityRow)
    let newPlayer2UtilityMatrix = player2UtilityMatrix.appendBellow(matrix: player2UtilityRow)
    return (
      p1Matrix: newPlayer1UtilityMatrix,
      p2Matrix: newPlayer2UtilityMatrix,
      p1BRValue: bestResponseValueP1,
      p2BRValue: bestResponseValueP2)
  }
  
  private func recomputeEquilibrium() {
    self.previousPlayer1EquilibriumValue = self.player1EquilibriumValueApprox
    self.previousPlayer2EquilibriumValue = self.player2EquilibriumValueApprox
    (self.player1EquilibriumValueApprox, self.player1EquilibriumApprox) = NormalFormGame.computeEquilibriumTwoPlayersZeroSum(
      rowPlayer1Utility: self.player1UtilityMatrix)
    (self.player2EquilibriumValueApprox, self.player2EquilibriumApprox) = NormalFormGame.computeEquilibriumTwoPlayersZeroSum(
      columnPlayer2Utility: self.player2UtilityMatrix)
  }
  
  /// Computes and add a best response.
  /// Returns false if adding best response to the restricted matrix have not improved equilibrium.
  /// Return false if found best response is strictly less then equilibrium approximation. NO NO
  func findAndAddBestResponseP1() throws -> Bool {
    let opponentEquilibriumStrategy = MixedStrategy(
      player2PureStrategies,
      distribution: player2EquilibriumApprox)
    let iteration = self.log.count
    let planCache = self.plansCacheDir?.appendingPathComponent(
      "player1BestResponseIteration_\(iteration).json",
      isDirectory: false)
    let plannerStartTime = Date()
    let (bestResponseStrategies, _, bRespLog, _) = try self.bestResponsePlannerP1.findBestResponse(
      player: 1,
      to: 2,
      opponentStrat: opponentEquilibriumStrategy,
      utilityLowerBound: self.player1EquilibriumValueApprox,
      plansCacheFile: planCache,
      continueFunction: nil,
      solutionChecker: self)
    let plannerComputationDuration = -plannerStartTime.timeIntervalSinceNow
    if bestResponseStrategies.isEmpty {
      // Oracle failure
      self.previousPlayer1EquilibriumValue = self.player1EquilibriumValueApprox
      self.previousPlayer2EquilibriumValue = self.player2EquilibriumValueApprox
      self.appendToLog(
        step: .player1BestResponseFailure,
        plannerLog: bRespLog,
        plannerDesc: self.bestResponsePlannerP1.shortDescription,
        duration: plannerComputationDuration,
        bestResponseValP1: nil,
        bestResponseValP2: nil)
      self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1 += 1
      return false
    }
    let bestResponseStrategiesNotAddedBefore = bestResponseStrategies.filter {
      !self.player1PureStrategiesSet.contains($0)
    }
    if !bestResponseStrategiesNotAddedBefore.isEmpty {
      // We found some plans not added before.
      let (newPlayer1UtilityMatrix, newPlayer2UtilityMatrix, bestResponseValuesP1, bestResponseValuesP2) = try self.computeBestResponseValueP1(
        bestResponse: bestResponseStrategiesNotAddedBefore)
      let bestResponseValueP1Max = bestResponseValuesP1.max()!
      let bestResponseValueP2Max = bestResponseValuesP2[bestResponseValuesP1.firstIndex(
        of: bestResponseValueP1Max)!]
      if bestResponseValueP1Max >= self.player1EquilibriumValueApprox + self.epsilon {
        // Player 1 found improving plan
        self.player1PureStrategiesSet.formUnion(bestResponseStrategiesNotAddedBefore)
        self.player1PureStrategies.append(contentsOf: bestResponseStrategiesNotAddedBefore)
        self.player1UtilityMatrix = newPlayer1UtilityMatrix
        self.player2UtilityMatrix = newPlayer2UtilityMatrix
        recomputeEquilibrium()
        self.numberOfBestResponsesComputationsPlayer1 += 1
        self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1 = 0
        self.appendToLog(
          step: .player1BestPureResponse,
          plannerLog: bRespLog,
          plannerDesc: self.bestResponsePlannerP1.shortDescription,
          duration: plannerComputationDuration,
          bestResponseValP1: bestResponseValueP1Max,
          bestResponseValP2: bestResponseValueP2Max)
        return true
      } else {
        // No found plan improved equilibrium.
        self.previousPlayer1EquilibriumValue = self.player1EquilibriumValueApprox
        self.previousPlayer2EquilibriumValue = self.player2EquilibriumValueApprox
        self.appendToLog(
          step: .player1BestResponseFailureNotImprovedEquilibriumApprox,
          plannerLog: bRespLog,
          plannerDesc: self.bestResponsePlannerP1.shortDescription,
          duration: plannerComputationDuration,
          bestResponseValP1: bestResponseValueP1Max,
          bestResponseValP2: bestResponseValueP2Max)
        self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1 += 1
        return false
      }
    } else {
      // All found plans has been added before.
      let (_, _, bestResponseValuesP1, bestResponseValuesP2) = try self.computeBestResponseValueP1(
        bestResponse: bestResponseStrategies)
      let bestResponseValueP1Max = bestResponseValuesP1.max()!
      let bestResponseValueP2Max = bestResponseValuesP2[bestResponseValuesP1.firstIndex(
        of: bestResponseValueP1Max)!]
      self.previousPlayer1EquilibriumValue = self.player1EquilibriumValueApprox
      self.previousPlayer2EquilibriumValue = self.player2EquilibriumValueApprox
      self.appendToLog(
        step: .player1BestResponseFailurePlanAlreadyAdded,
        plannerLog: bRespLog,
        plannerDesc: self.bestResponsePlannerP1.shortDescription,
        duration: plannerComputationDuration,
        bestResponseValP1: bestResponseValueP1Max,
        bestResponseValP2: bestResponseValueP2Max)
      self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1 += 1
      return false
    }
  }
  
  func findAndAddBestResponseP2() throws -> Bool {
    let opponentEquilibriumStrategy = MixedStrategy(player1PureStrategies, distribution: player1EquilibriumApprox)
    let iteration = self.log.count
    let planCache = self.plansCacheDir?.appendingPathComponent(
      "player2BestResponseIteration_\(iteration).json",
      isDirectory: false)
    let plannerStartTime = Date()
    let (bestResponseStrategies, _, bRespLog, _) = try self.bestResponsePlannerP2.findBestResponse(
      player: 2,
      to: 1,
      opponentStrat: opponentEquilibriumStrategy,
      utilityLowerBound: self.player2EquilibriumValueApprox,
      plansCacheFile: planCache,
      continueFunction: nil,
      solutionChecker: self)
    let plannerComputationDuration = -plannerStartTime.timeIntervalSinceNow
    if bestResponseStrategies.isEmpty {
      // Oracle failure
      self.previousPlayer1EquilibriumValue = self.player1EquilibriumValueApprox
      self.previousPlayer2EquilibriumValue = self.player2EquilibriumValueApprox
      self.appendToLog(
        step: .player2BestResponseFailure,
        plannerLog: bRespLog,
        plannerDesc: self.bestResponsePlannerP2.shortDescription,
        duration: plannerComputationDuration,
        bestResponseValP1:nil,
        bestResponseValP2:nil)
      self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2 += 1
      return false
    }
    let bestResponseStrategiesNotAddedBefore = bestResponseStrategies.filter {
      !self.player2PureStrategiesSet.contains($0)
    }
    if !bestResponseStrategiesNotAddedBefore.isEmpty {
      // We found some plans not added before.
      let (newPlayer1UtilityMatrix, newPlayer2UtilityMatrix, bestResponseValuesP1, bestResponseValuesP2) = try self.computeBestResponseValueP2(bestResponse: bestResponseStrategiesNotAddedBefore)
      let bestResponseValueP2Max = bestResponseValuesP2.max()!
      let bestResponseValueP1Max = bestResponseValuesP1[bestResponseValuesP2.firstIndex(of: bestResponseValueP2Max)!]
      if bestResponseValueP2Max >= self.player2EquilibriumValueApprox + self.epsilon {
        // Player 2 found improving plan.
        self.player2PureStrategiesSet.formUnion(bestResponseStrategiesNotAddedBefore)
        self.player2PureStrategies.append(contentsOf: bestResponseStrategiesNotAddedBefore)
        self.player1UtilityMatrix = newPlayer1UtilityMatrix
        self.player2UtilityMatrix = newPlayer2UtilityMatrix
        recomputeEquilibrium()
        self.numberOfBestResponsesComputationsPlayer1 += 1
        self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1 = 0
        self.appendToLog(
          step: .player2BestPureResponse,
          plannerLog: bRespLog,
          plannerDesc: self.bestResponsePlannerP2.shortDescription,
          duration: plannerComputationDuration,
          bestResponseValP1: bestResponseValueP1Max,
          bestResponseValP2: bestResponseValueP2Max)
        return true
      } else {
        // No found plan improved equilibrium.
        self.previousPlayer1EquilibriumValue = self.player1EquilibriumValueApprox
        self.previousPlayer2EquilibriumValue = self.player2EquilibriumValueApprox
        self.appendToLog(
          step: .player2BestResponseFailureNotImprovedEquilibriumApprox,
          plannerLog: bRespLog,
          plannerDesc: self.bestResponsePlannerP2.shortDescription,
          duration: plannerComputationDuration,
          bestResponseValP1:bestResponseValueP1Max,
          bestResponseValP2:bestResponseValueP2Max)
        self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2 += 1
        return false
      }
    } else {
      // All found plans has been added before.
      let (_, _, bestResponseValuesP1, bestResponseValuesP2) = try self.computeBestResponseValueP2(bestResponse: bestResponseStrategies)
      let bestResponseValueP2Max = bestResponseValuesP2.max()!
      let bestResponseValueP1Max = bestResponseValuesP1[bestResponseValuesP2.firstIndex(of: bestResponseValueP2Max)!]
      self.previousPlayer1EquilibriumValue = self.player1EquilibriumValueApprox
      self.previousPlayer2EquilibriumValue = self.player2EquilibriumValueApprox
      self.appendToLog(
        step: .player2BestResponseFailurePlanAlreadyAdded,
        plannerLog: bRespLog,
        plannerDesc: self.bestResponsePlannerP2.shortDescription,
        duration: plannerComputationDuration,
        bestResponseValP1: bestResponseValueP1Max,
        bestResponseValP2: bestResponseValueP2Max)
      self.numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2 += 1
      return false
    }
  }
  
  /// Starts DO algorithm.
  /// - Parameters:
  ///   - startWithPlayer: DO computes first best response for this player.
  ///   - numberOfIterations: Maximum number of iterations.
  /// - Returns: True if DO converged, False if not.
  public func run(startWithPlayer: Int = 1, numberOfIterations: Int? = nil) throws -> Bool {
    var player1AddedBestResponse = true
    var player2AddedBestResponse = true
    var iteration = 1
    func checkConvergence() -> Bool {
      if !player1AddedBestResponse && !player2AddedBestResponse {
        // Algorithm converged
        appendToLog(
          step: .algorithmConverged,
          plannerLog: "",
          plannerDesc: "",
          duration: 0.0,
          bestResponseValP1: nil,
          bestResponseValP2: nil)
        if verbose {
          print("Player 1 utility matrix:")
          print(player1UtilityMatrix)
          print("Player 1 equilibrium strategy:")
          print(player1EquilibriumApprox)
          print("Player 2 utility matrix:")
          print(player2UtilityMatrix)
          print("Player 2 equilibrium strategy:")
          print(player2EquilibriumApprox)
        }
        return true
      } else {
        return false
      }
    }
    var firstIt = true
    while true {
      if !firstIt || startWithPlayer == 1 {
        player1AddedBestResponse = try findAndAddBestResponseP1()
        if checkConvergence() {
          return true
        }
        
        try saveState()
      }
      firstIt = false
      
      player2AddedBestResponse = try findAndAddBestResponseP2()
      
      if checkConvergence() {
        return true
      }
      
      try saveState()
      
      if let maxIterations = numberOfIterations, iteration >= maxIterations {
        return false
      }
      
      iteration += 1
    }
  }
  
  func saveState() throws {
    if let plansCacheDir = plansCacheDir {
      let encoder = JSONEncoder()
      let data = try encoder.encode(self)
      try? FileManager.default.removeItem(
        at: plansCacheDir.appendingPathComponent("dostate_n.json"))
      try data.write(to: plansCacheDir.appendingPathComponent("dostate_n.json"))
      try? FileManager.default.removeItem(at: plansCacheDir.appendingPathComponent("dostate.json"))
      try FileManager.default.moveItem(
        at: plansCacheDir.appendingPathComponent("dostate_n.json"),
        to: plansCacheDir.appendingPathComponent("dostate.json"))
    }
  }
  
  required public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    numberOfInitialStrategies = try container.decode(Int.self, forKey: .numberOfInitialPlans)
    initialPlansPlannerP1 = nil
    initialPlansPlannerP2 = nil
    bestResponsePlannerP1 = nil
    bestResponsePlannerP2 = nil
    strategiesEvaluator = nil
    player1PureStrategies = try container.decode([PSP1].self, forKey: .player1Plans)
    player2PureStrategies = try container.decode([PSP2].self, forKey: .player2Plans)
    
    player1PureStrategiesSet = Set(player1PureStrategies)
    player2PureStrategiesSet = Set(player2PureStrategies)
    
    player1UtilityMatrix = try container.decode(Matrix.self, forKey: .player1UtilityMatrix)
    player2UtilityMatrix = try container.decode(Matrix.self, forKey: .player2UtilityMatrix)
    player1EquilibriumApprox = try container.decode(
      [Double].self,
      forKey: .player1EquilibriumApprox)
    player2EquilibriumApprox = try container.decode(
      [Double].self,
      forKey: .player2EquilibriumApprox)
    player1EquilibriumValueApprox = try container.decode(
      Double.self,
      forKey: .player1EquilibriumValueApprox)
    player2EquilibriumValueApprox = try container.decode(
      Double.self,
      forKey: .player2EquilibriumValueApprox)
    epsilon = try container.decode(Double.self, forKey: .epsilon)
    plansCacheDir = try container.decode(URL.self, forKey: .plansCacheDir)
    verbose = try container.decode(Bool.self, forKey: .verbose)
    verbosePrintPlans = try container.decode(Bool.self, forKey: .verbosePrintPlans)
    numberOfBestResponsesComputationsPlayer1 = try container.decode(
      Int.self,
      forKey: .numberOfBestResponsesComputationsPlayer1)
    numberOfBestResponsesComputationsPlayer2 = try container.decode(
      Int.self,
      forKey: .numberOfBestResponsesComputationsPlayer2)
    log = try container.decode([DoubleOracleLogItem].self, forKey: .log)
    numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1 = try container.decode(
      Int.self,
      forKey: .numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1)
    numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2 = try container.decode(
      Int.self,
      forKey: .numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2)
    maximumNumberOfAttemptsToImproveEquilibriumApprox = try container.decode(
      Int.self,
      forKey: .maximumNumberOfAttemptsToImproveEquilibriumApprox)
    let oldStartTime = try container.decode(Date.self, forKey: .startTime)
    configurationName = try container.decode(String.self, forKey: .configurationName)
    self.offsetTime = nil
    let offset = try container.decode(Date.self, forKey: .offsetTime)
    let timeElapsedSinceSave = -offset.timeIntervalSinceNow
    print(timeElapsedSinceSave)
    startTime = oldStartTime.addingTimeInterval(timeElapsedSinceSave)
    self.matrixDataSemaphore = DispatchSemaphore(value: 1)
  }
  
  private enum CodingKeys: String, CodingKey {
    case numberOfInitialPlans
    case player1UtilityMatrix
    case player2UtilityMatrix
    case player1Plans
    case player2Plans
    case player1EquilibriumApprox
    case player2EquilibriumApprox
    case player1EquilibriumValueApprox
    case player2EquilibriumValueApprox
    case epsilon
    case plansCacheDir
    case verbose
    case verbosePrintPlans
    case numberOfBestResponsesComputationsPlayer1
    case numberOfBestResponsesComputationsPlayer2
    case log
    case numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1
    case numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2
    case maximumNumberOfAttemptsToImproveEquilibriumApprox
    case startTime
    case configurationName
    case offsetTime
  }
  
  enum CodingError: Error {
    case decoding(String)
  }
  
  public func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(numberOfInitialStrategies, forKey: .numberOfInitialPlans)
    try container.encode(player1UtilityMatrix, forKey: .player1UtilityMatrix)
    try container.encode(player2UtilityMatrix, forKey: .player2UtilityMatrix)
    try container.encode(player1PureStrategies, forKey: CodingKeys.player1Plans)
    try container.encode(player2PureStrategies, forKey: CodingKeys.player2Plans)
    try container.encode(player1EquilibriumApprox, forKey: .player1EquilibriumApprox)
    try container.encode(player2EquilibriumApprox, forKey: .player2EquilibriumApprox)
    try container.encode(player1EquilibriumValueApprox, forKey: .player1EquilibriumValueApprox)
    try container.encode(player2EquilibriumValueApprox, forKey: .player2EquilibriumValueApprox)
    try container.encode(epsilon, forKey: .epsilon)
    try container.encode(plansCacheDir, forKey: .plansCacheDir)
    try container.encode(verbose, forKey: .verbose)
    try container.encode(verbosePrintPlans, forKey: .verbosePrintPlans)
    try container.encode(
      numberOfBestResponsesComputationsPlayer1,
      forKey: .numberOfBestResponsesComputationsPlayer1)
    try container.encode(
      numberOfBestResponsesComputationsPlayer2,
      forKey: .numberOfBestResponsesComputationsPlayer2)
    try container.encode(log, forKey: .log)
    try container.encode(
      numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1,
      forKey: .numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer1)
    try container.encode(
      numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2,
      forKey: .numberOfLastBestResponsesWhereEquilibriumApproxHasNotBeenImprovedPlayer2)
    try container.encode(
      maximumNumberOfAttemptsToImproveEquilibriumApprox,
      forKey: .maximumNumberOfAttemptsToImproveEquilibriumApprox)
    try container.encode(startTime, forKey: .startTime)
    try container.encode(configurationName, forKey: .configurationName)
    try container.encode(Date(), forKey: .offsetTime) // Save current time
    
  }
  
}

extension DoubleOracleAlgorithm {
  public var results : DoubleOracleResults<PSP1, PSP2> {
    return DoubleOracleResults(
      statistics: log.map { $0.statItem },
      player1UtilityMatrix: player1UtilityMatrix,
      player2UtilityMatrix: player2UtilityMatrix,
      player1Plans: player1PureStrategies,
      player2Plans: player2PureStrategies,
      player1Equilibrium: player1EquilibriumApprox,
      player2Equilibrium: player2EquilibriumApprox,
      player1EquilibriumValue: player1EquilibriumValueApprox,
      player2EquilibriumValue: player2EquilibriumValueApprox,
      state: .finished)
  }
}

