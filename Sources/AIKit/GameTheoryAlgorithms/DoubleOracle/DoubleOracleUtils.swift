//
//  DoubleOracleUtils.swift
//  AIKit
//
//  Created by Pavel Rytir on 7/18/20.
//

import Foundation

public enum DoubleOracleUtils {
  /// Executes Double Oracle algorithm
  /// - Parameters:
  ///   - gameFileName: Game on which executes DO.
  ///   - outputDirName: Saves results into this directory.
  ///   - doconfigFileName: Configuration of DO.
  ///   - frasConfigFileName: General FRAS configuration.
  ///   - plansCachePath: Cache directory.
  public static func runDoubleOracle<
    E: PureStrategyEvaluator,
    IOP1: InitOracle,
    BROP1: BestResponseOracle,
    IOP2: InitOracle,
    BROP2: BestResponseOracle>(
      doubleOracleConfiguration: DoubleOracleConfiguration<E, IOP1, BROP1, IOP2, BROP2>,
      outputDirName: String?,
      plansCachePath: String?,
      saveResult: Bool = true) throws -> DoubleOracleResults<E.PSP1, E.PSP2> where
  E.PSP1 == IOP1.PS,
  E.PSP2 == IOP2.PS,
  E.PSP1 == BROP2.OPPONENTPS,
  E.PSP2 == BROP1.OPPONENTPS
  {
    let encoder = JSONEncoder()
    encoder.outputFormatting = .prettyPrinted
    let decoder = JSONDecoder()
    
    let outputFileNameAllResultsToSave: URL?
    var doubleOracle : DoubleOracleAlgorithm<E, IOP1, IOP2, BROP1, BROP2>
    let startWithPlayer : Int
    if let outputDirName = outputDirName {
      let outputDirURL = URL(fileURLWithPath: outputDirName, isDirectory: true)
      let outputFileNameAllResults = outputDirURL.appendingPathComponent("AllResults.json")
      outputFileNameAllResultsToSave = outputFileNameAllResults
      if FileManager.default.fileExists(atPath: outputFileNameAllResults.path) {
        throw AIKitError(message: "DO already computed results." +
          " Delete AllResults.json and dostate.json. If you want to restart computation!")
      }
      if FileManager.default.fileExists(
        atPath: outputDirURL.appendingPathComponent(
          "planCache/").appendingPathComponent("dostate.json").path) {
        doubleOracle = try decoder.decode(
          DoubleOracleAlgorithm.self,
          from: Data(contentsOf: outputDirURL.appendingPathComponent(
            "planCache/").appendingPathComponent("dostate.json")))
        doubleOracle.setPlannersFrom(configuration: doubleOracleConfiguration)
        startWithPlayer = doubleOracle.log.last!.player! == 1 ? 2 : 1
      } else {
        doubleOracle = try DoubleOracleAlgorithm(
          configuration: doubleOracleConfiguration,
          plansCacheDir: plansCachePath ?? outputDirURL.appendingPathComponent("planCache/").path,
          verbose: true)
        startWithPlayer = 1
      }
    } else {
      outputFileNameAllResultsToSave = nil
      doubleOracle = try DoubleOracleAlgorithm(
        configuration: doubleOracleConfiguration,
        plansCacheDir: nil,
        verbose: true)
      startWithPlayer = 1
    }
    let equilibriumReached = try doubleOracle.run(startWithPlayer: startWithPlayer)
    print(equilibriumReached ? "Double oracle converged" : "Double oracle have not converged")
    let result = doubleOracle.results
    if outputFileNameAllResultsToSave != nil && saveResult {
      let outputResultsData = try encoder.encode(result)
      try outputResultsData.write(to: outputFileNameAllResultsToSave!)
    }
    return result
  }
  
}
