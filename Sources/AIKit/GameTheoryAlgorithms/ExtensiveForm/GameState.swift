//
//  GameState.swift
//  FRASLib
//
//  Created by Pavel Rytir on 7/4/20.
//

import Foundation

public protocol GameState {
  associatedtype A: GameAction
  var activePlayerId: Int { get }
  func apply(_ action: A)
  func availableActions(for player: Int) -> [A]?
}

public protocol GameAction {
  
}
