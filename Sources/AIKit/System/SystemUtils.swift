//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/17/20.
//

import Foundation

final class Weak<T: AnyObject> {
  weak var value: T?

  init(_ value: T) {
    self.value = value
  }
}

///// Compute the prefix sum of `seq`.
//fileprivate func scan<
//  S : Sequence, U
//>(_ seq: S, _ initial: U, _ combine: (U, S.Iterator.Element) -> U) -> [U] {
//  var result: [U] = []
//  result.reserveCapacity(seq.underestimatedCount)
//  var runningResult = initial
//  for element in seq {
//    runningResult = combine(runningResult, element)
//    result.append(runningResult)
//  }
//  return result
//}
//
//public func withArrayOfCStrings<R>(
//  _ args: [String], _ body: ([UnsafeMutablePointer<CChar>?]) -> R
//) -> R {
//  let argsCounts = Array(args.map { $0.utf8.count + 1 })
//  let argsOffsets = [ 0 ] + scan(argsCounts, 0, +)
//  let argsBufferSize = argsOffsets.last!
//
//  var argsBuffer: [UInt8] = []
//  argsBuffer.reserveCapacity(argsBufferSize)
//  for arg in args {
//    argsBuffer.append(contentsOf: arg.utf8)
//    argsBuffer.append(0)
//  }
//
//  return argsBuffer.withUnsafeMutableBufferPointer {
//    (argsBuffer) in
//    let ptr = UnsafeMutableRawPointer(argsBuffer.baseAddress!).bindMemory(
//      to: CChar.self, capacity: argsBuffer.count)
//    var cStrings: [UnsafeMutablePointer<CChar>?] = argsOffsets.map { ptr + $0 }
//    cStrings[cStrings.count - 1] = nil
//    return body(cStrings)
//  }
//}

//final public class ProcessRunner {
//  private let process: Process
//  public private(set) var startTime: Date?
//  public init(
//    executableURL: URL,
//    arguments: [String]?,
//    currentDirectoryURL: URL? = nil) {
//    self.startTime = nil
//    self.process = Process()
//    process.executableURL = executableURL
//    if let arguments = arguments {
//      process.arguments = arguments
//    }
//    if let currentDirectoryURL = currentDirectoryURL {
//      process.currentDirectoryURL = currentDirectoryURL
//    }
//  }
//  public func run() throws {
//    
//    func startProcess() throws {
//      try process.run()
//      startTime = Date()
//    }
//    
//    DispatchQueue.main.sync {
//      try startProcess()
//    }
//  }
//}



//let serialQueue = DispatchQueue(label: "Results Queue")
//
//DispatchQueue.concurrentPerform(iterations: 100) { index in
//    let r = computePartialResult(chunk: index)
//    serialQueue.sync {
//        results.append(r);
//    }
//}
