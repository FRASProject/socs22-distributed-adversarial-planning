//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/15/21.
//

import Foundation

extension PDDLActionDuration {
  public var durationFunction: PDDLFunction? {
    if case let .expression(e) = self, case let .function(f) = e {
      return f
    } else {
      return nil
    }
  }
}


