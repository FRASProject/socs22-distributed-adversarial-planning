//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/10/20.
//

import Foundation

public struct PDDLTemporalGameOld {
  public let domain: PDDLDomainOld
  public let temporalPlannerDomain: String // TODO: Should be derived from domain.
  public let classicalPlannerDomain: String // TODO: Should be derived from domain.
  public let instance: PDDLGameInstanceOld
  public init(
    domain: String,
    instance: String,
    temporalPlannerDomain: String,
    classicalPlannerDomain: String){
    self.domain = PDDLDomainOld(stringRepresentation: domain)
    self.instance = PDDLGameInstanceOld(raw: instance)
    self.temporalPlannerDomain = temporalPlannerDomain
    self.classicalPlannerDomain = classicalPlannerDomain
  }
  public init(domainName: String, problemName: String) throws {
    let (gameDomainFile, gameInstanceFile, plannerDomainFile, plannerTemporalDomainFile, _) =
    ExperimentManager.defaultManager.findGameDomainAndInstance(.dictionary([
        "GAME": AIKitData.string(problemName),
        "DOMAIN": AIKitData.string(domainName)
      ]))
    try self.domain = PDDLDomainOld(
      stringRepresentation: String(contentsOf: URL(fileURLWithPath: gameDomainFile)))
    try self.instance = PDDLGameInstanceOld(
      raw: String(contentsOf: URL(fileURLWithPath: gameInstanceFile)))
    try self.temporalPlannerDomain = String(
      contentsOf: URL(fileURLWithPath: plannerTemporalDomainFile))
    try self.classicalPlannerDomain = String(contentsOf: URL(fileURLWithPath: plannerDomainFile))
  }
  public init(config: AIKitData) throws {
    try self.init(
      domainName: config["DOMAIN"]!.string!,
      problemName: config["GAME"]!.string!)
  }
  public func generateInitClassicalPlanningProblem(for player: Int) throws -> PDDLPlanningOld {
    let (classicalInstancePDDL, _ ) = try FRASPDDLGeneratorUtils.generateInitPlanPDDL(
      temporalGameDomain: domain.stringRepresentation,
      temporalGameInstance: instance.stringRepresentation,
      classicalPlannerDomain: classicalPlannerDomain,
      player: player)
    return PDDLPlanningOld(
      instance: PDDLPlanningProblemOld(raw:classicalInstancePDDL),
      domain: PDDLDomainOld(stringRepresentation: classicalPlannerDomain))
  }
  public func generateBestResponseClassicalPlanningProblem(
    for player: Int,
    opponentStrategy: MixedStrategy<String>) throws -> PDDLPlanningOld
  {
    let (classicalInstancePDDL, _ ) = try FRASPDDLGeneratorUtils.generateBestResponsePlanPDDL(
      temporalGameDomain: domain.stringRepresentation,
      temporalGameInstance: instance.stringRepresentation,
      classicalPlannerDomain: classicalPlannerDomain,
      player: player,
      opponentStrat: opponentStrategy)
    return PDDLPlanningOld(
      instance: PDDLPlanningProblemOld(raw:classicalInstancePDDL),
      domain: PDDLDomainOld(stringRepresentation: classicalPlannerDomain))
  }
  public func generateInitTemporalPlanningProblem(for player: Int) -> PDDLPlanningOld {
    PDDLPlanningOld(
      instance: instance.generateTemporalInitProblem(for: player),
      domain: PDDLDomainOld(stringRepresentation: self.temporalPlannerDomain))
  }
}
