//
//  PDDLGameProblem.swift
//  AIKit
//
//  Created by Pavel Rytir on 10/4/20.
//

import Foundation

public struct PDDLGameInstanceOld {
  public let name: String
  public let domainName: String
  public let objectsType: [String:[PDDLObjectOld]]
  public let initState: PDDLGenerationStateOld
  public let goalState: [Int: PDDLGenerationStateOld]
  public let players: [Int: PDDLObjectOld]
  public let raw: String?
  public init(raw: String) {
    // TODO: Implement parsing and remove raw property.
    self.raw = raw
    self.name = ""
    self.domainName = ""
    self.objectsType = [:]
    self.initState = PDDLGenerationStateOld()
    self.goalState = [:]
    self.players = [:]
  }
  public init(
    name: String,
    domainName: String,
    objectsType: [String:[PDDLObjectOld]],
    initState: PDDLGenerationStateOld,
    goalState: [Int: PDDLGenerationStateOld],
    players: [Int: PDDLObjectOld]
  ) {
    self.name = name
    self.domainName = domainName
    self.objectsType = objectsType
    self.initState = initState
    self.goalState = goalState
    self.players = players
    self.raw = nil
  }
  
  public var objects: [PDDLObjectOld] {
    objectsType.flatMap { $0.value }
  }
  
  public var stringRepresentation: String {
    if let raw = raw { return raw }
    var pddl = "(define (problem \(self.name))\n"
    pddl += "(:domain \(self.domainName))\n"
    pddl += "(:objects \n\(objects.objectsStringRepresentation))\n"
    pddl += "(:init\n\(initState.stringRepresentation))\n"
    
    for (id, _) in Array(players).sorted(by: { $0.0 < $1.key}) {
      // Player goal
      pddl += " ; Player \(id)\n"
      pddl += "(:goal\n(and \n\(generateGoalString(for: id))\n))\n"
      //Player metric
      pddl += "(:metric minimize \(generateMetricString(for: id))\n)\n"
    }
    pddl += ")\n"
    return pddl
  }
  
  private func generateGoalString(for player: Int) -> String {
    var str = ""
    var predicates = goalState[player]!.predicates
    predicates.sort(by: {$0.name < $1.name})
    for predicate in predicates {
      for instanceData in predicate.sortedData {
        let goalPredicateString  = instanceData.reduce("(\(predicate.name)") { $0 + " \($1.name)" } + ")"
          //+ " \(player.name))"
        
        let goalPredicateString2 = instanceData.reversed().reduce(
          "(preference ") // \(player.name)
          {$0 + "\($1.name)-" } +
          "\(predicate.name)"
        
        str += goalPredicateString2 + " " + goalPredicateString + ")\n"
      }
    }
    return str
  }
  private func generateMetricString(for player: Int) -> String {
    var str = "(+\n"
    var predicates = goalState[player]!.predicates
    predicates.sort(by: {$0.name < $1.name})
    for predicate in predicates {
      for instanceData in predicate.sortedData {
        let weight = 100
        str += "(* \(weight) (is-violated " +
          instanceData.reversed().reduce("") {$0 + "\($1.name)-" } + "\(predicate.name)))\n"
      }
    }
    str += ")\n"
    return str
  }
}


extension PDDLGameInstanceOld {
  public func generateTemporalInitProblem(for player: Int) -> PDDLPlanningProblemOld {
    let playerObject = players[player == 1 ? 2 : 1]!
    return PDDLPlanningProblemOld(
      name: name,
      domainName: domainName,
      objects: objects.filter { !$0.isEqualTo(other: playerObject) },
      initState: initState.filtered(without: playerObject),
      goalState: goalState[player]!,
      metric: PDDLPlanningProblemOld.minimizeTotalTimeMetric)
  }
}

extension PDDLGameInstanceOld {
  public var gcdActionsLengths: Int? {
    actionLengths.map {Int($0.rounded(.up))}.gcd
  }
  public var actionLengths: [Double] {
    initState.temporalActionsFluents.compactMap { $0.data }.flatMap { $0.map { $0.1 } }
  }
}

