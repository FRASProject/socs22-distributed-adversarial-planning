//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/27/21.
//

import Foundation

public enum PDDLEffect: Hashable {
  case negativeTerm(PDDLAtomicTerm)
  case term(PDDLAtomicTerm)
  case assignment(PDDLAssignmentEffect)
}

extension PDDLEffect {
  public init(from predicateDefinition: PDDLPredicateSkeleton) {
    self = .term(PDDLAtomicTerm(from: predicateDefinition))
  }
  public init(from literal: PDDLNameLiteral) {
    switch literal {
    case let .atomic(a):
      self = .term(PDDLAtomicTerm(from: a))
    case let .not(a):
      self = .negativeTerm(PDDLAtomicTerm(from: a))
    }
  }
  public var name: EffectName? {
    switch self {
    case let .term(term):
      return EffectName.positive(term.name)
    case let .negativeTerm(term):
      return EffectName.negative(term.name)
    case .assignment:
      return nil
    }
    
  }
  public func removing(
    _ parametersToRemove: [PDDLTerm],
    atLeastOneExists: Set<PDDLTerm>) -> PDDLEffect? {
    switch self {
    case let .term(atomicTerm):
      if let reducedAtomicTerm = atomicTerm.removing(
          parametersToRemove,
          atLeastOneExists: atLeastOneExists) {
        return .term(reducedAtomicTerm)
      } else {
        return nil
      }
    case let .negativeTerm(negativeTerm):
      if let reducedNegativeTerm = negativeTerm.removing(
          parametersToRemove,
          atLeastOneExists: atLeastOneExists) {
        return .negativeTerm(reducedNegativeTerm)
      } else {
        return nil
      }
    case let .assignment(assignmentEffect):
      if let reducedEffect = assignmentEffect.removing(parametersToRemove) {
        return .assignment(reducedEffect)
      } else {
        return nil
      }
    }
  }
}

public enum EffectName: Hashable {
  case negative(String)
  case positive(String)
  public var name: String {
    switch self {
    case let .positive(s):
      return s
    case let .negative(s):
      return s
    }
  }
}

public struct PDDLTemporalEffect: Hashable {
  public enum TimeSpecifier {
    case atStart
    case atEnd
  }
  public let timeSpecifier: TimeSpecifier
  public let term: PDDLEffect
}

extension PDDLTemporalEffect {
  public func removing(
    _ parametersToRemove: [PDDLTerm],
    atLeastOneExists: Set<PDDLTerm>) -> PDDLTemporalEffect?
  {
    if let restrictedTerm = term.removing(parametersToRemove, atLeastOneExists: atLeastOneExists) {
      return PDDLTemporalEffect(timeSpecifier: timeSpecifier, term: restrictedTerm)
    } else {
      return nil
    }
  }
}

public struct PDDLAssignmentEffect: Hashable {
  public enum AssignmentType {
    case increase
    case decrease
  }
  public let type: AssignmentType
  public let variable: PDDLFunction
  public let expression: PDDLExpression
}

extension PDDLAssignmentEffect {
  public func removing(_ parametersToRemove: [PDDLTerm]) -> PDDLAssignmentEffect? {
    if let reducedVariable = variable.removing(parametersToRemove),
       let reducedExpression = expression.removing(parametersToRemove) {
      return PDDLAssignmentEffect(
        type: type,
        variable: reducedVariable,
        expression: reducedExpression)
    } else {
      return nil
    }
  }
}
