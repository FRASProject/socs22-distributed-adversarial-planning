//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/19/20.
//

import Foundation

public struct PDDLFunctionLiteral {
  public let function: PDDLFunction
  public let value: Int
}

extension PDDLFunctionLiteral {
  public func shrinking(allObjectsOfType: Set<String>, to number: Int) -> PDDLFunctionLiteral {
    PDDLFunctionLiteral(
      function: function.shrinking(allObjectsOfType: allObjectsOfType, to: number),
      value: value)
  }
  public func deletingIfParametersPresent(
    _ parametersToRemove: Set<String>) -> PDDLFunctionLiteral?
  {
    function.deletingIfParametersPresent(parametersToRemove).map {
      PDDLFunctionLiteral(function: $0, value: value)
    }
  }
}

public struct PDDLAtomicNameFormula: Hashable, Comparable, Codable {
  public let name: String
  public let parameters: [String]
  public static func < (lhs: PDDLAtomicNameFormula, rhs: PDDLAtomicNameFormula) -> Bool {
    lhs.name < rhs.name ||
      (lhs.name == rhs.name && lhs.parameters.lexicographicallyPrecedes(rhs.parameters))
  }
}

extension PDDLAtomicNameFormula {
  public func shrinking(allObjectsOfType: Set<String>, to number: Int) -> PDDLAtomicNameFormula {
    var count = number
    var newParameters = [String]()
    for parameter in parameters {
      if allObjectsOfType.contains(parameter) {
        if count > 0 {
          newParameters.append(parameter)
          count -= 1
        }
      } else {
        newParameters.append(parameter)
      }
    }
    return PDDLAtomicNameFormula(name: name, parameters: newParameters)
  }
  public func removing(parameters parametersToRemove: Set<String>) -> PDDLAtomicNameFormula? {
    if parametersToRemove.isDisjoint(with: parameters) {
      return self
    } else {
      return nil
    }
  }
}

public enum PDDLActionDuration: Hashable {
  case number(Int)
  case expression(PDDLExpression)
}

extension PDDLActionDuration {
  public func removing(_ parametersToRemove: [PDDLTerm]) -> PDDLActionDuration? {
    switch self {
    case let .number(number):
      return .number(number)
    case let .expression(e):
      if let re = e.removing(parametersToRemove) {
        return .expression(re)
      } else {
        return nil
      }
    }
  }
}

public struct PDDLPredicateSkeleton {
  public let name: String
  public let parameters: [PDDLTypedVariable]
}
extension PDDLPredicateSkeleton {
  public init(from function: PDDLFunctionSkeleton) {
    self.name = function.name
    self.parameters = function.parameters
  }
  public func restrictingNumberOfObjectsOf(type: String, to number: Int) -> PDDLPredicateSkeleton {
    PDDLPredicateSkeleton(
      name: name,
      parameters: parameters.restrictingNumberOfObjectsOf(type: type, to: number))
  }
}

public struct PDDLFunctionSkeleton: Equatable {
  public let name: String
  public let parameters: [PDDLTypedVariable]
}
extension PDDLFunctionSkeleton {
  public func restrictingNumberOfObjectsOf(type: String, to number: Int) -> PDDLFunctionSkeleton {
    PDDLFunctionSkeleton(
      name: name,
      parameters: parameters.restrictingNumberOfObjectsOf(type: type, to: number))
  }
}

public struct PDDLTypedVariable: Hashable {
  public let identifier: String
  public let type: String?
}

public struct PDDLTypedParameter {
  public let identifier: String
  public let type: String?
}

