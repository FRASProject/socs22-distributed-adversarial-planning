//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/27/21.
//

import Foundation

public struct PDDLAtomicTerm: Hashable {
  public let name: String
  public let parameters: [PDDLTerm]
}

extension PDDLAtomicTerm {
  public init(from predicate: PDDLPredicateSkeleton) {
    self.name = predicate.name
    self.parameters = predicate.parameters.map {
      PDDLTerm.variable($0.identifier)
    }
  }
  public init(from formula: PDDLAtomicNameFormula) {
    self.name = formula.name
    self.parameters = formula.parameters.map {
      PDDLTerm.name($0)
    }
  }
  public func removing(
    _ parametersToRemove: [PDDLTerm],
    atLeastOneExists: Set<PDDLTerm>) -> PDDLAtomicTerm?
  {
    let newParameters = parameters.removing(parametersToRemove)
    if newParameters == parameters {
      return self
    }
    if atLeastOneExists.isDisjoint(with: newParameters) {
      return nil
    } else {
      return PDDLAtomicTerm(name: name, parameters: newParameters)
    }
  }
  public var asPDDLAtomicNameFormula: PDDLAtomicNameFormula? {
    var newParameters = [String]()
    for parameter in parameters {
      if let name = parameter.name {
        newParameters.append(name)
      } else {
        return nil
      }
    }
    return PDDLAtomicNameFormula(name: name, parameters: newParameters)
  }
}
