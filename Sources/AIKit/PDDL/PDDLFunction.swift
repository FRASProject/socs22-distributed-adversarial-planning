//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/27/21.
//

import Foundation

public struct PDDLFunction: Hashable {
  public let name: String
  public let parameters: [PDDLTerm]
}

extension PDDLFunction {
  public func removing(_ parametersToRemove: [PDDLTerm]) -> PDDLFunction? {
    let newParameters = parameters.removing(parametersToRemove)
    if newParameters.isEmpty && !parameters.isEmpty {
      return nil
    } else {
      return PDDLFunction(name: name, parameters: newParameters)
    }
  }
  public func shrinking(allObjectsOfType: Set<String>, to number: Int) -> PDDLFunction {
    var count = number
    var newParameters = [PDDLTerm]()
    for parameter in parameters {
      if allObjectsOfType.contains(parameter.termName) {
        if count > 0 {
          newParameters.append(parameter)
          count -= 1
        }
      } else {
        newParameters.append(parameter)
      }
    }
    return PDDLFunction(name: name, parameters: newParameters)
  }
  public func deletingIfParametersPresent(_ parametersToRemove: Set<String>) -> PDDLFunction? {
    if parametersToRemove.isDisjoint(with: parameters.map {$0.termName}) {
      return self
    } else {
      return nil
    }
  }
}
