//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/27/21.
//

import Foundation

public indirect enum PDDLExpression: Hashable {
  case plus(PDDLExpression, PDDLExpression)
  case minus(PDDLExpression, PDDLExpression)
  case multiply(PDDLExpression, PDDLExpression)
  case divide(PDDLExpression, PDDLExpression)
  case unaryMinus(PDDLExpression)
  case number(Int)
  case function(PDDLFunction)
}

extension PDDLExpression {
  public func removing(_ parametersToRemove: [PDDLTerm]) -> PDDLExpression? {
    switch self {
    case let .function(function):
      if let reducedFunction = function.removing(parametersToRemove) {
        return .function(reducedFunction)
      } else {
        return nil
      }
    case let .number(number):
      return .number(number)
    case let .unaryMinus(e):
      if let re = e.removing(parametersToRemove) {
        return .unaryMinus(re)
      } else {
        return nil
      }
    case let .divide(e1, e2):
      if let re1 = e1.removing(parametersToRemove), let re2 = e2.removing(parametersToRemove) {
        return .divide(re1, re2)
      } else {
        return nil
      }
    case let .multiply(e1, e2):
      if let re1 = e1.removing(parametersToRemove), let re2 = e2.removing(parametersToRemove) {
        return .multiply(re1, re2)
      } else {
        return nil
      }
    case let .minus(e1, e2):
      if let re1 = e1.removing(parametersToRemove), let re2 = e2.removing(parametersToRemove) {
        return .minus(re1, re2)
      } else {
        return nil
      }
    case let .plus(e1, e2):
      if let re1 = e1.removing(parametersToRemove), let re2 = e2.removing(parametersToRemove) {
        return .plus(re1, re2)
      } else {
        return nil
      }
    }
    
  }
}


