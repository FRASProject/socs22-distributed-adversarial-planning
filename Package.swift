// swift-tools-version:5.5

import PackageDescription

let package = Package(
  name: "AIKit",
  platforms: [.macOS(.v12)],
  products: [
    .library(name: "AIKit", targets: ["AIKit"]),
    .executable(
      name: "ExperimentRunner",
      targets: ["ExperimentRunner"]
    ),
  ],
  dependencies: [
    .package(url: "git@gitlab.com:FRASProject/MathLib.git", from: "1.33.0"),
    .package(url: "git@github.com:pavelrt/Antlr4.git", from: "4.9.1"),
    .package(url: "https://github.com/apple/swift-argument-parser", from: "1.0.0"),
    .package(
          url: "https://github.com/apple/swift-collections.git",
          .upToNextMajor(from: "1.0.0")
        ),
    .package(url: "https://github.com/apple/swift-system", from: "1.0.0"),
    .package(url: "git@gitlab.com:FRASProject/temporal_downward.git", from: "1.0.1"),
    //.package(url: "https://github.com/pvieito/PythonKit.git", .branch("master")),
  ],
  targets: [
    .systemLibrary(name: "cplex", pkgConfig: "libcplex"),
    .systemLibrary(name: "python3embed", pkgConfig: "python3-embed"),
    //.systemLibrary(name: "temporaldownward", pkgConfig: "libtemporaldownward"),
    .systemLibrary(name: "downward", pkgConfig: "libdownward"),
    .target(
      name: "CAIKit",
      dependencies: [
        "python3embed",
      ]),
    .target(
      name: "AIKit",
      dependencies: [
        .product(name: "MathLib", package: "MathLib"),
        .product(name: "Antlr4", package: "Antlr4"),
        .target(name: "cplex"),
        //.target(name: "temporaldownward"),
        .product(name: "temporal_downward", package: "temporal_downward"),
        .target(name: "downward"),
        .target(name: "CAIKit"),
        .product(name: "Collections", package: "swift-collections"),
        .product(name: "SystemPackage", package: "swift-system"),
        //"PythonKit",
      ]),
    .executableTarget(
      name: "ExperimentRunner",
      dependencies: [
        .target(name: "AIKit"),
        .product(name: "ArgumentParser", package: "swift-argument-parser"),
      ]),
    .testTarget(
      name: "AIKitTests",
      dependencies: [.target(name: "AIKit")])
  ]
)
