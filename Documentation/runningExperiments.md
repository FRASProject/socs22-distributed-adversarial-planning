# Running experiments

## Setup required paths
Customize file configuration_example.json file. Mainly set "DOMAINS_PATH" to the benchmark PDDL files and "FASTDOWNWARD" to the Fastdownward planner.

## Running experiment

1. Create a new working directory.
2. Put and customize the experiment configuration file [experiment_config.json](Benchmarks/experiment_config.json).
3. Run the following command in the directory

```
/pathTo/AIKit/.build/release/ExperimentRunner --fastdownwardaliases configuration_fastdownward.json --experimentconfig experiment_config.json --frasconfig configuration_example.json --experiment PDDL_DO_Sampling_Baseline
```

4. Results will be saved in the current directory.
 


