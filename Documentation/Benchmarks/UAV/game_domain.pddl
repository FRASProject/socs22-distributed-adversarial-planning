(define (domain resource-hunting-classical)
(:requirements :typing :fluents :duration-inequalities :durative-actions :strips :adl :equality :preferences)
(:types unit resource location sensor player - object)
(:predicates
    (connected ?l1 ?l2 - location)
    (has-unit ?u - unit ?p - player)
    (at-unit ?u - unit ?l - location)
    (free ?u - unit) ;the unit can operate (used to prohibit applying actions for a single unit in parallel)
    (at-resource ?r - resource ?l - location)
    (available ?r - resource) ;the resource has not yet been "sampled"
	;(sampled ?u - unit ?r - resource) ;the unit acquired data about the resource
    (collected ?r - resource ?p - player) ; redundant?
    ;(can-communicate ?l - location) ;whether an unit can send data from the location to the "central system"
    (has-sensor ?u - unit ?s - sensor)
    (required-sensor ?r - resource ?s - sensor)
    (required-two-sensors ?r - resource ?s1 ?s2 - sensor)
)
(:functions
    (move-cost ?l1 ?l2 - location) - number
    (sample-cost ?r - resource) - number
    (sample-two-cost ?r - resource) - number
)

(:durative-action move
    :parameters (?u - unit ?curpos ?nextpos - location ?p - player)
    :duration (= ?duration (move-cost ?curpos ?nextpos))
    :condition (and
        (at start (at-unit ?u ?curpos))
        (over all (connected ?curpos ?nextpos))
        (over all (has-unit ?u ?p))
    )
    :effect (and
        (at start (not (at-unit ?u ?curpos)))
        (at end (at-unit ?u ?nextpos))
    )
)

(:durative-action sample
    :parameters (?u - unit ?r - resource ?l - location ?s - sensor ?p - player)
    :duration (= ?duration (sample-cost ?r))
    :condition (and
        (over all (at-unit ?u ?l))
        (at start (free ?u))
        (over all (at-resource ?r ?l))
        (at start (available ?r))
        (over all (has-sensor ?u ?s))
        (over all (required-sensor ?r ?s))
        (over all (has-unit ?u ?p))
    )
    :effect (and
        (at start (not (free ?u)))
        (at start (not (available ?r)))
        (at end (collected ?r ?p))
        (at end (free ?u))
    )
)

(:durative-action sample-two
    :parameters (?u ?u2 - unit ?r - resource ?l - location ?s ?s2 - sensor ?p - player)
    :duration (= ?duration (sample-two-cost ?r))
    :condition (and
        (over all (at-unit ?u ?l))
        (at start (free ?u))
        (over all (at-unit ?u2 ?l))
        (at start (free ?u2))
        (over all (at-resource ?r ?l))
        (at start (available ?r))
        (over all (has-sensor ?u ?s))
        (over all (has-sensor ?u2 ?s2))
        (over all (required-two-sensors ?r ?s ?s2))
        (over all (has-unit ?u ?p))
        (over all (has-unit ?u2 ?p))
    )
    :effect (and
        (at start (not (free ?u)))
        (at start (not (free ?u2)))
        (at start (not (available ?r)))
        (at end (collected ?r ?p))
        (at end (free ?u))
        (at end (free ?u2))
    )
)
)
