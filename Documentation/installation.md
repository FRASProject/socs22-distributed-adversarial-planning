#  Intallation

## Ubuntu linux 16.04

1. Install swift at least version 5.5. Follow instructions on https://swift.org/download/#releases
 
2. Install cplex into /usr/local/opt/ibm/ILOG/CPLEX_Studio or install elsewhere and create a link

3. Copy libcplex.cp to pkg-config directory. ( /usr/lib/pkgconfig ). Double check that libcplex.cp contains the correct path. Uncomment/comment paths for linux or OS X, respectively.

4. Clone project AIKit git clone git@gitlab.com:FRASProject/AIKit.git

5. swift build -Xswiftc "-DDISABLE_CFASTDOWNWARD"


## OS X

1. Install xcode 
2. Install cplex into /usr/local/opt/ibm/ILOG/CPLEX_Studio or install elsewhere and create a link
3. Copy libcplex.cp to pkg-config directory. ( /usr/local/lib/pkgconfig ). Double check that libcplex.cp contains the correct path. Uncomment/comment paths for linux or OS X, respectively.

4. Clone the project

5. swift build -Xswiftc "-DDISABLE_CFASTDOWNWARD"


## CLion IDE

1. Install swift plugin

2. Open the folder FRAS in CLion IDE.

3. Update CMakeLists.txt - Set correct path to swift toolchain.

4. Build project





